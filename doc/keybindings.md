# Keyboard Commands

## Views:
There are different views, which can be reached via keyboard commands:
- DirsView
- IndexView (ThumbView)
- Imageview


Each view has specific key bindings for the functionality it provides.

See the following tables to find out,
what key is bound to which functionality.


### DirsView:

| Key-Command   | Functionality
| ------------- | -------------
| Home / Pos1   | jump to first directory
| End           | jump to last directory
| Cursor_Left   | Previous Dir (dir with index decremented by 1)
| Cursor_Down   | Go to Dir below
| Cursor_Up     | Go to DIr above
| Cursor_Right  | Next Dir (dir with index incremented by 1)
| Ctrl-PageUp   | go to dir with index 1 less than current dir (same as 'h')
| Ctrl-PageDown | go to dir with index 1 higher than current dir (same as 'l')
|  h            | same as Cursor_Left
|  j            | same as Cursor_Down
|  k            | same as Cursor_Up
|  l            | same as Cursor_Right
| Enter         | Go into Indexview of the active Dir
|  e            | Same as Enter
| Ctrl-r        | reverse current order
| Ctrl-n        | sort by name of the directories
| Ctrl-s        | sort by size of (number of files/images in) the directories
| Ctrl-q        | **quit the program**
| d             | dump screen
|  &circ;       | toggle window size between default size and full screen mode
|  F            | toggle window size between default size and full screen mode


### Indexview/Thumbview:

| Key-Command   | Functionality
| ------------- | -------------
| Home / Pos1   | jump to first image
| End           | jump to last image
| PageUp        | ./.
| PageDown      | ./.
| Cursor_Left   | Previous Image (in indexview it is the image left to the current one) (index decremented by 1)
| Cursor_Down   | Go to Image below (in index-view) (increment index by width in number of images of the indexview)
| Cursor_Up     | Go to Image above (in index-view) (decrement index by width in number of images of the indexview)
| Cursor_Right  | Next Image (in indexview it is the image to the right of the current one) (index incremented by 1)
|  h            | same as Cursor_Left
|  j            | same as Cursor_Down
|  k            | same as Cursor_Up
|  l            | same as Cursor_Right
| Ctrl-PageUp   | go to dir with index 1 less than current dir
| Ctrl-PageDown | go to dir with index 1 higher than current dir
| Ctrl-r        | reverse current order
| Ctrl-w        | sort by image width
| Ctrl-h        | sort by image height
| Ctrl-s        | sort by image size (width x height)
| Ctrl-a        | sort by image aspect-ratio
| Ctrl-n        | sort by image name (abspath)
| Ctrl-m        | sort by image namestructure (of abspath)
| Ctrl-t        | Sort by modification time
| Ctrl-u        | Sort by Luminance (L of Lab colors)
| Ctrl-v        | Sort by a "ab" of "Lab" (planned for the future: 2D sort/clustering for (a,b) of Lab, to be viewed in (X,Y)-Grid)
| Ctrl-x        | exchange two images "horizontally" (the current image with the following image (the image with next index))
| Ctrl-y        | exchange two images "vertically" (the current image with the image below)
| Ctrl-0        | move active image to pos. 0 and all others shift right until pos. of active image
| Ctrl-9        | move active image to last pos. and all others shift left until pos. of active image
| d             | dump screen
| m             | mark / unmark current file
| n             | go to next marked image (wrap-around search)
| N             | go to previous marked image (wrap-around search)
| p             | print all filenames of this directory (only print marked, if files are marked)
| P             | print all filenames of this directory (even if marked files exist)
| r             | rename files, prepending either epoch-time, width, height, size to the name (if sorted by these properties) or by prepending the index to the basename (for all other sorts or unsorted index)
| R             | rename files, replacing the basename by the index (with _ after it) followed by the extension of the basename
| U             | Unmark all files of the current directory
| o             | open active image with program set in environment variable `PLIO_EXTERNAL_PROGRAM` (if enabled in config.h)
| Enter         | Go into Imageview of the active Image
|  e            | Same as Enter
|  x            | Same as Ctrl-x
|  y            | Same as Ctrl-y
|  q            | **go up to DirsView**
|  0            | same as Ctrl-0
|  &circ;       | toggle window size between default size and full screen mode
|  F            | toggle window size between default size and full screen mode


### Imageview:

| Key-Command   | Functionality
| ------------- | -------------
| Home / Pos1   | jump to first image (of the current dir)
| End           | jump to last image (of the current dir)
| Cursor_Left   | Previous Image (in indexview it is the image left to the current one) (index decremented by 1)
| Cursor_Down   | Go to Image below (in index-view) (increment index by width in number of images of the indexview)
| Cursor_Up     | Go to Image above (in index-view) (decrement index by width in number of images of the indexview)
| Cursor_Right  | Next Image (in indexview it is the image to the right of the current one) (index incremented by 1)
|  h            | same as Cursor_Left
|  j            | same as Cursor_Down
|  k            | same as Cursor_Up
|  l            | same as Cursor_Right
| Ctrl-PageUp   | go to dir with index 1 lower than current dir - show active image of that dir
| Ctrl-PageDown | go to dir with index 1 higher than current dir - show active image of that dir
| Ctrl-0        | move active image to pos. 0 and all others shift right until pos. of active image
| Ctrl-9        | move active image to last pos. and all others shift left until pos. of active image
| m             | mark / unmark current file
| n             | go to next marked image (wrap-around search)
| N             | go to previous marked image (wrap-around search)
| Enter         | Go back to Indexview of the directory, this image is in
|  \|           | flip image horizontally (toggling)
|   _           | flip image vertically (toggling)
|  \<            | rotate image Counter_Clock_Wise
|  \>            | rotate image Clock_Wise
|  e            | Same as Enter
| o             | open active image with program set in environment variable `PLIO_EXTERNAL_PROGRAM` (if enabled in config.h)
| d             | dump screen
|  q            | **go up to DirsView**
|  &circ;       | toggle window size between default size and full screen mode
|  F            | toggle window size between default size and full screen mode


