# Key Features:

- Easy overview over many images located in many subdirectories
- Three different views available so far: DirsView, IndexView, ImageView
- Each directory represented by one thumbnail
- Image-thumbs (for directories and images) shown in a 2D array
- Navigating in horizontal and vertical direction in the 2D array (in all three views)
- Keyboard driven control (inspired by sxiv/vim)
- Sorting images based on image properties (default: filename)
- Available sort properties:
  - width
  - height
  - size (which does mean width * height, not the file size)
  - aspect ratio
  - name
  - namestructure
  - file date (modification time)
  - Luminance/Luminosity (brightness)
  - color (a,b of Lab)
- custom change of image order
  - switching two (horitontally or vertically) adjoining images
  - move active image to first position in the 2D array, while the other images move up towards active position (rotation)
- Working directly on the filesystem (no database needed)
- Bulk renaming files by prepending index number (or properties like modification time) to each basename
- For each directory, it is memorized, which image was viewed the last time, when the directory was visited
- dump window to png file (in any view)
- all thumbnails are surrounded with a a quadratic frame (aspect ratio of image is obvious)

# Future Feature Ideas:
- Grouping / Clustering the images based on the image properties (e.g. color sort)
- (X,Y) grid view - e.g. for two dimensional property based display of clustered images - for example (a,b) of Lab in 2D grid
- (re)collecting images (moving to other directories - for example cluster based moving to directories)
- thumbnail caching
- scaling of the images other than fit-to-screen (e.g. 100\% view)
