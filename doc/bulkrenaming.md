# How Bulk-Renaming works

In all renaming variants of the files started with key <b>'r'</b>,
the basename of each file is changed,
so that the value of a certain property of an image
is prepended to the basename of each image file.

The default for the prepended property is the index number (integer value) of the according image.
(Position of the image in the array.)

The prepended property is affected by some of the sort commands,
when they have been used before the renaming operation.

See the following table for clarification:

Used Sort      | property prepended to the basename
-------------- | ----------------------------------
   ./.         | index value (integer)
 name          | index value (integer)
 name-structure| index value (integer)
 aspect ratio  | index value (integer)
 luminance     | index value (integer)
 (a,b) color   | index value (integer)
 mod-time      | modtime (unix epoch, subseconds resolution)
 width         | width
 height        | height
 size          | size

Some of the prepended values will have leading 0's,
so that the files, when listed by name,
will have the same order as established by the sort.

Reordering images with `Ctrl-x`, `Ctrl-y` or `Ctrl-0` (or synonym bindings for these commands)
will set the prepended property back to index value.

This can be used for a trick:
Say you want to order the images by width, height or size,
but you also want the prepend property be the index value.
This can be achieved, by sorting by the property you want,
and then use either x- or y-switching twice.
This way, the established sort order will not be changed
(because the switch is done twice),
nevertheless the prepended property will be the index value.


Example for files renamed after time sort:

original name | new name
--------------|------------------------------
 foo.png      | 0071103600.29247951_foo.png
 bar.png      | 1682080666.31914556_bar.png
