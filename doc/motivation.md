# Motivation for writing PLIO:

### sxiv
For many years I used
[sxiv](https://github.com/muennich/sxiv)
intensively.
I enjoyed the keyboard-driven usage and the comfortable overview
on many images at once.

But I was missing some options, which were not planned to be integrated by the
developer.
Especially sorting images by their properties and establish this order by
changing the filenames (bulk rename) accordingly, was what I was looking for.
I did not want to use image organizers, which add many features I don't need
and add overhead (database).
When exporting the files (just the files without the database),
the order that was shown by the organizer,
would not be preserved (by changing the filenames accordingly).
So the order of the images was not independent of the program in use.
My feature wishes for sxiv were not implemented, and some time later the sxiv
repository was set into archive mode.

### Alternatives
It's successor (as drop-in replacement)
[nsxiv](https://codeberg.org/nsxiv/nsxiv) could not convince me either,
as it was not intended to add many more features into this viewer.

Looking out for other image viewers for Linux (from time to time but over a longer
period) showed me, that what I was looking for, did not exist.
A lot of the image  viewers offer almost the same features and
have similar look & feel.
<SMALL>But, hey, the have a different name! Let's just try them all ;-)</SMALL>
Many of them allow editing of images - which I consider not being the purpose of a viewer.
But sorting the images by different properties - to allow viewing the images in a pleasant order -
was either not possible or it needed the effort to program extensions.
I think extensibility for certain specialized features is nice,
but sorting images by modification time or size IMHO
is not a specialized feature, but a basic one.

The exploration of other image viewers then evolved into the conclusion:
I either have to stay with sxiv/nsxiv, or have to write my own imageviewer.
While I tried the first option for some months,
I decided for the second option, as I had some free time in the fall of the year 2022.

### Missing Sort Options: Other People - Same Problem
During the time when I was looking out for a viewer,
I found out, that other people had similar requests:
- [An image viewer that can sort by modification date and do it in a directory with 70.000+ files ...](https://www.reddit.com/r/linuxquestions/comments/o5yyh8/an_image_viewer_that_can_sort_by_modification/)
- [Is there a Linux image viewer that can browse images by a specific order?](https://unix.stackexchange.com/questions/544194/is-there-a-linux-image-viewer-that-can-browse-images-by-a-specific-order)
- [Viewing the images in the order of modified date](https://forums.linuxmint.com/viewtopic.php?t=193294)
- [What Linux file manager can show images in sort list order?](https://www.linuxquestions.org/questions/linux-software-2/what-linux-file-manager-can-show-images-in-sort-list-order-4175577925/)

<b>
Obviously no answer there was sufficient to my needs.
So this viewer project was started.
</b>


Short before the first release of PLIO I found these articles, which affirm my above mentioned impression:
- [Application to order undated photos?](https://askubuntu.com/questions/835834/application-to-order-undated-photos) (I found this article on 2023-04-23.)
- [Digital light table for visually sorting/ordering image files before renaming them on Linux](https://softwarerecs.stackexchange.com/questions/80662/digital-light-table-for-visually-sorting-ordering-image-files-before-renaming-th) (found on 2023-04-23)
