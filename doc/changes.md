| release tag    | changes                                                     |
| -------------- | ----------------------------------------------------------- |
| 2025\_02\_01.0 | Missing headerfiles included (which were not necessary before).|
| 2024\_12\_18.0 | Using pkg-config in Makefile |
| 2024\_11\_01.0 | Sort by namestructure changed, to better work with hex-filenames |
| 2024\_06\_24.0 | To avoid inconsitencies, renaming done only if names will not become too long; new functionality: goto next/prev marked image. |
| 2024\_03\_12.0 | BUGFIX: file-renaming also updates namestructure; time-strings 9 digits after comma; modtime of directories tracked and warning message, if dir changed externally; print commands do flushing; README mentions new env-vars (prescale and fontname) |
| 2023\_10\_25.0 | `PRESCALE` and `FONT_NAMAE` from `config.h` can be overridden with environment variables `PLIO_PRESCALE` and `PLIO_FONTNAME` |
| 2023\_09\_26.0 | dumpscreen: with timestamp (and homdir via $HOME), non-existing paths now ignored, Both Shift- and Ctrl-Keys accepted (not only left keys) Bugfix in get\_kmapping() Code cleanups, documentation enhanced |
| 2023\_08\_20.0 | Direct Dir-change in all views (Ctrl-PageUp / Ctrl-PageDown) implemented. Ctrl-9: move active image to last pos. of IndexView. Code/Docs cleanup/corrections.|
| 2023\_07\_31.0 | Switched back to keycodes instead of scancodes (events). Fixed a problem with recursive symlinks.|
| 2023\_07\_29.1 | Update for the changes.md  for 2023-07-29.0 supplemented |
| 2023\_07\_29.0 | Mogrify-command output added |
| 2023\_07\_25.0 | Adjustments in dpi-scaling; opening image with external programm (set via ENV-var) via 'o' command; switched from keycodes to scancode in keyboard input section; 'P' command; 'F' command as proxy key for caret/circumflex; 'R' command for replacing rename; some code cleanups |
| 2023\_05\_15.0 | dpi-scaling corrected - now thumbsize and lengths also adapted |
| 2023\_05\_14.0 | prescaling and dpi-scaling added                            |
| 2023\_05\_08.0 | renaming operation overhauled - name clahses now impossible |
