# First Steps of Usage

## Starting the Viewer

Here is assumed, that the viewer has been already installed
and can be started from the commmand line by entering the program name.

in general, `plio` only shows directories,
which contain images.
Empty directories or directories that don't contain at least one
image file, will just be ignored.

If there are no images in the directories that have been looked at,
`plio` would just quit.

On the command line, `plio` can be started without arguments:

 <b>$ plio</b>

In this case, `plio` looks for images only in the current working directory.


You also can start the viewer with a list of directory names:

 <b>$ plio < dirname ></b>

or

 <b>$ plio <dirname_1> ... <dirname_n></b>

This would open all the directories, look for image files there,
and show all directories with a representative thumbnail, if they contain image files.
No subdirectories will be opened with this command.

If you want to also open sub-directories,
then use the `-s` switch:

 <b>$ plio -s <dirname_1> ... <dirname_n></b>

This then would look for image files in all directories given on the command
lines as well as all subdirectories of them.

Each directory is represented by a thumbnail of an image found in it.


## Directory Arguments and Subdir-Switch
The selection of directories that will be scanned for image files is summed up in this table:

|                                    | without -s                                  | with -s |
| ---------------------------------- | ------------------------------------------- | ------------------------------------------- |
| **no dirnames given**              | only the current working directory `.`      | the current working directory `.` and all it's subdirs |
| **dirnames given**                 | only the directories given via command line | only the directories given on the command line plus all their subdirectories |


## Mogrify commands for flipped and rotated images
In the ImageView (explained below), you can flip images horizontally and vertically, and you also can rotate them.
These changes don't affect the image files, and only present the images differently on the screen.
While `plio` is intended to view, but not to modify images, it nevertheless would be pleasant, if images that must be flipped/rotated
for viewing, can be changed accodringly, so that these changes are persistent.

You can use external tools for this,
and the tool supported by `plio` is the command line tool
[mogrify](https://imagemagick.org/script/mogrify.php),
which is one of the tools that
[ImageMagick](https://imagemagick.org/)
offers.

If you want the flip- and rotate- commands be performed, you need the names of the files and the information,
which file has been flipped/rotated, and how.

This is supported by `plio`.
You can use the command line switch `-m`, and `plio` will print the `mogrify` commands,
which you can use to change the image files in-place.



## Window size

Plio starts with a default size,
which is configured in the file
[config.h](../src/config.h).
With the key `^` the window is switched to full-screen.
Using the same key a second time, switches back the size to the default size.

This is the behaviour in floating mode.
With a tiling window manager, when you put `plio` into a tile,
it follows the size that the window manager sets.

In general you will get the best usage experience with tiling window managers.
Because `plio` has been designed with large amounts of images in mind,
fullscreen mode seems to be the natural size anyhow.



## Scaling the Views
If needed, the whole layout (thumbsize, lengths, fontsize) can be scaled (up and down)
via the PRESCALE value in
[config.h](../src/config.h).

For high display DPI values (higher than 100), automatic upscaling is provided,
so that PRESCALE is not necessary to be used for monitor adaption.



## Further Usage

After you sent that command to your shell,
the viewer is started and
for each directory that contains image files, a thumbnail is shown.
So, when starting the viewer, each image directory is reprresented by a thumbnail.
This view is called the **DirsView**.

There is a <em>green square</em> visible, marking the currently selected directory.
The directories' name is shown in the footer of the page.

If you press the `ENTER` key, the current directory will be opened in a different view.
This view is called the **IndexView** or **ThumbView**.
If the directory is entered the first time (since starting the program),
the image files of that directory will be read (which may take a while), so
that the Thumbs of the images of that dir can be shown in the index.

Pressing the `ENTER` key again
leads into the **ImageView**.
As the name suggests, the currently selected image
(marked with a <em>green square</em> in the IndexView)
will be opened and shown.

Pressing `ENTER` again would lead you back to the IndexView.

If you press the `q`-key, from within the ImageView or the IndexView/ThumbView,
the current image or directory will be left and the DirsView is shown again.

If you leave the ThumbView, even though not all images of the directory
has been read already,
no more images will be read.
If entering the directory again,
more of the image files will be read.
Reading images of a directory is only done when you are in either the IndexView or the ImageView.
Going back to the DirsView stops reading more files.
(This allows peeking into many directories, without starting too many file
reading in the background, which would need more and more system resources.)

**Initially** the directories in the DirsView as well as the files in the IndexView
are **sorted by name**. You can change the sort order, but if not all files have been read so far,
you need to repeat the sorting for new files.
Only the name-sorting would be guaranteed to stay intact during reading of more files.

To **move** around the *green square* to select a different directory or image,
you can use either the Cursor-Keys, or the vim keybindings `h`, `j`, `k` and `l`.
Instead of `ENTER` you can use the `e` key.

If you want to <b>leave the program</b>,
first go back to the DirsView.
Then press `Ctrl-q`.

There are a lot more keybindings,
which can be found in the tables of the section on keyboard commands.


## Switching between the Views


```mermaid
stateDiagram-v2
    [*] --> DirsView

    DirsView --> IndexView : Enter
    IndexView --> ImageView : Enter
    IndexView --> DirsView : q

    ImageView --> IndexView : Enter
    ImageView --> DirsView : q

    DirsView --> [*] : Ctrl-q

```
