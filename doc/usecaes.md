# Usage / Use Cases of Sort and Rename in Detail

## Sorting / Reordering

### Sorting with available Sort Commands
If you want to use the available sort commands,
for example sort by modification time, sort by image height, ...,
you can just use those commands,
as can be found in [Keybindings of  Commands](keybindings.md)
and you are done.

### Changing the Order of two adjacent Images
- If you want to change two images, where one follows the other directly,
  you can use the `Ctrl-x` command.
  The active image is then exchanged with the one following it (right of it, or
  more general: the index with the next higher index value).

- If you want to change two images, where one is directly below the other,
  you can use the `Ctrl-y` command.
  The active image is then exchanged with the one below it.

### Collect (cluster) a Bunch of Images
- If you want to collect certain images,
  so that they are grouped together,
  one after the other,
  you can either use the above commands and switch images pairwise.
  Just repeat the commands for switching, then maybe change the active image and
  use the above mentioned switch-commands again, until you are ready.
- It is also possible to use the `Ctrl-0` command, which moves the active image to position 0
  and then shifts all images between pos. 0 and the pos. of the active image in the direction of the active image.
  (It's a rotation.)
  If You repeat that procedure after selecting the next image that needs to be
  part of the group, you can select another image as active, use `Ctrl-0` again,
  and repeat the procedure as often as needed.
  Then all images that belong together, will appear at the beginning of the
  image array. If you wish, you then can reorder these also.



## Making an Image Order permanent / persistent

When plio starts up, all images are read, sorted by names of the imagefiles.

If you later reorder them, this order is only maintained,
as long as the program is running.
If you quit plio, the order is gone.
Also you can ruin your image order, if after applying certain hand-crafted order,
you use one of the built-in orders like time-sort or name-sort.
Your prvious order is then eliminated.

If you want a certain image order be persistent/permament,
you have to rename the files (`r` command), after you arranged your pleasent order.
Then the basenames of the files will be renamed - and if the program is quitted,
next time the files will be read again with name-ordering.
But because the renaming has established the aimed order in the image names,
that order is available again.

If you do a more complicated reordering,
feel free to use the rename command more often,
so that inermediate orders are represented in the filenames.
If you then go on with the reordering,
but mistakenly screwed up, you can use `Ctrl-n` to go back to the
persistent naming order (which was a establishes as intermediate order last time)
and then again start over from there.

The only problem with often renaming is, that the filenames become longer and longer.
