# Building and Installing PLIO

## Dependencies

It's assumed that the typical development libraries and tools of your GNU/Linux system are installed.
That means `gcc` as compiler, standard C-libraries, GNU's `make` and so on.
The Makefile to build `plio` uses the tool `pkg-config`, which also must be installed on your system.

Other system near dependencies:
- pthread (multi threading)
- librt (message queues)

Dependencies related Image-Processing and Rendering are:
- SDL2
- SDL_ttf
- FreeImage

On some GNU/Linux distributions the header files of libraries are located in packages seperated from the binaries,
so you may also need to install development packages as well.
For example on Debian you would need `libfreeimage3` as well as `libfreeimage-dev`.

## Preparation (Configuration)
The textfont for displaying information is configured in the file
[config.h](config.h).
You have to set the fontname definition to a fontname that is installed on your system.

With `$ fc-list` you should be able to get a list of TTF fonts, that are installed on your system.
Select the font you want to use and set the fontname definition accordingly.
You need to use an absolut pathname for the fontfile.

It's recommended to use a [Monospaced Font](https://en.wikipedia.org/wiki/Monospaced_font).
The reason is, that filenames are easier to read and compare,
when you skim through directories in the DirsView (or skim through the images in the ImageView).
The slashes of the imagefile's paths will then stay at the same place
for different names of the same length.

Update (2023-10-25): the environment variable `PLIO_FONTNAME` overrides the setting from `config.h`,
so the user can set the font.

## Building
From the main repository directory go into the directory `src`.

Then type (dollar sign indicating shell prompt):
  `$ make`
This should compile plio.

## Installing

### User installs in Home-Dir
After building plio, as mentioned above,
just copy plio from the src dir to `$HOME/bin`,
which should be mentioned in your `PATH` environment variable.

### User root installs by hand in `/usr/bin/` or `/usr/local/bin/`
Please check, what's the right place for self installed programs in your system.
Then as root user copy plio to the according directory.

### User root installs via Package Manager
When plio will be installable via package manager of your GNU/Linux system,
this is the recommended way to do it.

If you use Arch Linux, you can install plio via
this package for [AUR](https://aur.archlinux.org/packages/plio).

## De-Installing
Depending on your installation method, either remove plio by hand,
or deinstall with the help of the package manager of your GNU/Linux system.


## Build/Install succeeded, but Problems occured running PLIO?

### Images are shown, but no Text
The text-font is hard coded in the file [config.h](../src/config.h).
If the font defined there is not available on your system, plio runs without
displaying text on the window. (But writes it to stderr instead).

You need to either install the font, that is set in config.h,
or change the setting and build/install plio again.
Please read the section Preparation/Configuration on how to do that.
