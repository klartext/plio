/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <assert.h>

#include "geometry.h"
#include "grid.h"
#include "nav.h"


void show_nav_info( NAV * navp )
{
    assert(navp);

    if( navp->grid != NULL )
    {
        printf("               nav.grid (addr)        %p\n", navp->grid);
        printf("               nav.grid.veclen        %d\n", navp->grid->veclen);
        printf("               nav.grid.used_length   %d\n", navp->grid->used_length);
        printf("               nav.grid.index         %d\n", navp->grid->index);
    }
    else
    {
        fprintf(stderr, "               GRID of nav %p is not initialized!\n", navp);
    }

    //lock_grid(navp->grid);

    if( navp->scroll.ismax == NULL )
    {
        printf("               nav.scroll             NOT INITIALIZED\n");
    }
    else
    {
        printf("               nav.scroll.min         %d\n", navp->scroll.min);
        printf("               nav.scroll.max         %d\n", navp->scroll.max);
        printf("               nav.scroll.count       %d\n", navp->scroll.count);
        printf("               nav.view.min           %d\n", navp->scroll.min);
        printf("               nav.view.max           %d\n", navp->scroll.max);
    }

    printf("               nav.view_dim       ... no details here...\n");

    //unlock_grid(navp->grid);

    return;
}



void show_nav( NAV * nav )
{
    printf("-----------------------------------------------------------------------------------\n");
    show_grid_delim(nav->grid, ", ", "\n");
    show_counter_raw(&nav->scroll, ", ", "\n");
    show_range_raw(&nav->view, "", ", ", "\n\n");
    show_index_dimensions(&nav->view_dim);

    // sort status
    printf("The GRID is %s\n", nav->sort_status.is_sorted ? "sorted" : "unsorted");
    if( nav->sort_status.is_sorted )
    {
        printf("GRID was sorted with sort command %d\n", nav->sort_status.used_sort_command);
        printf("GRID-sort is %s\n", nav->sort_status.inverted ? "inverted" : "not inverted");
    }

    return;
}





void nav_move_right( NAV * navp )
{
    GRID   * grid = navp->grid;
    RANGE  * view   = &navp->view;
    BCOUNT * scroll = &navp->scroll;

    lock_grid(grid);

    RAG_PROP prop = rag(view, grid_get_index_raw(grid), grid_get_xlen_raw(grid));

    if ( prop.last_col  && prop.last_row  && (! (scroll->ismax)(scroll) ) ) // CMD_RIGHT_SCROLL_DOWN
    {
        (*scroll->incr)(scroll);
        //(*grid->xincr)(grid);
        grid_increment_raw(grid);
    }
    else // CMD_RIGHT
    {
        //(*grid->xincr)(grid);
        grid_increment_raw(grid);
    }

    unlock_grid(grid);

    return;
}


void nav_move_left( NAV * navp )
{
    GRID   * grid = navp->grid;
    RANGE  * view   = &navp->view;
    BCOUNT * scroll = &navp->scroll;

    lock_grid(grid);

    RAG_PROP prop = rag(view, grid_get_index_raw(grid), grid_get_xlen_raw(grid));

    if ( prop.first_row && prop.first_col ) // CMD_LEFT_SCROLL_UP;
    {
        (*scroll->decr)(scroll);
        //(*grid->xdecr)(grid);
        grid_decrement_raw(grid);
    }
    else // CMD_LEFT
    {
        //(*grid->xdecr)(grid);
        grid_decrement_raw(grid);
    }

    unlock_grid(grid);

    return;
}


void nav_move_up( NAV * navp )
{
    GRID   * grid = navp->grid;
    RANGE  * view   = &navp->view;
    BCOUNT * scroll = &navp->scroll;

    lock_grid(grid);

    RAG_PROP prop = rag(view, grid_get_index_raw(grid), grid_get_xlen_raw(grid));

    if ( prop.first_row ) // CMD_UP_SCROLL_UP;
    {
        (*scroll->decr)(scroll);
        //(*grid->ydecr)(grid);
        grid_decrement_y_raw(grid);
    }
    else // CMD_UP
    {
        //(*grid->ydecr)(grid);
        grid_decrement_y_raw(grid);
    }

    unlock_grid(grid);

    return;

}


void nav_move_down( NAV * navp )
{
    GRID   * grid = navp->grid;
    RANGE  * view   = &navp->view;
    BCOUNT * scroll = &navp->scroll;

    lock_grid(grid);

    RAG_PROP prop = rag(view, grid_get_index_raw(grid), grid_get_xlen_raw(grid));

    if ( prop.last_row  ) // CMD_DOWN_SCROLL_DOWN;
    {
        (*scroll->incr)(scroll);
        //(*grid->yincr)(grid);
        grid_increment_y_raw(grid);
    }
    else // CMD_DOWN
    {
        //(*grid->yincr)(grid);
        grid_increment_y_raw(grid);
    }

    unlock_grid(grid);

    return;
}


void nav_exchange_grid_entries_horizontal(NAV * navp )
{
    GRID   * grid = navp->grid;
    RANGE  * view   = &navp->view;

    lock_grid(grid);

    int index = grid_get_index_raw(grid);

    RAG_PROP prop = rag(view, index, grid_get_xlen_raw(grid));

    if( ! prop.last ) // if at last image, nothing follows it anyhow
    {
        int this = index;
        int next = index + 1;

        grid_exchange_payloads_raw(grid, this, next);
    }

    unlock_grid(grid);

    return;
}


void nav_exchange_grid_entries_vertical(NAV * navp )
{
    GRID   * grid = navp->grid;
    RANGE  * view   = &navp->view;

    lock_grid(grid);

    int index = grid_get_index_raw(grid);
    int grid_len = grid_length_raw(grid);
    int grid_xlen = grid_get_xlen_raw(grid);

    RAG_PROP prop = rag(view, index, grid_xlen);

    if( ! prop.last_row ) // if in last row, you can't see what's below (or there is nothing below)
    {
        int this = index;
        int next = index + grid_xlen; // one row below

        if( next < grid_len )  // can't exchange with element behind the last of the grid
        {
            grid_exchange_payloads_raw(grid, this, next);
        }
    }

    unlock_grid(grid);

    return;
}


void nav_reverse_order( NAV * navp )
{
    GRID   * grid = navp->grid;
    grid_reverse_payload_order(grid);
}


void nav_first_pos( NAV * navp )
{
    GRID   * grid = navp->grid;
    BCOUNT * scroll = &navp->scroll;

    grid_set_index(grid, 0);
    (*scroll->set)(scroll, 0);
}


/** \brief Set new position and scrolling according to newpos_idx; scroll only, if necessary.
*/
void nav_goto_pos( NAV * navp, int newpos_idx )
{
    assert(navp);

    int new_ypos = 0;
    int newscroll = 0;

    GRID   * grid = navp->grid;
    BCOUNT * scroll = &navp->scroll;
    INDEX_DIM * layout = &navp->view_dim;

    lock_grid(grid);

    int xlen = grid_get_xlen_raw(grid);

    grid_set_index_raw(grid, newpos_idx);

    index_to_indexpair(newpos_idx, xlen, NULL, &new_ypos);
    newscroll = new_ypos - layout->height + 1;

    if(newscroll < 0 )
    {
        newscroll = 0;
    }

    int scroll_diff = (*scroll->get)(scroll) - newscroll;

    if( scroll_diff < 0 || scroll_diff > layout->height )
    {
        (*scroll->set)(scroll, newscroll);
    }

    unlock_grid(grid);

    return;
}


void nav_last_pos( NAV * navp )
{
    assert(navp);

    int new_ypos = 0;
    int newscroll = 0;

    GRID   * grid = navp->grid;
    BCOUNT * scroll = &navp->scroll;
    INDEX_DIM * layout = &navp->view_dim;

    lock_grid(grid);

    int xlen          = grid_get_xlen_raw(grid);
    int new_index     = grid_length_raw(grid) - 1;

    grid_set_index_raw(grid, new_index);

    index_to_indexpair(new_index, xlen, NULL, &new_ypos);
    newscroll = new_ypos - layout->height + 1;

    if(newscroll < 0 )
    {
        newscroll = 0;
    }

    (*scroll->set)(scroll, newscroll);

    unlock_grid(grid);

    return;
}



