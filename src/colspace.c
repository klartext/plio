/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <math.h>
#include <stdio.h>

#include "colspace.h"



/* XYZ -> Lab
  https://help.commonvisionblox.com/API/C/FoundationDLL/group___f_color_space_conversion_functions.html#ga12807af9ab1de272d7b435f0d1e95db6
*/

__inline__
static double trplrt( double x ) // Triple-root (y = x ^ (1/3))
{
  const double ein_drittel = 1.0/3;
  return pow(x, ein_drittel);
}

// ======================================================================================



double wurzelausdruck( double p, double p_n )
{
    double schwellwert = 216.0 / 24389.0;
    double p_zu_pn     = p / p_n;
    double res = 0.0;

    if( p_zu_pn < schwellwert )
    {
        res = 1.0 / 116.0 * ( 24389.0 / 27.0 * p_zu_pn + 16.0);
    }
    else
    {
        res = trplrt(p_zu_pn);
    }

    return res;
}



double xyz_to_lab_l( XYZ * xyz, double y_n )
{
    double L = 116.0 * wurzelausdruck(xyz->y, y_n ) - 16.0;
    return L;
}


double xyz_to_lab_a( XYZ * xyz, double x_n, double y_n )
{
    double a = 500.0 * (wurzelausdruck(xyz->x, x_n) - wurzelausdruck(xyz->y, y_n));
    return a;
}


double xyz_to_lab_b( XYZ * xyz, double y_n, double z_n )
{
    double b = 200.0 * (wurzelausdruck(xyz->y, y_n) - wurzelausdruck(xyz->z, z_n));
    return b;
}


/*
__inline__
static void xyz_to_labf_d65( const XYZ* xyz, LABF * labf )
{
  return xyz_to_lab_raw(xyz, labf, 0.950455, 1.0, 1.088753);
}
*/

/*
  Die Werte für rgb und xyz werden via Pointer gelesen UND geschrieben!
*/
__inline__
static void rgbf_to_xyz( const RGBF * rgbf, XYZ * xyz )
{
  xyz->x = 0.4124564 * rgbf->r + 0.3575761 * rgbf->g + 0.1804375 * rgbf->b;
  xyz->y = 0.2126729 * rgbf->r + 0.7151522 * rgbf->g + 0.0721750 * rgbf->b;
  xyz->z = 0.0193339 * rgbf->r + 0.1191920 * rgbf->g + 0.9503041 * rgbf->b;
  return;
}


// SDL_Color rgba;
LABF rgb_to_lab( const RGBF * rgbf)
{
    LABF labf;
    XYZ xyz;

    XYZ xyz_n;

    // https://de.wikipedia.org/wiki/Lab-Farbraum#Umrechnung_von_XYZ_zu_Lab
    //XYZ xyz_n_d65_2  = { 95.047, 100.0, 108.883};
    //XYZ xyz_n_d65_10 = { 94.811, 100.0, 107.304 };

    //XYZ xyz_n_d50_2  = { 96.422, 100.0, 82.521 };
    XYZ xyz_n_d50_10 = { 96.720, 100.0, 81.427 };

    xyz_n = xyz_n_d50_10; // Auswahl des benutzten Korrekturwertesatzes


    // Berechnungen
    // ------------
    rgbf_to_xyz(rgbf, &xyz);

    labf.l = xyz_to_lab_l(&xyz, xyz_n.y);
    labf.a = xyz_to_lab_a(&xyz, xyz_n.x, xyz_n.y);
    labf.b = xyz_to_lab_b(&xyz, xyz_n.y, xyz_n.z);

/*
printf("xyz.x: %f  xyz.y: %f  xyz.z: %f\n", xyz.y, xyz.y, xyz.z);
printf("x/xn = %f\n", xyz.x / xyz_n_d65_2.x);
printf("y/yn = %f\n", xyz.y / xyz_n_d65_2.y);
printf("z/zn = %f\n", xyz.z / xyz_n_d65_2.z);
*/

    return labf;
}


// this function just casts the double's to int's
LABI labi_of_labf( const LABF * labf )
{
    LABI res;

    res.l = labf->l;
    res.a = labf->a;
    res.b = labf->b;

    return res;
}


void show_labf( LABF * col )
{
    printf("LABF color: L = %8.3f, a = %8.3f, b = %8.3f\n", col->l, col->a, col->b);
    return;
}


void show_labi( LABI * col )
{
    printf("LABI color: L = %8d, a = %8d, b = %8d\n", col->l, col->a, col->b);
    return;
}
