/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <assert.h>

#include "range.h"
#include "macros.h"


RANGE range_from_intpair( int a, int b )
{
    RANGE range;

    range.min = MIN(a,b);
    range.max = MAX(a,b);

    return range;
}



int bounded_int( int anyint, RANGE * range )
{
    int bounded = anyint;

    if( anyint < range->min )
    {
        bounded = range->min;
    }
    else if ( anyint > range->max )
    {
        bounded = range->max;
    }

    return bounded;
}



RANGE clip_range( RANGE * anyrange, RANGE * border )
{
    RANGE bounded;

    bounded.min = bounded_int(anyrange->min, border);
    bounded.max = bounded_int(anyrange->max, border);

    return bounded;
}



RANGE shift_range( RANGE * range, int shift )
{
    RANGE shifted;

    shifted.min = range->min + shift;
    shifted.max = range->max + shift;

    return shifted;
}




// range: pointer to a range
// msg: message (e.g. name) printed before the values
// sep: string between min and max (e.g. spaces or tabs),
// end: string at the end of the range (after range.max, e.g. \n)
void show_range_raw( RANGE * range, char* msg, char* sep, char* end )
{ // layout adapted to match that of BCOUNT-printer
    printf("%srange.min   = %3d%srange.max        = %3d%s", msg, range->min, sep, range->max, end);
    return;
}



void show_range( RANGE * range )
{
    show_range_raw(range, "", ", ", "\n");
    return;
}


// ----------------------------------------------------------------
// For a range that represents first and last index value of a GRID,
// for a given index (inside the RANGE) and a given xlen (<= length
// of the RANGE, calculate some properties.
// ----------------------------------------------------------------
RAG_PROP rag( RANGE * range, int index, int xlen )
{
    RAG_PROP property;
    int newidx = 0;

    newidx = index - range->min;                  // based on idx_0 = 0

    property.first = index == range->min ? true : false;
    property.last  = index == range->max ? true : false;

    property.first_row = (newidx / xlen == 0) ? true : false;
    property.first_col = (newidx % xlen == 0) ? true : false;

    property.last_col = ((newidx % xlen)  == xlen - 1) ? true : false;
    property.last_row = ( (newidx / xlen) == ((range->max - range->min) / xlen) ) ? true : false;

    property.local_idx = newidx;

    property.below   = (index < range->min) ? true : false;
    property.above   = (index > range->max) ? true : false;
    property.outside = property.below || property.above;
    property.inside  = ! property.outside;

    return property;

}



void print_ragprop( RAG_PROP * prop )
{
    if( prop->inside ) printf("-> inside\n");
    if( prop->outside ) printf("-> outside\n");
    if( prop->below ) printf("-> below\n");
    if( prop->above ) printf("-> above\n");

    if( prop->first ) printf("-> first\n");
    if( prop->last ) printf("-> last\n");

    if( prop->first_col ) printf("-> first_col\n");
    if( prop->last_col ) printf("-> last_col\n");

    if( prop->first_row ) printf("-> first_row\n");
    if( prop->last_row ) printf("-> last_row\n");

    printf("-> local_idx = %d\n", prop->local_idx);

    return;
}
