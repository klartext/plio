/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

#include <sys/types.h>
#include <dirent.h>

#include <libgen.h>
#include <assert.h>

#include "folder.h"
#include "stringhelpers.h"


// OS-X has no get_current_dir_name()
#ifndef get_current_dir_name
char *my_get_current_dir_name(void)
{
    char dirnamebuf[PATH_MAX];
    char * dirname = NULL;

    dirname = getcwd(dirnamebuf, sizeof(dirnamebuf));
    if( dirname == NULL )
    {
        fprintf(stderr, "%s(): %s\n", __FUNCTION__, strerror(errno));
    }
    else
    {
        dirname = strdup(dirnamebuf);
    }

    return dirname;
}

#define get_current_dir_name my_get_current_dir_name

#endif




/** \brief If dirname must be excluded, return true, else return false.
*/
bool dirname_is_to_be_ignored( char * dirname )
{
        if(    (!strcmp(dirname, "."))
                || (!strcmp(dirname, ".."))
                || (!strcmp(dirname, ".git"))
          )
        {
            return true;
        }

    return false;
}



bool regfilename_is_to_be_ignored( char * filename )
{
    char * accept[] = { ".jpg", ".jpeg", ".jpe", ".jif", ".jfif", ".jfi",
                        ".png", ".gif", ".tif", ".tiff", ".bmp", ".jp2",
                        ".j2k", ".jpf", ".jpx", ".jpm", ".mj2", ".eps", ".webp"
                      };

    int max_accept_idx = sizeof(accept)/sizeof(char*);

    // just in case: disallowed dirnames are also disallowed filenames
    // ---------------------------------------------------------------
    if( dirname_is_to_be_ignored(filename ) )
    {
        return true;
    }

    // go through accept list
    // ----------------------
    for( int idx = 0; idx < max_accept_idx; idx++ )
    {
        if( extension_is_equal(filename, accept[idx]) )
        {
            return false;
        }
    }

    return true;
}


/** \brief Returns boolean, indicating the existence of the file, given as `pathname`.
*/
bool file_exists( char * pathname )
{
    int exists = false;
    int res    = -1;

    res = access(pathname, F_OK);
    exists = ( res == 0 ) ? true : false;

    return exists;
}

int filetype_of_entry( char * real_pathname )
{
    struct dirent * dep = NULL;
    DIR  * dir          = NULL;
    int filetype = 0;

    char basenamebuf[PATH_MAX]; // as in struct dirent
    char dirnamebuf[PATH_MAX]; // as in struct dirent

    bcopy(real_pathname, basenamebuf, strlen(real_pathname) + 1);
    bcopy(real_pathname, dirnamebuf, strlen(real_pathname) + 1);

    dir = opendir(dirname(dirnamebuf));
    assert(dir);

    while( (dep = readdir(dir)) )
    {
        if( ! strcmp(dep->d_name, basename(basenamebuf)) )
        {
            filetype = dep->d_type;
            break;
        }

    }
    closedir(dir);

    return filetype;
}



/** \brief  Read items of specific d_type from directory,
        and write up to maxitems into resvec,
        returning the actual number of items written.
        The function pointed to by IGNORE_FUN gets a char* arg and returns true,
        for names to be ignored.

        The ignore-filter can be used to accept only filenames with a certain extension,
        or to ignore directories like '.git'.

    For all found items the absolute pathname is returned; it is allocated with strdup().

*/
static int read_generic_dtype_of_single_dir_write_vec_rec(char * dirname, int matching_d_type, char ** resvec, int maxitems, bool (*IGNORE_FUN)(char *), bool do_recursion );

static int read_generic_dtype_of_single_dir_write_vec_rec(char * dirname, int matching_d_type, char ** resvec, int maxitems, bool (*IGNORE_FUN)(char *), bool do_recursion )
{
    assert(dirname);
    if( maxitems <= 0) { return 0; }

    DIR  * dir      = NULL;
    int    retval   = -1;

    struct dirent * dep;
    char * startdir   = get_current_dir_name(); // chdir() back to this dir at the end

    char extended_abspath[PATH_MAX];
    int filetype = 0;

    int item_count = 0;


    retval = chdir(dirname); // go there to get correct name
    if( retval == -1 )
    {
        //fprintf(stderr, "%s(): could not open dir \"%s\"\n", __FUNCTION__, dirname);
        free(startdir); // get_current_dir_name() allocates mem with malloc()
        return item_count;
    }

    dir = opendir(".");
    if( dir )
    {
        while( item_count <= maxitems )
        {
            dep = readdir(dir);
            if( dep )
            {
                if( (*IGNORE_FUN)(dep->d_name) )
                {
                    continue;
                }

                if( ( ! (dep->d_type == matching_d_type) ) && ( ! (dep->d_type == DT_LNK) ) )
                {
                    continue;
                }

                realpath(dep->d_name, extended_abspath);

                if( ! file_exists(extended_abspath) )
                {
                    fprintf(stderr, "%s(): path \"%s\" does not exist / will be ignored\n", __FUNCTION__, extended_abspath);
                    continue;
                }

                if( !strcmp(dirname, extended_abspath) ) // opening current dir ad infinitum
                {
                    fprintf(stderr, "%s(): in dir \"%s\" opening \"%s\" would open \"%s\" - ignoring that entry (symbolic link loop)\n", __FUNCTION__, dirname, dep->d_name, extended_abspath);
                    continue;
                }

                filetype = dep->d_type;
                if( dep->d_type == DT_LNK )
                {
                    filetype = filetype_of_entry(extended_abspath);
                }

                if( filetype == matching_d_type ) // holds true for the orig as well as the expanded path
                {
                    resvec[item_count] = strdup(extended_abspath);
                    ++item_count;

                    if( do_recursion && filetype == DT_DIR )
                    {
                      item_count += \
                        read_generic_dtype_of_single_dir_write_vec_rec(
                                                                          extended_abspath,
                                                                          matching_d_type,
                                                                          resvec + item_count,
                                                                          maxitems - item_count,
                                                                          IGNORE_FUN,
                                                                          do_recursion
                                                                      );
                    }
                }
            }
            else
            {
                break;
            }
        }
        closedir(dir);
    }
    else
    {
        fprintf(stderr, "Dir %s could not be opened\n", dirname);;
    }

    chdir(startdir); // back to where we started
    free(startdir); // get_current_dir_name() allocates mem with malloc()

    return item_count;
}

static int read_generic_dtype_of_single_dir_write_vec(char * dirname, int matching_d_type, char ** resvec, int maxitems, bool (*IGNORE_FUN)(char *) )
{
  return read_generic_dtype_of_single_dir_write_vec_rec(dirname, matching_d_type, resvec, maxitems, IGNORE_FUN, false);
}



static int read_regfilenames_of_single_dir_write_vec(char * dirname, char ** resvec, int maxitems )
{
     return read_generic_dtype_of_single_dir_write_vec(dirname, DT_REG, resvec, maxitems, regfilename_is_to_be_ignored);
}



static int read_dirnames_of_single_dir_write_vec(char * dirname, char ** resvec, int maxitems )
{
     return read_generic_dtype_of_single_dir_write_vec(dirname, DT_DIR, resvec, maxitems, dirname_is_to_be_ignored);
}



STRING_VEC read_regfilenames_of_single_dir_to_vec(char * dirname )
{
    STRING_VEC files;
    bzero(&files, sizeof(files));

    files.num = count_files_extension_filtered(dirname, false);

    if( files.num > 0 )
    {
        files.vec = malloc(sizeof(char*) * files.num);
        if( files.vec != NULL )
        {
            files.used = read_regfilenames_of_single_dir_write_vec(dirname, files.vec, files.num);
        }
    }

    return files;
}



STRING_VEC read_dirnames_of_single_dir_to_vec(char * dirname )
{
    STRING_VEC dirs;
    bzero(&dirs, sizeof(dirs));

    dirs.num = count_dirs(dirname, false);

    if( dirs.num > 0 )
    {
        dirs.vec = malloc(sizeof(char*) * dirs.num);
        if( dirs.vec != NULL )
        {
            dirs.used = read_dirnames_of_single_dir_write_vec(dirname, dirs.vec, dirs.num);
        }
    }

    return dirs;
}



int count_dtype_items_recursively(char * dirname, int matching_d_type, bool (*IGNORE_FUN)(char *), bool do_recursion )
{
    assert(dirname);

    DIR  * dir      = NULL;
    int    retval   = -1;

    struct dirent * dep;
    char * startdir   = get_current_dir_name(); // chdir() back to this dir at the end

    char extended_abspath[PATH_MAX];
    int filetype = 0;

    int count = 0;


    retval = chdir(dirname); // go there to get correct name
    if( retval == -1 )
    {
        //fprintf(stderr, "%s(): could not open dir \"%s\"\n", __FUNCTION__, dirname);
        free(startdir); // get_current_dir_name() allocates mem with malloc()
        return count;
    }

    dir = opendir(".");
    if( dir )
    {
        while( true )
        {
            dep = readdir(dir);
            if( dep )
            {
                if( (*IGNORE_FUN)(dep->d_name) )
                {
                    continue;
                }

                if( ( ! (dep->d_type == matching_d_type) ) && ( ! (dep->d_type == DT_LNK) ) )
                {
                    continue;
                }

                realpath(dep->d_name, extended_abspath);

                if( ! file_exists(extended_abspath) )
                {
                    fprintf(stderr, "%s(): path \"%s\" does not exist / will be ignored\n", __FUNCTION__, extended_abspath);
                    continue;
                }

                if( !strcmp(dirname, extended_abspath) ) // opening current dir ad infinitum
                {
                    fprintf(stderr, "%s(): in dir \"%s\" opening \"%s\" would open \"%s\" - ignoring that entry (symbolic link loop)\n", __FUNCTION__, dirname, dep->d_name, extended_abspath);
                    continue;
                }

                filetype = dep->d_type;
                if( dep->d_type == DT_LNK )
                {
                    filetype = filetype_of_entry(extended_abspath);
                }

                if( filetype == matching_d_type ) // holds true for the orig as well as the expanded path
                {
                    ++count;
                }

                if( filetype == DT_DIR && do_recursion == true )
                {
                    count += count_dtype_items_recursively(extended_abspath, matching_d_type, IGNORE_FUN, do_recursion);
                }
            }
            else
            {
                break;
            }
        }
        closedir(dir);
    }
    else
    {
        fprintf(stderr, "Dir %s could not be opened\n", dirname);;
    }

    chdir(startdir); // back to where we started
    free(startdir); // get_current_dir_name() allocates mem with malloc()

    return count;
}


int count_files_extension_filtered(char * dirname, bool do_recursive )
{
    return count_dtype_items_recursively(dirname, DT_REG, regfilename_is_to_be_ignored, do_recursive);
}



int count_dirs(char * dirname, bool recursive )
{
    return count_dtype_items_recursively(dirname, DT_DIR, dirname_is_to_be_ignored, recursive);
}



static void print_names_of_imagefiles( char * dirname )
{
    int idx = 0;
    int actual_read = 0;
    int num_files = count_files_extension_filtered(dirname, false);
    char ** filevec = malloc(sizeof(char*) * num_files);

    if( filevec != NULL )
    {
        actual_read = read_regfilenames_of_single_dir_write_vec(dirname, filevec, num_files);
        for( idx = 0; idx < actual_read; idx++ )
        {
            printf("Dateiname: %s\n", filevec[idx]);
            free(filevec[idx]);
        }
        free(filevec);
    }
    return;
}



static void print_names_of_dirs( char * dirname )
{
    int idx = 0;
    int num_dirs = count_dirs(dirname, false);
    int actual_read = 0;

    if( num_dirs > 0 )
    {
        char ** dirvec = malloc(sizeof(char*) * num_dirs);
        if( dirvec != NULL )
        {
            actual_read = read_dirnames_of_single_dir_write_vec(dirname, dirvec, num_dirs);
            for( idx = 0; idx < actual_read; idx++ )
            {
                printf("Verzeichnisname: %s\n", dirvec[idx]);
                free(dirvec[idx]);
            }
            free(dirvec);
        }
    }
    return;
}


/** \brief Print names of imagefiles and names of directories directly located in the directory, given by 'dirname'.
*/
void print_names_of_imagefiles_and_dirs( char * dirname )
{
    int num_files = count_files_extension_filtered(dirname, false);
    int num_dirs = count_dirs(dirname, false);

    printf("*** Found %3d files and %3d dirs in %s\n", num_files, num_dirs, dirname);
    print_names_of_imagefiles(dirname);
    print_names_of_dirs(dirname);

    return;
}


/** \brief Read names of subdirectories (direct only or recursively) and optional include the original dirname.

How the boolean args/flags control the behaviour:

collect_orig |  recursive | Collected dirname strings
------------ | ---------- | ----------------------------------------------
    false    |    false   | only subdirs of ORIGDIR included
    false    |     true   | subdirs, subsubdirs, etc. of ORIGDIR included (recursive)
     true    |    false   | ORIGDIR and direct subdirs of ORIGDIR included
     true    |     true   | ORIGDIR, direct subdirs and all other subdirs of ORIGDIR (recursively) included
*/
STRING_VEC collect_subdirnames( char * dirname, bool collect_orig, bool recursive )
{
    STRING_VEC dirs;
    bzero(&dirs, sizeof(dirs));
    PATHFIDDLE dirpath;

    int add_orig = collect_orig ? 1 : 0; // allocate one item more, to add the original directory also

    dirs.num = add_orig + count_dirs(dirname, recursive);

    if( dirs.num > 0 )
    {
        dirs.vec = malloc(sizeof(char*) * dirs.num); // dirs.num subdirs + original from 'dirname'
        if( dirs.vec != NULL )
        {
            char ** start = dirs.vec + add_orig;
            dirs.used = \
                read_generic_dtype_of_single_dir_write_vec_rec(dirname, DT_DIR, start, dirs.num, dirname_is_to_be_ignored, recursive);
        }
    }

    if( add_orig )
    {
        ++dirs.used;
        dirpath = calculate_paths(dirname);
        dirs.vec[0] = strdup(dirpath.real);
    }

    return dirs;
}



/** \brief Read dir names (including startdir), and recurse subdirs, iff 'recursive' is true.
*/
static STRING_VEC read_dir_subdirs( char * dirname, bool recursive)
{
    STRING_VEC dirs;

    if( recursive )
    {
        dirs = collect_subdirnames(dirname, true, recursive); // may contain doublettes  // POTENTIELLES INTERFACE!!!
    }
    else
    {
        dirs = stringvec_of_string(dirname);
    }

    return dirs;
}



/** \brief For all strungs of the dir-stringvec, call read_dir_subdirs() and collect all the results.
*/
//static STRING_VEC collect_dirs_subdirs_for_all( STRING_VEC * dirsvec, bool recursive )
STRING_VEC collect_dirs_subdirs_for_dirsvec( STRING_VEC * dirsvec, bool recursive )
{
    STRING_VEC collected;
    STRING_VEC tmp;
    bzero(&collected, sizeof(collected));
    bzero(&tmp, sizeof(tmp));

    int didx = 0;

    for( didx = 0; didx < dirsvec->used; didx++ )
    {
        tmp = read_dir_subdirs(dirsvec->vec[didx], recursive);
        stringvec_append(&collected, &tmp);
        free_stringvec(&tmp);
    }

    return collected;
}
