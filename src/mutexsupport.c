/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mutexsupport.h"


//#define DEBUG_MUT

void print_mutex_info( MUTEX_WITH_ATTR * mut, char * action )
{
    printf("   %s():  Mutex %p, ", __FUNCTION__, mut);

    if(mut->name)
    {
        printf("(%s), ", mut->name);
    }
    else
    {
        printf("(?), ");
    }

    if( action )
    {
        printf("(%s)\n", action);
    }

    fflush(stdout);

    return;
}



void initialize_mutex( MUTEX_WITH_ATTR * mut )
{
    int res = 1;

    mut->name = NULL;

    res = pthread_mutexattr_init(&mut->attr);
    assert(res == 0);

    res = pthread_mutexattr_settype(&mut->attr, PTHREAD_MUTEX_ERRORCHECK);
    assert(res == 0);

    res = pthread_mutex_init(&mut->mutex, &mut->attr);
    assert(res == 0);

    return;
}


void mutex_set_name( MUTEX_WITH_ATTR * mut, char * name )
{
    mut->name = strdup(name);
    return;
}



void destroy_mutex( MUTEX_WITH_ATTR * mut )
{
#ifdef DEBUG_MUT
print_mutex_info(mut, "DESTROYING");
#endif
    pthread_mutexattr_destroy(&mut->attr);
    pthread_mutex_destroy(&mut->mutex);

    if( mut->name )
    {
        free(mut->name);
        mut->name = NULL;
    }

    return;
}


void lock_mutex( MUTEX_WITH_ATTR * mut )
{
#ifdef DEBUG_MUT
print_mutex_info(mut, "LOCKING");
#endif
    int res = 0;
    res = pthread_mutex_lock(&mut->mutex);
    if(res)
    {
        fprintf(stderr, "%s(): %s\n", __FUNCTION__, strerror(res));
        if( res == EDEADLK ) printf("EDEADLK\n");
    }
    assert(res == 0);

    return;
}


void unlock_mutex( MUTEX_WITH_ATTR * mut )
{
#ifdef DEBUG_MUT
print_mutex_info(mut, "UNLOCKING");
#endif
    int res = 0;
    res = pthread_mutex_unlock(&mut->mutex);
    assert(res == 0);

    return;
}
