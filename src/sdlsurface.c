/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>

#include "sdlsurface.h"

/*
    Infos:
        - https://en.wikipedia.org/wiki/RGBA_color_model
        - https://wiki.libsdl.org/SDL2/SDL_Surface
        - https://wiki.libsdl.org/SDL2/SDL_PixelFormat
        - https://wiki.libsdl.org/SDL2/SDL_PixelFormatEnum

*/

/** \brief Calculate (8 Bit) pixel color value (R, G, B, A) of a 32-Bit Pixel.
*/
uint8_t pixel32_colval( uint32_t pixel, uint32_t mask, uint8_t shift, uint8_t loss )
{
    uint32_t val32 = 0;

    val32 = pixel & mask;
    val32 = val32 >> shift;
    val32 = val32 << loss;

    return val32;
}


// candidate for inlining or using a macro
void print_pixel_colors( SDL_Surface * surface, int pixel_idx )
{
    int num_pixels = surface-> w * surface->h;
    SDL_PixelFormat * pxlfrmt = NULL;

    uint32_t * pxladdr = NULL;
    uint32_t pixel = 0;
    uint8_t  red   = 0;
    uint8_t  green = 0;
    uint8_t  blue  = 0;
    uint8_t  alpha = 0;


    if( surface->format->BitsPerPixel != 32 )
    {
        fprintf(stderr, "BPP != 32 (but %d), color analysis not possible\n", surface->format->BitsPerPixel);
        return;
    }

    pxlfrmt = surface->format;

    printf("pxlfrmt->Rmask: %X\n", pxlfrmt->Rmask);
    printf("pxlfrmt->Gmask: %X\n", pxlfrmt->Gmask);
    printf("pxlfrmt->Bmask: %X\n", pxlfrmt->Bmask);
    printf("pxlfrmt->Amask: %X\n", pxlfrmt->Amask);
    putchar('\n');

    printf("pxlfrmt->Rloss: %X\n", pxlfrmt->Rloss);
    printf("pxlfrmt->Gloss: %X\n", pxlfrmt->Gloss);
    printf("pxlfrmt->Bloss: %X\n", pxlfrmt->Bloss);
    printf("pxlfrmt->Aloss: %X\n", pxlfrmt->Aloss);

    for( pixel_idx = 0; pixel_idx < num_pixels; pixel_idx++ )
    {
        pxladdr = (uint32_t *) surface->pixels + pixel_idx;
        pixel = *pxladdr;

        red   = pixel32_colval(pixel, pxlfrmt->Rmask, pxlfrmt->Rshift, pxlfrmt->Rloss);
        green = pixel32_colval(pixel, pxlfrmt->Gmask, pxlfrmt->Gshift, pxlfrmt->Gloss);
        blue  = pixel32_colval(pixel, pxlfrmt->Bmask, pxlfrmt->Bshift, pxlfrmt->Bloss);
        alpha = pixel32_colval(pixel, pxlfrmt->Amask, pxlfrmt->Ashift, pxlfrmt->Aloss);

        printf("Pixel %5d: Color -> R: %3d,  G: %3d,  B: %3d, A: %3d\n", pixel_idx, red, green, blue, alpha);
    }
}



void calculate_rgb_centroid( SDL_Surface * surface, RGBF * cent )
{
    int num_pixels = surface-> w * surface->h;
    SDL_PixelFormat * pxlfrmt = NULL;

    uint32_t * pxladdr = NULL;
    uint32_t pixel = 0;
    uint8_t  red   = 0;
    uint8_t  green = 0;
    uint8_t  blue  = 0;

    int rsum = 0;
    int gsum = 0;
    int bsum = 0;

    if( surface->format->BitsPerPixel != 32 )
    {
        fprintf(stderr, "BPP != 32 (but %d), color analysis not possible\n", surface->format->BitsPerPixel);
        return;
    }

    pxlfrmt = surface->format;
    pxladdr = surface->pixels;
    while( pxladdr < (uint32_t *) surface->pixels + num_pixels )
    {
        pixel = *pxladdr;
        red   = pixel32_colval(pixel, pxlfrmt->Rmask, pxlfrmt->Rshift, pxlfrmt->Rloss);
        green = pixel32_colval(pixel, pxlfrmt->Gmask, pxlfrmt->Gshift, pxlfrmt->Gloss);
        blue  = pixel32_colval(pixel, pxlfrmt->Bmask, pxlfrmt->Bshift, pxlfrmt->Bloss);
        //alpha = pixel32_colval(pixel, pxlfrmt->Amask, pxlfrmt->Ashift, pxlfrmt->Aloss);
        //printf("Pixelddr %p: Color -> R: %3d,  G: %3d,  B: %3d, A: %3d\n", pxladdr, red, green, blue, alpha);
        //printf("Pixelddr %p: Color -> R: %3d,  G: %3d,  B: %3d\n", pxladdr, red, green, blue);

        rsum += red;
        gsum += green;
        bsum += blue;

        ++pxladdr;
    }

    cent->r = ((double) rsum / (double) num_pixels); // + 0.5 ?
    cent->g = ((double) gsum / (double) num_pixels); // + 0.5 ?
    cent->b = ((double) bsum / (double) num_pixels); // + 0.5 ?
    cent->a = 0;

    return;
}
