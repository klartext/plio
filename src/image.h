/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __IMAGE__
#define __IMAGE__

#include <stdbool.h>

#include <FreeImage.h>
#include <SDL2/SDL.h>

#include "colspace.h"
#include "mutexsupport.h"
#include "marks.h"
#include "fibsurftex.h"


/*
    Documentation for SDL_Surface:
        https://wiki.libsdl.org/SDL2/SDL_Surface

    Documentation for Pixel Format of SDL_Surface:
        https://wiki.libsdl.org/SDL2/SDL_PixelFormat
*/


typedef struct _thumbnail THUMBNAIL;
struct _thumbnail
{
    SDL_Texture * texture;
    SDL_Surface * surface;
    FIBITMAP    * fib; // the fib can't be freed immadeiately,. because SDL_CreateRGBSurfaceFrom() is used
    //SURF_FIB      surface;
};

typedef enum rotate_num { ROT_0, ROT_90, ROT_180, ROT_270 } ROTATION;
#define ROTATION_MASK 0x03
#define HFLIP_MASK 0x04
#define VFLIP_MASK 0x08



typedef struct _imagestruct IMAGE;
struct _imagestruct
{
    char * abspath; // if NULL, then the folder is a virtual folder

    // char * basename_no_ext;
    // char * file_extension;

    SDL_Texture * texture;

    char * name; // symbolic name / tag
    int vecidx;  // positional index in the vecidx (some kind of id / permutation position)

    int width; // should initially be -1 to indicate invalidity
    int height; // should initially be -1 to indicate invalidity

    struct timespec modtime_hr;

    struct _comparable_
    {
        char * namestr;  // namestructure string

        RGBF centroid; // color centroid (RGB)
        LABF centroid_lab; // centroid converted to LABF
    } cmp;

    THUMBNAIL thumb;

    MARKS mark; // marking of the image (bitwise -> 8 different kind of marks)

    uint8_t orientation; // flip and rotate

    MUTEX_WITH_ATTR mut;
};

IMAGE * new_image_with_thumbnail( char * file_abspath, int thumb_size );
void destroy_image( IMAGE * img );

void show_image_dimensions( IMAGE * image );
double aspect_ratio_of_image( const IMAGE * image );
void print_image_info( IMAGE * img );

// function prototypes related to image orientation (unaltered, flipping, rotation)
void orientation_reset( IMAGE * img );

void orientation_set_hflip( IMAGE * img );
void orientation_unset_hflip( IMAGE * img );

void orientation_set_vflip( IMAGE * img );
void orientation_unset_vflip( IMAGE * img );

ROTATION orientation_get_rotate( IMAGE * img );
void orientation_set_rotate( IMAGE * img, ROTATION  rot );
bool orientation_is_hflip( IMAGE * img );
bool orientation_is_vflip( IMAGE * img );
int orientation_rot_degrees( IMAGE * img );

SDL_RendererFlip flip_of_image( IMAGE * img );
void print_mogrify_command( IMAGE * img );


// IMAGE-COMPARISON-FUNCTIONS
// --------------------------
int compare_width( const void * i1, const void * i2 );
int compare_height( const void * i1, const void * i2 );
int compare_size( const void * i1, const void * i2 );
int compare_aspect_ratio( const void * i1, const void * i2 );
int compare_name( const void * i1, const void * i2 );
int compare_modtime( const void * i1, const void * i2 );
int compare_namestructure( const void * i1, const void * i2 );

int compare_combine_size_aspect( const void * i1, const void * i2 );
int compare_combine_aspect_size( const void * i1, const void * i2 );
int compare_combine_namestruct_name( const void * i1, const void * i2 );

int compare_centroid_luminance( const void * i1, const void * i2 );
//int compare_centroid_lab_a( const void * i1, const void * i2 );
int compare_centroid_lab_ab( const void * i1, const void * i2 );
int compare_centroid_lab_ab_atan2( const void * i1, const void * i2 );
void rename_image( IMAGE * img, char * newname );

#endif
