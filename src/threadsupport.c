/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

/*
    This file contains support for threading.

    EXTRACTED allows extracting data from the Collections,
    while maintaining Mutex-locking including locked/not-locked
    status.
    As the EXTRACTED datastructure is intended to be thread-local,
    access to it's contents (including "already locked that thread")
    can be accessed without the need of additional synchronisation.
    Each thread though remembers for itself, if it has locked certain Mutexes.
    But by using these extraction functions for all threads,
    thread-locking-order is provided for all threads that use these functions.
*/

#include <assert.h>

#include "threadsupport.h"


// Service-Funktionen für Daten-Extraktion - sollen extract_nav_old() usw. ersetzen

// TODO: evtl. aufteilen auf zwei Funktionen, also prepare_extraction() und extract_dircoll()?
// ... aber dircoll hat man doch schon... nur nochj nicht gelockt?

EXTRACTED prepare_extraction( DIR_COLLECTION * dcollp )
{
    EXTRACTED ext_struct;
    EXTRACTED * ext = &ext_struct;

    // locking the Dir-Collection Mutex
    // But it's rather expected, that the locking has been done already
    //if (lock_dcoll == true )
    //{
    //    res = SDL_LockMutex(dcoll_mutex);
    //    assert(res == 0);
    //}

    ext->dcollp  = dcollp;
    ext->currdir = NULL;
    ext->currimg = NULL;

    ext->collnav = &ext->dcollp->nav;

    //ext->coll_mutex = dcoll_mutex;
    //ext->dir_mutex = NULL;

    return ext_struct;
}


void extract_currdir(EXTRACTED * ext)
{
    assert(ext != NULL);

    if( ext->currdir == NULL ) // if not already done
    {
        assert(ext->collnav != NULL );

        ext->currdir   = grid_get_payload(ext->collnav->grid);
        //ext->dir_mutex = ext->currdir->mut;

        //lock_mutex(&ext->dir_mutex);

        ext->dirnav = &ext->currdir->nav; // dirmutex locked, so nav can be 'exported'
    }

    return;
}



void unlock_currdir( EXTRACTED * ext )
{
    {
//print_mutex_info(&ext->dir_mutex, "unlock_currdir() wants to unlock the current dir");

        //unlock_mutex(&ext->dir_mutex);

        ext->currimg   = NULL;
    }

    return;
}

// unlock_coll -> unlocked image UND currdir UND DirColl ? Oder brauch ich das garnicht?
