/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include "timediff.h"
#include "stdio.h"

#include <SDL2/SDL.h>


void init_time( TIMEDIFF * td )
{
    td->tick_0 = SDL_GetTicks();
    td->tick_1 = td->tick_0;

    return;
}

void next_time( TIMEDIFF * td )
{
    td->tick_0 = td->tick_1;
    td->tick_1 = SDL_GetTicks();

    return;
}

uint32_t timediff( TIMEDIFF * td )
{
    return td->tick_1 - td->tick_0;
}


void print_timediff( TIMEDIFF * td, char * msg )
{
    if( msg == NULL )
    {
        printf("    -> timediff: %4u ms\n", timediff(td));
    }
    else
    {
        printf("    -> timediff (%s): %4u ms\n", msg, timediff(td));
    }

    return;
}
