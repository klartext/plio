/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <libgen.h> // PRINT-Command
#include <string.h> // PRINT-Command
#include <assert.h>

#include "display_index.h"
#include "display.h"
#include "geometry.h"
#include "macros.h"



void setup_initial_indexview( APP * app, DIRCONTENTS * dcont, INDEX_LAYOUT * layout )
{
    int win_w = 0;
    int win_h = 0;
    //int res = 0;

    // Setting up the GRID with the NAV'
    // -----------------------------------------

    grid_set_used_length(dcont->nav.grid, dcont->noi_num);
    SDL_GetWindowSize(app->window, &win_w, &win_h);
    calc_grid_navigation(win_w, win_h, &dcont->nav, layout);

    return;
}



/** \brief Get the keyboard input for the indexview/thumbindex.
*/
/** \brief Display thumbnails and draw frame around each of them for all images in 'imgidx_range' (view).
*/
void render_thumbindex_from_range( APP * app, INDEX_LAYOUT * layout, NAV * navp, SELECTED_ITEMS * title, SELECTED_ITEMS * footer )
{
    on_NULL_return(app);
    on_NULL_return(layout);
    on_NULL_return(navp);
    on_NULL_return(title);
    on_NULL_return(footer);

    GRID  * image_grid   = navp->grid;
    RANGE * imgidx_range = &navp->view;

    int win_w = 0;
    int win_h = 0;

    SDL_GetWindowSize(app->window, &win_w, &win_h);
    calc_grid_navigation(win_w, win_h, navp, layout);

    char textbuf[PATH_MAX];
    bzero(textbuf, sizeof(textbuf));

    int num_pics = imgidx_range->max - imgidx_range->min + 1;

    // show all thumbnails with frames and active frame in thick
    // ---------------------------------------------------------
    draw_thumb_frames(app->renderer, app->drawcol, layout, num_pics, win_w, win_h); // frames of the thumbs
    display_index_thumbs_of_grid_via_range(app->renderer, layout, image_grid, imgidx_range, win_w, win_h); // thumbs without frames
    display_marks(app->renderer, layout, image_grid, imgidx_range, win_w, win_h); // marks

    draw_thick_frame_for_active_image(app, layout, image_grid->index - imgidx_range->min, image_grid, win_w, win_h); // thick frame

    return;
}



SELECTED_ITEMS make_indexview_title(void)
{
    SELECTED_ITEMS title;
    bzero(&title, sizeof(SELECTED_ITEMS));

    title.curr_fmt = "%5d ";
    title.max_fmt  = "%d ";
    title.actual_sing  = "Image ";
    title.actual_plur  = "Images ";
    title.src_sing  = "File";
    title.src_plur  = "Files";
    title.wrap_start  = "(out of ";
    title.wrap_end    = ")";
    title.curr   = 0;
    title.max    = 0;

    return title;
}



SELECTED_ITEMS make_indexview_footer(void)
{
    SELECTED_ITEMS footer;
    bzero(&footer, sizeof(SELECTED_ITEMS));

    footer.curr_fmt = NULL;
    footer.max_fmt  = NULL;
    footer.actual_sing  = "Filename:     ";
    footer.actual_plur  = "Filename:     ";
    footer.src_sing    = "";
    footer.src_plur    = "";
    footer.wrap_start  = "";
    footer.wrap_end    = "FILENAME";
    footer.curr   = 0;
    footer.max    = 0;

    return footer;
}
