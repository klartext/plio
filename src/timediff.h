/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __TIMEDIFF__
#define __TIMEDIFF__

#include <stdint.h>
#include <stdbool.h>

typedef struct _timediff_ TIMEDIFF;
struct _timediff_
{
    uint32_t tick_0;
    uint32_t tick_1;
};

void init_time( TIMEDIFF * td );
void next_time( TIMEDIFF * td );
uint32_t timediff( TIMEDIFF * td );
void print_timediff( TIMEDIFF * td, char * msg );

#endif
