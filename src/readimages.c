/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <assert.h>

#include "folder.h"
#include "macros.h"
#include "readimages.h"
#include "tools.h"
#include "fib.h"


/** \brief Traverse filelist, return first readable/decompressable image file.
*/
static IMAGE * traverse_filelist_read_first_image( STRING_VEC * filelist, int thumbsize )
{
    IMAGE * img = NULL;
    int idx = 0;

    for( idx = 0; idx < filelist->used && img == NULL; idx++ )
    {
        img = new_image_with_thumbnail(filelist->vec[idx], thumbsize);
    }

    return img;
}


/** \brief From the directory read the first image file that cn be read.

    The image data is given back as a thumbnail inside a IMAGE structure.
*/

IMAGE * read_first_image_of_dir( char * dirname, int thumbsize )
{
    errexit_on_NULL(dirname);

    IMAGE * img = NULL;
    STRING_VEC  filenames;

    filenames = read_regfilenames_of_single_dir_to_vec(dirname);
    img = traverse_filelist_read_first_image(&filenames, thumbsize);
    free_stringvec(&filenames); // clean up

    return img;
}



/** \brief Check, if the list of filenames represents at least one file, which can be read by FreeImage-Lib.
*/
bool stringvec_contains_file_with_valid_fif( STRING_VEC * filenames )
{
    bool has_valid = false;

    for( int idx = 0; idx < filenames->used && (!has_valid); idx++ )
    {
        has_valid = filetype_is_valid_fif(filenames->vec[idx]);
    }

    return has_valid;
}
