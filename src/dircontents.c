/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "dircontents.h"
#include "folder.h"
#include "readimages.h"
#include "stringhelpers.h"
#include "fib.h"
#include "tools.h" // PRINTINT() usw



DIRCONTENTS make_dircontents( char * dirname )
{
    DIRCONTENTS dircont;
    bzero(&dircont, sizeof(dircont));

    STRING_VEC filenames_unsorted;
    STRING_VEC filenames;

    bool dir_has_no_valid_file = true;

    strcpy(&dircont.dirname[0], dirname);

    // read the files of dir and sort them
    // -----------------------------------
    filenames_unsorted    = read_regfilenames_of_single_dir_to_vec(dirname);
    dir_has_no_valid_file = ! stringvec_contains_file_with_valid_fif(&filenames_unsorted);

    if( (filenames_unsorted.used == 0) || (dir_has_no_valid_file) )
    {
        //fprintf(stderr, "No valid image files in directory %s\n", dirname);
        free_stringvec(&filenames_unsorted);
        return dircont;
    }
    else // set up filename entries and GRID
    {
        filenames = stringvec_sort_uniq(&filenames_unsorted); // derefrencing symlinks might have created doublettes
        free_stringvec(&filenames_unsorted);

        dircont.noi_vec = calloc(filenames.used, sizeof(NOID));
        assert(dircont.noi_vec != NULL);
        dircont.noi_num = filenames.used;

        for( int idx = 0; idx < dircont.noi_num; idx++ )
        {
            dircont.noi_vec[idx].noir.filename = filenames.vec[idx]; // the strings have been allocated with strdup.
        }

        dircont.nav.grid = create_grid(filenames.used, 1); // xlen = 1 is a dummy; the value will be set later

        // free_stringvec(&filenames) will NOT be used, because the strings now have been mounted into dircont
        // but the vector of strings, which is filenames.vec, will be free()'d, because it can't be reused.
        free(filenames.vec);
    }

    initialize_mutex(&dircont.mut);
    mutex_set_name(&dircont.mut, "Dircontents-Mutex");
//printf(stderr, "{ \"entity\": \"MUTEX_DIRCONTENTS\", \"dirname\": \"%s\", \"Mutex\": \"%p\" }\n", dirname, &dircont.mut.mutex);

    dircont.rpl = &dircont.noi_vec[0].noir.img; // RPL is set to first image as default

    modtime_of_file_hr(dirname, &dircont.modtime);

    dircont.valid = true;

    return dircont;
}



/** \brief Set the 'on_free_print_mogrify' flag.
*/
void set_mogrify_info( DIRCONTENTS * dircontp )
{
    assert(dircontp);
    dircontp->on_free_print_mogrify = true;
    return;
}



// The GRID is, what really is read by the Updater and Renderer - the noi_vec itself is not used by them
// -> so the GRID must be updated and consistent; noi can be under construction... noi_num can be number of Filenames
// there is no need to lower it later - only the GRID-scroll (max scroll index) must be adapted (increased with
// each new read Image-File, but can stay below noi_num -1.
//
// Flag updated will be set, if this function (at least attempts) to update the data structures.
// It can be used for detecting the need for rendering anew.
// It is allowed to pass a NULL pointer for that flag.
bool read_next_image_into_dircontents( DIRCONTENTS * dcont, int thumb_surface_size, bool * updated )
{
    IMAGE * img = NULL;
    bool call_me_again = false;

    if( dcont->completed )
    {
        return false;
    }

    assert(dcont->ridx < dcont->noi_num);
    assert(dcont->widx < dcont->noi_num);
    assert(dcont->noi_vec[dcont->widx].is_imagedata == false); // assert: not already converted

    // we are not completed, so we updated the data structures
    if( updated != NULL ) // allow NULL pointer to be passed for updated-flag
    {
        *updated = true;
    }

    // Reading the image - unlocking and relocking DIRCONTENTS-mutex for that purpose
    // ------------------------------------------------------------------------------
    img = new_image_with_thumbnail(dcont->noi_vec[dcont->ridx].noir.filename, thumb_surface_size); // directly reading image file

    if( img != NULL )
    {
        free(dcont->noi_vec[dcont->ridx].noir.filename); // free()ing, bevore it will be overwritten
        dcont->noi_vec[dcont->ridx].noir.filename = NULL;
        dcont->noi_vec[dcont->widx].noir.img = img;
        dcont->noi_vec[dcont->widx].is_imagedata = true;

        // In MULTITHREADED environment, GRID Locking would be needed around this whole section
        // and then the grid_..._raw() functions needed to be used here

        int old_index = grid_get_index(dcont->nav.grid); // save current index (maybe changed by other thread - viewing images)

        grid_increment_used_length(dcont->nav.grid);  // make grid longer
        grid_set_index(dcont->nav.grid, dcont->widx); // next insertion index
        grid_set_payload(dcont->nav.grid, img);       // insert

        grid_set_index(dcont->nav.grid, old_index); // restore old index value
        img->vecidx = dcont->widx; // setting the position in the dircontents as vecidx in the image

        ++dcont->ridx;
        ++dcont->widx;
    }
    else
    {
        ++dcont->ridx;
    }

    if( dcont->ridx == dcont->noi_num ) // no more filenames to read
    {
        dcont->completed = true;
    }

    call_me_again = ! dcont->completed; // if not completed, this function must be called again

    dcont->nav.sort_status.is_sorted = false;

    return call_me_again;
}



/* \brief Free dircontents (all images, grid etc.). If flag is set, mogrify-commands for flipped/rotated images will be printed before freeing an image.
*/
void free_dircontents( DIRCONTENTS * dcont )
{
    int idx = 0;
    assert(dcont != NULL);

    //fprintf(stderr, " *** Should now free data of the DIRCONTENTS\n");
    for( idx = 0; idx < dcont->noi_num; idx++ )
    {
        if( dcont->noi_vec[idx].is_imagedata )
        {
            if( dcont->noi_vec[idx].noir.img )
            {
                if( dcont->on_free_print_mogrify ) // MOGRIFY Command
                {
                    print_mogrify_command(dcont->noi_vec[idx].noir.img);
                }

                destroy_image(dcont->noi_vec[idx].noir.img);
                dcont->noi_vec[idx].noir.img = NULL;
            }
        }
        else if( dcont->noi_vec[idx].noir.filename)
        {
            free(dcont->noi_vec[idx].noir.filename);
            dcont->noi_vec[idx].noir.filename = NULL;
        }
    }

    if( dcont->noi_vec != NULL )
    {
        free(dcont->noi_vec);
        dcont->noi_vec = NULL;
    }

    if( dcont->nav.grid )
    {
        destroy_grid(dcont->nav.grid);
        dcont->nav.grid = NULL;
    }

    destroy_mutex(&dcont->mut);


    return;
}



// be sure, the dircontp-mutex is locked, when calling the printer function
void show_dircontents_info( DIRCONTENTS * dircontp )
{
    assert(dircontp);
    printf("DIRCONTENTS (%p) for dir %s\n", dircontp,    dircontp->dirname);
    printf("               Mutex:                 %p\n", &dircontp->mut.mutex);
    printf("               Num. filenames/images  %d\n", dircontp->noi_num);
    for( int idx = 0; idx < dircontp->noi_num; idx++)
    {
        if( dircontp->noi_vec[idx].is_imagedata )
        {
            printf("---> noid[%1d] image %p\n", idx, dircontp->noi_vec[idx].noir.img);
        }
        else
        {
            char * filename = dircontp->noi_vec[idx].noir.filename;
            printf("---> noid[%1d] -> name %p (%s)\n", idx, filename, (filename ? filename : ""));
        }
    }

    show_nav_info(&dircontp->nav);

    printf("               rpl (IMAGE*)           %p\n", dircontp->rpl);
    printf("               ridx                   %d\n", dircontp->ridx);
    printf("               widx                   %d\n", dircontp->widx);
    printf("               completed              %s\n", (dircontp->completed? "true" : "false"));

    fflush(stdout);

    return;
}


// ========================== Comparison functions ========================== //
// Keep in mind, that these functions will be called on
// the arguments are coming from the the statemachine as ACTION's.
// That means, that they are void pointers.
// And they come from a GRID, which also means void pointers.
// Hence there are some cast-and-dereferences done in each of these
// comparison functions (compare with the IMAGE-compare-functions.)
// These dereferncing steps might be done via specxial functions,
// as in commands.c, but for now it's doen directly in these
// DirContents-comparison functions.
// ========================================================================== //

int dircontents_compare_dirname( const DIRCONTENTS * dc1, const DIRCONTENTS * dc2 )
{
    assert(dc1);
    assert(dc2);

    return strcmp(dc1->dirname, dc2->dirname);
}



/*
    Compare functions for usage with GRID-sort,
    thats why ** pointers are used...
*/

/** \brief Compare two DIRCONTENTS' by their dirname.
*/
int comp_dircontents_by_dirname( const void * dc_v_1, const void * dc_v_2 )
{
    assert(dc_v_1);
    assert(dc_v_2);

    const DIRCONTENTS * dc_1 = * (DIRCONTENTS **) dc_v_1;
    const DIRCONTENTS * dc_2 = * (DIRCONTENTS **) dc_v_2;

    return dircontents_compare_dirname(dc_1, dc_2);

}


// =============================================================================
// =============================================================================


int dircontents_num_of_entries( const DIRCONTENTS * dc )
{
    assert(dc);
    return dc->noi_num;
}



int dircontents_compare_num_entries( const void * dc1_v, const void * dc2_v )
{
    const DIRCONTENTS * dc1 = * (const DIRCONTENTS ** ) dc1_v;
    const DIRCONTENTS * dc2 = * (const DIRCONTENTS ** ) dc2_v;

    assert(dc1);
    assert(dc2);

    int cmpres = 0;

    int num_1  = dircontents_num_of_entries(dc1);
    int num_2  = dircontents_num_of_entries(dc2);

    if( num_1 > num_2 )
    {
        cmpres = 1;
    }
    else if( num_1 < num_2 )
    {
        cmpres = -1;
    }

    return cmpres;
}



/** \brief Compare modtime of directory with modtime of DIRCONTENTS.

    Usecase:
    This comparison is used to find out, if the directory contents has changed
    externally (not done by plio).

    Precondition (for this use case to work) is
    that the modtime of DIRCONTENTS is updated after plio changed the
    directory.
*/
bool dircontents_did_change( const DIRCONTENTS * dc )
{
    struct timespec origtime; // frehsly read directory modtime
    bool res = false;

    modtime_of_file_hr(dc->dirname, &origtime); // real directory data

    if( cmp_timespec(&origtime, &dc->modtime) == 1 )
    {
        res = true;
    }

    return res;
}



/** \brief Update modtime of DIRCONTENTS with the modtime of the directry represented by the DIRCONTENTS.
*/
void dircontents_update_modtime( DIRCONTENTS * dc )
{
    struct timespec origtime;

    modtime_of_file_hr(dc->dirname, &origtime); // get origdata from actual directory
    copy_timespec(&dc->modtime, &origtime); // update Dircontent's modtime-entry

    return;
}



