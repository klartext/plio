/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <string.h>
#include <assert.h>

#include "collection.h"
#include "display.h"
#include "display_dir.h"
#include "display_image.h"
#include "display_index.h"
#include "render.h"
#include "threadsupport.h"
#include "tools.h"
#include "geometry.h"



void set_rendering_time( struct timespec * tsp )
{
    int res = 0;

    res = clock_gettime(CLOCK_REALTIME, tsp);
    assert( res == 0 );

    return;
}


int render_dirsview( APP * app, INDEX_LAYOUT   * layout, DIR_COLLECTION * dcollp )
{
    DIRCONTENTS * currdir = NULL;

    SELECTED_ITEMS title  = make_dirsview_title();
    SELECTED_ITEMS footer = make_dirsview_footer();

    currdir = grid_get_payload(dcollp->nav.grid);

    clear_screen(app, false);

    if( grid_length(dcollp->nav.grid) > 0 )
    {
        footer.curr = (currdir->completed)  ?  currdir->nav.grid->used_length  :  currdir->nav.grid->veclen;

        if( currdir->completed )
        {
            footer.actual_sing = "Image:  ";
            footer.actual_plur = "Images: ";
        }
        footer.wrap_end = currdir->dirname;

        render_header_and_footer(app, layout, &dcollp->nav, &title, &footer); // RENDER
        draw_dirsview_thumbs_frames(app, layout, &dcollp->nav);

    }
    else
    {
        fprintf(stderr, "%s(): no entries in GRID\n", __FUNCTION__);
    }

    SDL_RenderPresent(app->renderer);

    return 0;
}



int render_indexview( APP * app, INDEX_LAYOUT   * layout, DIR_COLLECTION * dcollp )
{
    DIRCONTENTS * currdir = NULL;

    SELECTED_ITEMS title  = make_indexview_title();
    SELECTED_ITEMS footer = make_indexview_footer();

    currdir = grid_get_payload(dcollp->nav.grid);

    clear_screen(app, false);

    if( grid_length(currdir->nav.grid) > 0 )
    {
        IMAGE * currimg = grid_get_payload(currdir->nav.grid);

        if( currimg != NULL )
        {
            footer.wrap_end = currimg->abspath;

            if( currdir->completed )
            {
                title.src_sing   = "";
                title.src_plur   = "";
                title.wrap_start = "";
                title.wrap_end   = "";
                title.max_fmt    = NULL;
            }

            render_header_and_footer(app, layout, &currdir->nav, &title, &footer); // RENDER
            render_thumbindex_from_range(app, layout, &currdir->nav, &title, &footer); // RENDER

        }
    }
    else
    {
        fprintf(stderr, "%s(): no entries in GRID\n", __FUNCTION__);
    }

    SDL_RenderPresent(app->renderer);

    return 0;
}




int render_imageview( APP * app, INDEX_LAYOUT   * layout, DIR_COLLECTION * dcollp, IMAGE ** lastimg_addr )
{
    int win_w = 0;
    int win_h = 0;

    DIRCONTENTS * currdir = NULL;

    SDL_GetWindowSize(app->window, &win_w, &win_h);

    currdir = grid_get_payload(dcollp->nav.grid);
    calc_grid_navigation(win_w, win_h, &currdir->nav, layout);

    if( grid_length(currdir->nav.grid) > 0 )
    {
        IMAGE * currimg = grid_get_payload(currdir->nav.grid);

        replace_lastimage_if_necessary(app->renderer, *lastimg_addr, currimg);
        *lastimg_addr = currimg; // needed for freeing the image in the next round

        display_imageview(app, win_w, win_h, currdir->nav.grid); // RENDER
    }
    else
    {
        HEREMSG("   __RENDER_IMAGE-VIEW__ grid_length(currdir->nav.grid)  == 0\n");
    }

    return 0;
}



int render_any( void * vdata )
{
    THREAD_DATA * data = vdata;

    APP            * app     = data->app;
    INDEX_LAYOUT   * layout  = data->layout;
    DIR_COLLECTION * dcollp  = data->dcollp;
    char * homedir = NULL;
    char homebuf[PATH_MAX];
    char timebuf[20];

    static IMAGE * lastimg = NULL;

    if( data->needs_rendering == false )
    {
        return 0;
    }

    if( dcollp->dump_screen )
    {
        // write screen-dump with timestamp to homedir
        // -------------------------------------------
        homedir = getenv("HOME");
        if( homedir != NULL )
        {
            get_clock(timebuf);

            homebuf[0] = '\0';
            strcat(homebuf, homedir);
            strcat(homebuf, "/pliodump-");
            strcat(homebuf, timebuf);
            strcat(homebuf, ".png");

            windowdump_png(app->renderer, homebuf);
            dcollp->dump_screen = false;
        }
        else
        {
            fprintf(stderr, "Environment variable HOME not set - can't save screendump.\n");
        }
    }

    // Scaling the layout for the selected display (monitor)
    // -----------------------------------------------------
    double current_scale = get_dpi_scaling(app->current_display);
    INDEX_LAYOUT scaled_layout = scale_index_layout(layout, current_scale);


    // calling to the correct (sub)renderer
    // ------------------------------------
    switch (dcollp->rsel)
    {
        case RENDER_NO_UPDATE:
        {
            ;
        }
        break;

        case RENDER_DIRSVIEW:
        {
            render_dirsview(app, &scaled_layout, dcollp);
        }
        break;

        case RENDER_INDEXVIEW:
        {
            render_indexview(app, &scaled_layout, dcollp);
        }
        break;

        case RENDER_IMAGEVIEW:
        {
            render_imageview(app, &scaled_layout, dcollp, &lastimg);
        }
        break;

        default:
            fprintf(stderr, "Unknown rsel in %s()\n", __FUNCTION__);
            abort();
    }

    //dcollp->rsel = RENDER_NO_UPDATE; // all things done, nothing do do here

    set_rendering_time(&dcollp->t_last_rendering);

    data->needs_rendering = false;

    return 0;
}
