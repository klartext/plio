/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __TOOLS__
#define __TOOLS__

#include <limits.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#include "clock_filetime.h"
#include "image.h"
#include "common.h"
//#include "collection.h"
//#include "dircontents.h"


#define PRINTPTR(foo)  printf("     %-30s: %p\n", #foo,  foo)
#define PRINTSTR(foo)  if(foo) printf("     %-30s: %s\n", #foo,  foo)
#define PRINTBOOL(foo) printf("     %-30s: %s\n", #foo,  (foo == true ? "true" : "false"))
#define PRINTINT(foo)  printf("     %-30s: %4d\n", #foo,  foo)
#define PRINTLU(foo) printf("%40s: %4lu\n", #foo,  foo)

#define HERE() { print_clock(stdout, ": "); printf("====> %s, line %d (function %s)\n", __FILE__, __LINE__, __FUNCTION__); fflush(stdout); }
#define HEREMSG(msg) { print_clock(stdout, ": "); printf("====> %s, line %d (function %s) ----> %s\n", __FILE__, __LINE__, __FUNCTION__, msg); fflush(stdout); }

int random_int( int maxval );
void wait_with_message(unsigned int wait_time);


//typedef bool (*TEST_IS_EMPTY)(void * item);


/*
void invoke_functions_on_collection( DIR_COLLECTION * dcollp,
                                        void (*collfun)(DIR_COLLECTION * dcollp),
                                        void (*contfun)(DIRCONTENTS    * dcontp),
                                        void (*imgfun)(IMAGE           * dcontp)
                                   );
*/


bool replace_lastimage_if_necessary( SDL_Renderer * renderer, IMAGE * lastimg, IMAGE * currimg ); // function maybe not needed anmyore

void windowdump_png(SDL_Renderer * renderer, char * outname);

void print_bits(int value);
void print_bits_nl(int value);
void print_bits_message(char * prepend, int value, char * append);

void show_surface_infos( SDL_Surface * surf );

void print_rect( SDL_Rect * rect );

void switch_ints( int * first, int * second );

void show_display_info( APP * app );

#endif
