/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __SELECTION__
#define __SELECTION__

#include <stdio.h>
#include <stdbool.h>

char * singularplural( int num, char * singular, char * plural );

// Functions to indicate boolean status change
// -------------------------------------------
bool switched_on( bool before, bool after );
bool switched_off( bool before, bool after );
bool did_not_switch( bool before, bool after );
bool did_switch( bool before, bool after );

#endif
