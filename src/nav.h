/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __NAV__
#define __NAV__

#include "grid.h"
#include "range.h"
#include "counter.h"
#include "indexlayout.h"

// the sort status is 'unsorted' if either no sort command has been used,
// or if commands that change the order 'by hand'a (no sort commands) has been used.
// If for example after sorting the X- or Y-Exchange functions have been used,
// the is_sorted flag is false. This is also the case, if one of these commands has been used twice,
// and the order has been restored.
// This will not be recorded.
typedef struct sort_status SORT_STATUS;
struct sort_status
{
    bool is_sorted; // is true if a sort command has been used and the order has not been destroyed

    int  used_sort_command; // the used sort command, e.g. CMD_SORT_ASPECT
    bool inverted;          // true iff CMD_REVERSE_ORDER has used an odd number of times
};

// --------------------------------------------------------------------

typedef struct grid_navigation NAV;
struct grid_navigation
{
    GRID   * grid;       // GRID referring to the images-vector
    BCOUNT   scroll;     // scroll index for the GRID
    RANGE    view;    // the selected range of images that are to be viewed
    INDEX_DIM view_dim;  // logical dimensions of the Thumbview

    SORT_STATUS sort_status; // indication of status of GRID sorting
};

// --------------------------------------------------------------------

void show_nav_info( NAV * nav );
void show_nav( NAV * nav );

void nav_move_right( NAV * navp );
void nav_move_left( NAV * navp );
void nav_move_up( NAV * navp );
void nav_move_down( NAV * navp );
void nav_exchange_grid_entries_horizontal(NAV * navp );
void nav_exchange_grid_entries_vertical(NAV * navp );


void nav_reverse_order( NAV * navp );
void nav_first_pos( NAV * navp );
void nav_goto_pos( NAV * navp, int newpos_idx );
void nav_last_pos(NAV * navp );

#endif
