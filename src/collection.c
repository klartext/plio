/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "collection.h"
#include "folder.h"



// STRING_VEC of dirname's -> for each dir in STRING_VEC ...
DIR_COLLECTION create_dir_collection( STRING_VEC * startdirs_uniq, bool recursive, bool mogrify_info )
{
    DIR_COLLECTION collection;
    bzero(&collection, sizeof(collection));

    STRING_VEC subdirs_temp;
    STRING_VEC dirs;

    int idx = 0;
    int write_idx = 0;

    DIRCONTENTS dirtmp;

    // Read dir names (including startdir) and sort the by name
    // --------------------------------------------------------
    subdirs_temp = collect_dirs_subdirs_for_dirsvec(startdirs_uniq, recursive);
    dirs      = stringvec_sort_uniq(&subdirs_temp); // free of doublettes
    free_stringvec(&subdirs_temp);

    collection.dircontvec = malloc(dirs.used * sizeof(DIRCONTENTS));
    if( collection.dircontvec == NULL )
    {
        fprintf(stderr, "Could not allocate mem for DIR_COLLECTION - exiuting now\n");
        exit(EXIT_FAILURE);
    }

    for( idx = 0; idx < dirs.used; idx++ )
    {
        dirtmp = make_dircontents(dirs.vec[idx]);
        if( mogrify_info )
        {
            set_mogrify_info(&dirtmp);
        }

        if( dirtmp.valid )
        {
            collection.dircontvec[write_idx] = dirtmp;
            ++write_idx;
        }
        else
        {
            fprintf(stderr, "*** directory ignored: %s\n", dirs.vec[idx]);
        }
    }

    collection.dc_num = write_idx;
    free_stringvec(&dirs);

    // creating the Grid for the DIRCONTENTS
    // -----------------------------------------
    collection.nav.grid = create_grid(collection.dc_num, 1); // the 1 is dummy
    for( idx = 0; idx < collection.dc_num; idx++ ) // TODO: check, if this is outdated after RPL is IMAGE **
    {
        collection.nav.grid->payload[idx] = collection.dircontvec[idx].rpl;
    }
    collection.nav.grid->used_length = collection.dc_num;

    return collection;
}



void free_dir_collection( DIR_COLLECTION * dcollp )
{
    if( dcollp == NULL )
    {
        return;
    }

    for( int idx = 0; idx < dcollp->dc_num; idx++ )
    {
        free_dircontents(&dcollp->dircontvec[idx]);
    }
    free(dcollp->dircontvec);
    dcollp->dc_num = 0;

    if( dcollp->nav.grid )
    {
        destroy_grid(dcollp->nav.grid);
    }

    dcollp->completed = 0;

    return;
}


void show_dircollection_info( DIR_COLLECTION * dcollp, bool with_dircontents )
{
    assert(dcollp);

    printf("DIR_COLLECTION info for dircollection %p\n", dcollp);
    printf("               dircontvec:            %p\n", dcollp->dircontvec);
    printf("               dc_num:                %d\n", dcollp->dc_num);

    show_nav_info(&dcollp->nav);

    if( with_dircontents )
    {
        putchar('\n');
        for( int idx = 0; idx < dcollp->dc_num; idx++ )
        {
            show_dircontents_info(&dcollp->dircontvec[idx]);
            putchar('\n');
        }
    }

    fflush(stdout);

    return;
}


int extract_dircoll_rsel( DIR_COLLECTION * dircoll )
{
    return dircoll->rsel;
}
