/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __FIB__
#define __FIB__

#include <stdbool.h>

#include <FreeImage.h>

// This structure / type is (as kind of adapter) used to
// 1) read image data and size of the orig image from file (orig image size is available)
// 2) read a thumbnail from file, but provide the size of the original image also
typedef struct image_or_thumb_reading IMG_META;
struct image_or_thumb_reading
{
    int img_w; // width of the original image (not the thumb)
    int img_h; // width of the original image (not the thumb)
    FIBITMAP * fib; // either orig or thumb
};


// Function Prototypes
// -------------------
bool filetype_is_valid_fif( char * filename ); // Would FreeImage-Lib accept the file?

FIBITMAP * get_freeimage_bitmap(char *filename);
IMG_META read_imagefile_resize( char * file_abspath, int thumb_size );

void show_freeimage_bitmap_properties( FIBITMAP * fib_bitmap, const char * prepend );

#endif
