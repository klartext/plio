/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __COMMAND_ENUMS__
#define __COMMAND_ENUMS__

enum internal_cmd
{
    ICMD_KEY_TO_CMD_DIRVIEW,
    ICMD_HANDLE_CMD_DIRVIEW,
    ICMD_UPDATE_IMAGE,
};

enum scr_cmd { CMD_NOTHING, CMD_QUIT,
               CMD_SWITCH_DIRVIEW, CMD_SWITCH_INDEXVIEW, CMD_SWITCH_IMAGEVIEW,
               CMD_DOWN, CMD_UP, CMD_RIGHT, CMD_LEFT,
               CMD_SORT_WIDTH, CMD_SORT_HEIGHT, CMD_SORT_ASPECT, CMD_SORT_SIZE, CMD_SORT_NAME, CMD_SORT_NAMESTRUCTURE, CMD_SORT_MTIME,

               CMD_SORT_COLOR_LUMINANCE,
               CMD_SORT_COLOR_LAB_AB,
               CMD_SORT_COLOR_LAB_AB_ATAN2,
               CMD_SORT_COLOR_LAB_B,
               CMD_SORT_COLOR,

               CMD_TOGGLE_GENERAL_IMAGE_MARK,
               CMD_UNSET_GENERAL_IMAGE_MARK,

               CMD_REVERSE_ORDER,
               CMD_PRINT_NAMES,
               CMD_PRINT_NAMES_OF_MARKED ,
               CMD_OPEN_IMAGEFILE_EXTERN,

               CMD_FIRST_POS, CMD_LAST_POS,
               CMD_NEXT_MARKED_POS, CMD_PREV_MARKED_POS,
               CMD_PAGE_UP, CMD_PAGE_DOWN,
               CMD_DECR_DIR_INDEX, CMD_INCR_DIR_INDEX,
               CMD_SCREENDUMP,
               CMD_EXCHANGE_GRID_ENTRIES,
               CMD_EXCHANGE_GRID_ENTRIES_VERTICAL,
               CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE,
               CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST,
               CMD_RENAME_FILES,
               CMD_RENAME_FILES_REPLACE_BASENAME,
               CMD_PREPARE_XY_GRID,

               CMD_FLIP_HOR,
               CMD_FLIP_VERT,
               CMD_ROTATE_CCW,
               CMD_ROTATE_CW,

               CMD_TOGGLE_FULLSCREEN,


               CMD_LIST_END
             }; // CMD_LIST_END must be the last entry, for allocating purposes

#endif
