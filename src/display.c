/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <libgen.h> // PRINT-Command
#include <string.h> // PRINT-Command
#include <assert.h>

#include "display.h"
#include "geometry.h"
#include "macros.h"
#include "config.h"



/** \brief Draw a frame (with specific color and thickness) around position (xpos,ypos) of size (width,height).
*/
void draw_frame( SDL_Renderer * renderer, Uint8 * col, int xpos, int ypos, int width, int height, int thickness )
{
    SDL_Rect  frame;
    int res = 0;

    frame.x = xpos;
    frame.y = ypos;
    frame.w = width;
    frame.h = height;

    res = SDL_SetRenderDrawColor(renderer, col[0], col[1], col[2], col[3]);
    sdl_errmsg_on_error(res);

    for( int add = 0; add < thickness; add++ )
    {
        frame.w += 2;
        frame.h += 2;
        frame.x -= 1;
        frame.y -= 1;
        SDL_RenderDrawRect(renderer, &frame);
    }

    return;
}



/** \brief Draw thick frame around the active image.
*/
void draw_thick_frame_for_active_image( APP * app, INDEX_LAYOUT * layout, int display_idx, GRID * image_grid, int win_w, int win_h )
{
    int thickness  = layout->selection_frame_thickness;
    void * payload = grid_get_payload_raw(image_grid);
    SDL_Rect rect  = get_index_position(layout, payload, display_idx, win_w, win_h); // where to draw

    draw_frame(app->renderer, app->emphcol, rect.x, rect.y, layout->thumb_sidelength, layout->thumb_sidelength, thickness); // do draw

    return;
}



/** \brief Draw light frame around each image-thumb (1 pixel width).
*/
void draw_thumb_frames( SDL_Renderer * renderer, Uint8 * drawcol, INDEX_LAYOUT * layout, int anzahl, int win_w, int win_h ) // might switch from 'int anzahl' to 'GRID * image_grid'
{
    SDL_Rect rect;

    for( int idx = 0; idx < anzahl; idx++)
    {
        rect = get_index_position(layout, NULL, idx, win_w, win_h);
        draw_frame(renderer, drawcol, rect.x, rect.y, layout->thumb_sidelength, layout->thumb_sidelength, 1);
    }

    return;
}



void display_marks( SDL_Renderer * renderer, INDEX_LAYOUT * layout, GRID * image_grid, RANGE * range, int win_w, int win_h )
{
    int idx = 0;
    int num_pics = range->max - range->min + 1;

    IMAGE * img = NULL;
    SDL_Rect rect;

    Uint8 marking_color[COLNUM] = {0x10, 0x10, 0xAA, 0xFF };
    const int marking_thickness = 10;

    for( idx = 0; idx < num_pics; idx++ )
    {
        img     = grid_get_payload_by_index_raw(image_grid, range->min + idx);

        rect = get_index_position(layout, img, idx, win_w, win_h);
        if( is_marked_general(&img->mark) )
        {
            draw_frame(renderer, marking_color, rect.x, rect.y, layout->thumb_sidelength, layout->thumb_sidelength, marking_thickness);
        }
    }

    return;
}



/** \brief Clear the screen with bgcol(or) (if present == true, then also call SDL_RenderPresent()).
*/
void clear_screen( APP * app, bool present )
{
    int res = 0;

    res = SDL_SetRenderDrawColor(app->renderer, app->bgcol[0], app->bgcol[1], app->bgcol[2], app->bgcol[3]);
    sdl_errmsg_on_error(res);

    res = SDL_RenderClear(app->renderer);
    sdl_errmsg_on_error(res);

    if( present )
    {
        SDL_RenderPresent(app->renderer);
    }

    return;
}



static void display_surface( SDL_Renderer * renderer, SDL_Surface * surface, int xpos, int ypos )
{
    assert(renderer);
    assert(surface);

    int res = 0;
    SDL_Texture * texture = NULL;

    SDL_Rect dest;
    dest.w = surface->w;
    dest.h = surface->h;
    dest.y = ypos;
    dest.x = xpos;

    texture = SDL_CreateTextureFromSurface(renderer, surface);

    if( texture == NULL )
    {
        fprintf(stderr, "%s(): warning, no texture could be created.\n", __FUNCTION__);
        return;
    }

    res = SDL_RenderCopy(renderer, texture, NULL, &dest );
    sdl_errmsg_on_error(res); // ??

    SDL_DestroyTexture(texture);

    return;
}



void display_text( SDL_Renderer * renderer, TTF_Font * font, char * text, SDL_Color color, int xpos, int ypos)
{
    assert(renderer);
    if( font == NULL )
    {
        fprintf(stderr, "No font available, text (\"%s\") not displayed\n", text);
        return;
    }

    SDL_Surface * surface = TTF_RenderUTF8_Blended(font, text, color);

    display_surface(renderer, surface, xpos, ypos); // the surface is created from SDL2-TTF -> crash

    SDL_FreeSurface(surface);

    return;
}



/** \brief Show the index number of the first image of a row on the left margin.
*/
void show_index_number( APP * app, int win_w, int win_h, INDEX_LAYOUT * layout, GRID * image_grid, RANGE * imgidx_range )
{
    SDL_Rect thpos; // thumb-position
SDL_Color white = { 0xFF, 0xFF , 0xFF, 0xFF };

    char textbuf[PATH_MAX];
    bzero(textbuf, sizeof(textbuf));

    for( int idx = imgidx_range->min; idx < imgidx_range->max; idx += image_grid->xlen )
    {
        textbuf[0] = '0';
        sprintf(textbuf, "%5d", (INDEX_START_ZERO ? idx : idx + 1));

        thpos = get_index_position( layout, NULL, idx - imgidx_range->min, win_w, win_h );
        int xpos = layout->outer_frame / 2;
        xpos = 0;

        display_text(app->renderer, app->fontvec[app->current_display].font, textbuf, white, xpos, thpos.y);
    }

    return;
}



/** \brief Renderfunction for DirsView and IndexView.
*/
void render_header_and_footer( APP * app, INDEX_LAYOUT * layout, NAV * navp, SELECTED_ITEMS * title, SELECTED_ITEMS * footer )
{
    on_NULL_return(app);
    on_NULL_return(layout);
    on_NULL_return(navp);
    on_NULL_return(title);
    on_NULL_return(footer);

    GRID  * dcgrid = navp->grid;
    RANGE * range  = &navp->view;

    int win_w = 0;
    int win_h = 0;

    SDL_GetWindowSize(app->window, &win_w, &win_h);
    calc_grid_navigation(win_w, win_h, navp, layout);

    char textbuf[PATH_MAX];
    bzero(textbuf, sizeof(textbuf));

    int xpos_title = layout->outer_frame / 2;
    int ypos_title = layout->outer_frame/2;

    int xpos_footer = 2 * layout->outer_frame;
    int ypos_footer = win_h - layout->outer_frame;

    SDL_Color white = { 0xFF, 0xFF , 0xFF, 0xFF };


    lock_grid(dcgrid); // LOCK grid mutex

    title->curr = dcgrid->used_length;
    title->max  = dcgrid->veclen;

    string_of_selected_items(textbuf, title);
    display_text(app->renderer, app->fontvec[app->current_display].font, textbuf, white, xpos_title, ypos_title);

    // Show footer (filename)
    // ----------------------
    textbuf[0] = '\0';
    string_of_selected_items(textbuf, footer);
    display_text(app->renderer, app->fontvec[app->current_display].font, textbuf, white, xpos_footer, ypos_footer);
    show_index_number(app, win_w, win_h, layout, dcgrid, range); // show index number

    unlock_grid(dcgrid); // UNLOCK grid mutex

    return;
}



/** \brief RenderCopy the thumbnail of the image to the destination 'dest'.

    - if the texture of the thumbnail is available, use it (Render-Copy)
    - if the texture is not available, but the surface is available, first create the texture and then use it
    - if neither texture nor surface of the thumbnail is available, just print an error message to stderr
*/
void render_copy_thumbnail( SDL_Renderer * renderer, IMAGE * img, SDL_Rect * dest )
{
    int res = 0;

    SDL_FPoint center = { 0.0, 0.0 };
    SDL_FRect fdest;

    ROTATION rot = orientation_get_rotate(img);
    SDL_RendererFlip flip = flip_of_image(img);

    double angle = 360.0 - orientation_rot_degrees(img); // SDL2 rotates in negative mathematical direction!

    // if not already existing, create thumb-texture from thumb-surface
    // ----------------------------------------------------------------
    if( img->thumb.texture == NULL && img->thumb.surface != NULL )
    {
        img->thumb.texture = SDL_CreateTextureFromSurface(renderer, img->thumb.surface);
    }

    // RenderCopy the texture to the Renderer
    // --------------------------------------
    if( img->thumb.texture )
    {
        int texture_w = 0;
        int texture_h = 0;
        SDL_QueryTexture(img->thumb.texture, NULL, NULL, &texture_w, &texture_h);

        fdest.x = dest->x;
        fdest.y = dest->y;
        fdest.w = dest->w;
        fdest.h = dest->h;

        fdest = rot_offset_correction(&fdest, rot);

        res = SDL_RenderCopyExF(renderer, img->thumb.texture, NULL, &fdest, angle, &center, flip );
        sdl_errmsg_on_error(res);
    }
    else
    {
        fprintf(stderr, "%s(): thumbnail: no texture for the thumbnail available\n", __FUNCTION__);
    }

    return;
}



/** \brief Display the image 'img' trimmed to fit into the window.

    Flipping of images is alco done here.

    This function uses SDL_RenderCopyExF(),
    see https://wiki.libsdl.org/SDL2/SDL_RenderCopyExF
    for documentation.

*/
void display_image_trimmed( SDL_Renderer * renderer, IMAGE * img, int win_w, int win_h )
{
    SDL_FRect fdst;
    SDL_FPoint center = { 0.0, 0.0 };

    int res = 0;

    ROTATION rot = orientation_get_rotate(img);
    SDL_RendererFlip flip = flip_of_image(img);

    double angle = 360.0 - orientation_rot_degrees(img); // SDL2 rotates in negative mathematical direction!

    if( img->texture )
    {
        fdst = aspect_corr_with_rot_corr(img->width, img->height, win_w, win_h, rot); // adapting width and height
        fdst = rot_offset_correction(&fdst, rot);

        res = SDL_RenderCopyExF(renderer, img->texture, NULL, &fdst, angle, &center, flip);
        if( res != 0 )
        {
            fprintf(stderr, "%s(): SDL_RenderCopy() failed: %s\n", __FUNCTION__, SDL_GetError() );
        }
        sdl_errmsg_on_error(res);
    }
    else // this case might occur, if the texture has been freed to save RAM (texture uncashed) - need to relaod image then
    {
        fprintf(stderr, "error: no texture for file %s\n", img->abspath);
    }

    return;
}



/** \brief Displaying all Thumbs.

    If the texture of the image is not available,
    it will be tried to use the surface, if that is available.
    In this case, the texture will be created from the surface
    on the fly, without destroying the surface.
*/
void display_index_thumbs_of_grid_via_range( SDL_Renderer * renderer, INDEX_LAYOUT * layout, GRID * image_grid, RANGE * range, int win_w, int win_h )
{
    int idx = 0;
    int num_pics = range->max - range->min + 1;

    IMAGE * img = NULL;

    SDL_Rect tmprect;

    for( idx = 0; idx < num_pics; idx++ )
    {
        img     = grid_get_payload_by_index_raw(image_grid, range->min + idx);

        tmprect = get_index_position(layout, img, idx, win_w, win_h);
        render_copy_thumbnail(renderer, img, &tmprect); // TODO: Mutex für Renderer?!?!?!
    }

    return;
}


void toggle_fullscreen( APP * app )
{
    int width  = 0;
    int height = 0;
    int res = 0;

    SDL_GetRendererOutputSize(app->renderer, &width, &height);

    if( width == INITIAL_WINDOW_WIDTH && height == INITIAL_WINDOW_HEIGHT )
    {
        res = SDL_SetWindowFullscreen(app->window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        if(res != 0)
        {
            fprintf(stderr, "fullscreen mode not available: %s\n", SDL_GetError());
        }
    }
    else
    {
        res = SDL_SetWindowFullscreen(app->window, 0);
        if(res != 0)
        {
            fprintf(stderr, "leaving fullscreen mode not possible: %s\n", SDL_GetError());
        }
    }


    return;
}



/** \brief Get diagonal display scaling. (display size divided by base dpi-value of 100.0).

    This function uses SDL_GetDisplayDPI().

    There is a comment to this function on the sdl-wiki:

    <em>
    WARNING: This reports the DPI that the hardware reports, and it is not
    always reliable! It is almost always better to use SDL_GetWindowSize() to
    find the window size, which might be in logical points instead of pixels,
    and then SDL_GL_GetDrawableSize(), SDL_Vulkan_GetDrawableSize(),
    SDL_Metal_GetDrawableSize(), or SDL_GetRendererOutputSize(), and compare
    the two values to get an actual scaling value between the two. We will be
    rethinking how high-dpi details should be managed in SDL3 to make things
    more consistent, reliable, and clear.
    </em>

    Source: https://wiki.libsdl.org/SDL2/SDL_GetDisplayDPI

    Source-Date: 2023-05-14

*/
double get_dpi_scaling( int display_idx )
{
    float ddpi;
    float hdpi;
    float vdpi;
    double scaling = 1.0;

    const int base_dpi = 90;
    // The layout parameters in config.h were set to be useful with
    // these monitor settings:
    // 1366 pixel x 768 pixel and size of 344mm x 194mm
    // 1920 pixel x 1080 pixel and size of 531mm x 299mm
    // The current layout settings in config.h have been chosen
    // for the second screen, which has about 90 dpi.
    // Hence base_dpi is set to 90. With the former value of 100,
    // parts of the index-text to the left of the first column of thumbs
    // were covered by the green cursor.
    // (Readjusting the values in config.h could also have worked,
    // but the relative dimensions are well chosen and should not be mixed up.)

    int res = 0;

    res = SDL_GetDisplayDPI(display_idx, &ddpi, &hdpi, &vdpi);
    if( res != 0 )
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        ddpi = base_dpi; // workaround / fallback
    }

    scaling = (double) ddpi / base_dpi;

    return scaling;
}



/** \brief Get the maximum of all scaling values and 1.0.
*/
double get_max_dpi_scaling( APP * app )
{
    int    idx = 0;
    double max_scale = 1.0;

    for( idx = 0; idx < app->num_displays; idx++ )
    {
        max_scale = MAX(max_scale, get_dpi_scaling(idx));
    }

    return max_scale;
}
