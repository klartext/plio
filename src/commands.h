/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __COMMANDS__
#define __COMMANDS__

#include "ctrlloop_dir.h"
#include "collection.h"
#include "ustate.h"

void handle_basic_commands( DIR_COLLECTION * dcollp, USTATE * stp);

void dump_screen( void * vdata );
void toggle_general_image_mark( void * vdata );
void clear_general_image_marks( void * vdata );
void start_imagereader_thread( void * vdata );
void rename_files_of_index( void * vdata );
void rename_files_of_index_noprepend( void * vdata );

void cmd_move_right( void * vdata );
void cmd_move_left( void * vdata );
void cmd_move_up( void * vdata );
void cmd_move_down( void * vdata );
void cmd_incr_dir_index( void * vdata );
void cmd_decr_dir_index( void * vdata );
void cmd_exchange_grid_entries( void * vdata );
void cmd_exchange_grid_entries_vertical( void * vdata );
void cmd_replace_zero_with_curr_shift_right( void * vdata );
void cmd_replace_last_with_curr_shift_left( void * vdata );
void cmd_reverse_order( void * vdata );
void cmd_first_pos( void * vdata );
void cmd_last_pos( void * vdata );
void cmd_next_marked_pos( void * vdata );
void cmd_prev_marked_pos( void * vdata );
void cmd_print_dirs_names( void * vdata );
void cmd_print_images_names( void * vdata );
void cmd_print_images_names_if_marked( void * vdata );
void cmd_print_image_name( void * vdata );
void cmd_toggle_hflip( void * vdata );
void cmd_toggle_vflip( void * vdata );
void cmd_rotate_ccw( void * vdata );
void cmd_rotate_cw( void * vdata );
void cmd_open_image_external( void * vdata );
void cmd_toggle_fullscreen( void * vdata );

void cmd_img_sort_width( void * vdata );
void cmd_img_sort_height( void * vdata );
void cmd_img_sort_size_then_aspect( void * vdata );
void cmd_img_sort_aspect( void * vdata );
void cmd_img_sort_name( void * vdata );
void cmd_img_sort_namestructure( void * vdata );
void cmd_img_sort_by_modtime( void * vdata );
void cmd_img_sort_by_luminance( void * vdata );
void cmd_img_sort_by_lab_ab( void * vdata );
void cmd_img_sort_by_lab_ab_atan2( void * vdata );

void cmd_dir_sort_by_name( void * vdata );
void cmd_dir_sort_by_num( void * vdata );

#endif
