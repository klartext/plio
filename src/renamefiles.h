/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __RENAMEFILES__
#define __RENAMEFILES__

#include <stdbool.h>


typedef struct _name_mapping_ NAME_MAPPING;
struct _name_mapping_
{
    char * origname;
    char * newname;
};

typedef struct _name_map_ NAME_MAP;
struct _name_map_
{
    NAME_MAPPING * mapvec;
    int len; // length of the map
};


NAME_MAP make_name_map( int num_files );
void cleanup_name_map( NAME_MAP * mapaddr );

void insert_name_mapping_at_index( NAME_MAP * map, char * orig, char * new, int idx );
void read_name_mapping_at_index( NAME_MAP * map, char ** orig_ptr, char ** new_ptr, int idx );

void show_name_mapping( NAME_MAP * map );
bool new_names_fit_limits( NAME_MAP * map );
void apply_renaming( NAME_MAP * map );

#endif
