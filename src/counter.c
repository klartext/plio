/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

/** \file counter.c
    Bounded counters.

    Bounded counters: counters with a minimal and maximal value.

    Use case: index values for array addressing, especially for
    input driven increment/decrement (e.g. keyboard commands).

    Because of the min- and max-value, the index is held in a
    safe range.
    Attempts to increment or decrement the index beyond it's minimnal or
    maximal value will be ignored that way.
*/

#include <stdio.h>

#include "counter.h"
#include "macros.h"


/** \brief Increment counter by 1.

    If the counter is already at it's maximum value, this function has no effect.
*/
int counter_increment(BCOUNT * counter)
{
    errexit_on_NULL(counter);

    if( counter->count + 1 <= counter->max )
    {
        ++counter->count;
    }

    return counter->count;
}


/** \brief Decrement counter by 1.

    If the counter is already at it's minimum value, this function has no effect.
*/
int counter_decrement(BCOUNT * counter)
{
    errexit_on_NULL(counter);

    if( counter->count - 1 >= counter->min )
    {
        --counter->count;
    }

    return counter->count;
}


/** \brief Get current counter value.
*/
int counter_get(BCOUNT * counter)
{
    errexit_on_NULL(counter);

    return counter->count;
}


/** \brief Set the counter's value to a certain value.

   If the new value is outside the [min, max] range of the counter,
   the value of the counter will not be changed
   (but an error message is printed to stderr).
*/
int counter_set(BCOUNT * counter, int new_value )
{
    errexit_on_NULL(counter);

    if( new_value > counter->max || new_value < counter->min )
    {
        fprintf(stderr, "\t\t\twarning (%s()): will not set ocunter-value out of range\n", __FUNCTION__);
    }
    else
    {
        counter->count = new_value;
    }

    return counter->count;
}


/** \brief Return boolean, indicating if the counter value is it's minimal value.
*/
bool counter_is_min(BCOUNT * counter)
{
    errexit_on_NULL(counter);

    return (counter->count == counter->min) ? true : false;
}


/** \brief Return boolean, indicating if the counter value is it's maximal value.
*/
bool counter_is_max(BCOUNT * counter)
{
    errexit_on_NULL(counter);

    return (counter->count == counter->max) ? true : false;
}



/* ================================== */
/* CREATE and DESTROY Bounded Counter */
/* ================================== */

/** \brief Create a bounded counter (dynamicylly).

    After usage, call destroy_counter() to free the
    dynamically allocated memory resources.
*/
BCOUNT * create_counter( int minval, int maxval )
{
    if( maxval < minval )
    {
        fprintf(stderr, "maxval (%d) is not allowed to be smaller than minval (%d). (%s, line %d)\n", maxval, minval, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    BCOUNT * counter = calloc(1, sizeof(BCOUNT));
    errexit_on_NULL(counter);

    counter->min = MIN(minval, maxval);
    counter->max = MAX(maxval, minval);

    counter->count = counter->min;

    counter->incr = counter_increment;
    counter->decr = counter_decrement;
    counter->get  = counter_get;
    counter->set  = counter_set;

    counter->ismin  = counter_is_min;
    counter->ismax  = counter_is_max;

    return counter;
}



/** \brief Destroy a dynamically created bounded counter.

*/
void destroy_counter( BCOUNT * counter )
{
    errexit_on_NULL(counter);

    if( counter )
    {
        free(counter);
    }

    return;
}



/** \brief Create a bounded counter (statically).

    After usage just forget about the counter - no freeing/destruction needed/allowed.
*/
BCOUNT make_counter( int minval, int maxval )
{
    BCOUNT   counter_struct;
    BCOUNT * counter = &counter_struct;

    counter->min = MIN(minval, maxval);
    counter->max = MAX(maxval, minval);

    counter->count = counter->min;

    counter->incr = counter_increment;
    counter->decr = counter_decrement;
    counter->get  = counter_get;
    counter->set  = counter_set;

    counter->ismin  = counter_is_min;
    counter->ismax  = counter_is_max;

    return counter_struct;
}


void show_counter_raw( BCOUNT * counter, char * delim, char * end )
{ // layout adapted to GRID-printer
    printf("counter.min = %3d%s"  "counter.max      = %3d%s"  "counter.count = %3d%s",
            counter->min, delim, counter->max, delim, counter->count, end);
}

void show_counter( BCOUNT * counter )
{
    show_counter_raw(counter, ", ", "\n");
    return;
}
