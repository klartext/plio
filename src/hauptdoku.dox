/** \mainpage PLIO Internals

- \subpage program_internals_toc

*/

//-----------------------------------------------------------

/** \page program_internals_toc Program Internals / Development

- \subpage change_keybindings
- \subpage add_functionality_with_new_keybindings
- \subpage comments_on_code

*/


/** \page change_keybindings Change or add Keybindings for existing Functionality

To switch already used keys for already existing functionality, do this:

- Change the mappings from keysmbol to command in the file keys2cmd.c

*/


//-----------------------------------------------------------

/** \page add_functionality_with_new_keybindings Adding Functionality and Keybindings

These things must be done to add new functionality with new keybindings for new commands:

New Functionality
- Add your function to file commands.c
- Add your function's prototype to file commands.h

New Key-Enum
- Add a new symbolic key constant (file keysenums.h)

New Command-Enum
- Add a new symbolic command constant (file commandenums.h)

New Mapping from Key-Code to Key-Enum
- Add a keymapping (file keymappings.c)

Add Mapping from Key-Enum to Commnd-Enum
- Add a mapping from key symbol to command symbol in the file keys2cmd.c (can be added to one or more mapping functions)

Activate the Command in the Statemachine
- Add an entry for the setup of the statemachine to cmdsetup.c

*/



/** \page comments_on_code Some comments on the Code / a bit of PLIO History

If you ended up here, I guess you are either extraordinarily curious,
and/or are a developer yourself.

You may already have looked into the code and wondered, why there are Mutexes used,
even though no Threads are created (but that there is a data structure `THREAD_DATA` nevertheless).

Well, this programming project started as a single-threaded program.

At the beginning, the DirsView was not existent.
It was initially planned to just display the contents of one specific dir.
Later I added the DirsView, which allowed many dirs to be represented by a thumbnail.

Reading a directory was done as one complete task, not being interrupted by the input reading loop.
When reading the images of a directory, this disabled acceptance of user commands (even quitting was not possible).
That was ok for some files, maybe 20, 50 or 200. But for directories with some thousands (or very big) images,
this just took too much time, before the user could get grasp of the program.

Later I added thrreading to allow reading the directories and reading and handling user input, to be concurrent.

It's never a good idea to add multithreading later on.
It should be designed in from the get-go.
I knew this, but hey, it was never planned to have more than just one directory displayed,
and if you want to sort all images by aspect ratio or brightness, you have to read them all anyway,
and I have heard that patience is a virtue!
And if you are really that impatient,
there is <em>killall -9</em> for the rescue! ;-)

Well, after I enjoyed the growing number of features and used plio (at that time that name was not existent)
for real world image directories with hundreds (or thousands) of images,
loading time became just annoyingly long,
so the stock of my patience was used up and I needed better interactivity.
That was, when I decided to go multithreaded.

After some painful weeks of program crashes,
and the insight that SDL2_ttf as well as SDL2 itself
are not thread safe and both have certain assumptions on how to structure the
program (only main tread is allowed to render, and only main thread is allowed to read input,
and TTF rendering should also only happen in the main thread),
I decided to go back to single threading.

This was a good decision and brought back program stability.
During the multithreaded phase after all I created the function read_next_image_into_dircontents(),
which I use also in the now-again-single-threaded program.
So, the detour did bring some results, that can be used anyway.

That there is Mutex code left over
(and some comments relate to multithreading and even the name of the above
mentioned data structure reflects the usage of multiple threads),
is a reminscence of the multithreaded episode.
Regarding the GRID code, I think, I will just let the Mutexes in the code,
as it looks not that bad and worked fine.
In other parts of the code, I may remove these leftovers,
or change the obsolete naming, when I think it's time to do so.


On libraries and thread safeness here a table to have an overview:

Library      | Thread Safeness   | Comment
------------ | ----------------- | ---------
FreeImage    | yes               | no globals
FreeType     | yes               | "local" library
MESA         | yes               | ./.
SDL2_TTF     | no                | and only to be used in main-thread
SDL2         | no                | input and rendering only to be used in main-thread

I also thought about using OpenGL, and MESA would be the matching library.
After being back in single-threaded land, and reconciling with SDL2 (and SDL2_ttf),
I have defered to switch away from SDL2. (Maybe switching to SDL3 instead?)
Also the direct use of FreeType library for TTF-rendering is now not that pressing anymore.


*/

