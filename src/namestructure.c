/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

# include <string.h>

#include "namestructure.h"


void calculate_namestructure( NAMESTRUCTURE * ns )
{
    unsigned int idx = 0;
    unsigned int maxidx = strlen(ns->name);

    int ch = 0;

    bzero(ns->name_structure, sizeof(ns->name_structure));

    for( idx = 0; idx < maxidx; idx++)
    {
        ch = ns->name[idx];

        if ( '0' <= ch && ch <= '9'  )
        {
            //ns->name_structure[idx] = 'i'; // integer - for hexnames: disorder
            ns->name_structure[idx] = 'l'; // letter - for hexnames is better
            continue;
        }

        if ( 'a' <= ch && ch <= 'z'  )
        {
            ns->name_structure[idx] = 'l'; // letter
            continue;
        }

        if ( 'A' <= ch && ch <= 'Z'  )
        {
            ns->name_structure[idx] = 'l'; // letter
            continue;
        }

        if ( ch == ' ' || ch == '\t' || ch == '\n' )
        {
            ns->name_structure[idx] = 'b'; // blank
            continue;
        }

        if ( ch == '.' )
        {
            ns->name_structure[idx] = 'd'; // dot
            continue;
        }

        if ( ch == '/' )
        {
            ns->name_structure[idx] = 's'; // slash
            continue;
        }

        ns->name_structure[idx] = 'o'; // other

    }

    return;
}


// calculate namestructure of the incoming string
// use strdup() to create the returned string
// (use free() to regain the memnory)
char * new_namestructure_from_string( char * string )
{
    NAMESTRUCTURE ns;

    bzero(&ns.name, sizeof(ns.name));
    bzero(&ns.name_structure, sizeof(ns.name));

    bcopy(string, ns.name, strlen(string));

    calculate_namestructure(&ns);

    return strdup(ns.name_structure);
}
