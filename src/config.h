/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

// BASIC FUNCTIONALITY
// ===================

// Initial Window Size
// -------------------
#define INITIAL_WINDOW_WIDTH  1024
#define INITIAL_WINDOW_HEIGHT 600

// Layout Options
// --------------
// These layout options worked fine for displays like these:
// 1366 pixel x 768 pixel and size of 344mm x 194mm
// 1920 pixel x 1080 pixel and size of 531mm x 299mm
//
#define OUTER_FRAME 30
#define THUMB_SIDELENGTH 80
#define MIN_SEP 10
#define SELECTION_FRAME_THICKNESS 4

// Layout-Prescaling
// -----------------
// Option for scaling the whole layout as well as the font.
#define PRESCALE 1.0


// Font Settings
// -------------
//#define FONT_NAME "/usr/share/fonts/TTF/DejaVuSansMono.ttf"
#define FONT_NAME "/usr/share/fonts/TTF/RobotoMono-Regular.ttf"
#define FONTSIZE    12

// EXTENDED FUNCTIONALITY
// ======================
#define ENABLE_EXTERNAL_PROGRAM true



// USER TASTE
// ==========

// Displaying Options
// ------------------
#define INDEX_START_ZERO true
