/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __MACROS__
#define __MACROS__

#include <stdio.h>
#include <stdlib.h>


#define MIN(a,b) ( ((a) < (b)) ? (a) : (b) )
#define MAX(a,b) ( ((a) > (b)) ? (a) : (b) )


#define errexit_on_NULL(arg) if( arg == NULL ) { fprintf(stderr, "NULL-pointer found in %s() (%s, line %d)\n", __FUNCTION__, __FILE__, __LINE__); exit(EXIT_FAILURE); }
#define on_NULL_errexit_with_sdlerrmsg(arg) if( arg == NULL ) { fprintf(stderr, "NULL-pointer found in %s() (%s, line %d) - SDL-Error: %s\n", __FUNCTION__, __FILE__, __LINE__, SDL_GetError()); exit(EXIT_FAILURE); }

#define on_NULL_return(arg) if( arg == NULL ) { fprintf(stderr, "NULL-pointer found for \"%s\" in %s() (%s, line %d)\n", #arg, __FUNCTION__, __FILE__, __LINE__); return; }
#define sdl_errmsg_on_error(retval) if( retval != 0 ) { fprintf(stderr, "%s() in file %s, line %d: %s\n", __FUNCTION__, __FILE__, __LINE__, SDL_GetError()); }

#define DO(command) { printf("%s(), %s, line %d: %s", __FUNCTION__, __FILE__, __LINE__, #command "\n"); command };

#endif
