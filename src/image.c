/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <string.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

#include <FreeImage.h>

#include "clock_filetime.h"
#include "colspace.h"
#include "image.h"
#include "namestructure.h"
#include "timediff.h"
#include "macros.h"
#include "sdlsurface.h"
#include "fibsurftex.h"
#include "fib.h"

typedef int (*IMAGE_COMP)( const IMAGE * i1, const IMAGE * i2);


/*
    Initally the IMAGE struct does not know the content of the image,
    as the texture is not loaded. The dimension of the image therefore
    initially is unknown (indicated by the value -1).

    The image or the thumbnail + metadata would be needed to be read,
    so that the image size etc. can be set.

    The texture initiall was loaded to the IMAGE for each image
    and never was freed before shutting down the program.
    This was allowed blazingly fast switching between images,
    but ate too much memory for large number of viewed images.

    So later the image was freed, when switching to another one.
    This is also the current (2023-04-07) way, it is done: read image and set
    the texture, when the image is viewed. If the image is switched, free the
    old texture, load the new image and set the texture for the new image.

    It is planned to throw out the texture from IMAGE completely
    and just use IMAGE as metadta structure an handle the texture separately.

    This would allow to clean up the code at some places; but is not of highest priority.
    But it should be kept in mind, when medium or large refactoring/rewriting is
    taken into account.

*/

/** \brief Allocating an IMAGE structure and setting some values - the image file will not be read.
*/
static IMAGE * new_image( char * file_abspath )
{
    assert(file_abspath);

    IMAGE * img = calloc(1, sizeof(IMAGE));
    assert(img);

    img->abspath = strndup(file_abspath, PATH_MAX);
    img->cmp.namestr = new_namestructure_from_string(img->abspath);
    modtime_of_file_hr(file_abspath, &img->modtime_hr); // NEW

    img->width  = -1; // -1 indicates: width not known
    img->height = -1; // -1 indicates: width not known

    initialize_mutex(&img->mut);
    mutex_set_name(&img->mut, file_abspath);

//fprintf(stderr, "{ \"entity\": \"IMAGE\", \"image_name\": \"%s\", \"Mutex\": \"%p\" }\n", file_abspath, &img->mut.mutex);

    return img;
}



/*
void get_imagemetadata_and_thumbnail_from_thumbtool( char * file_abspath, int thumb_size )
{
    // lies thumb
    // checke Thumbsize
    return;
}
*/


/** \brief Caclulate the centroid of the thumbnail surface - as RGB and Lab.

    This functions does access the Thumb-Surface directly,
    and it locks/unlocks the surface for that case.

    BitsPerPixel must be inequal to 8, but maybe other values also not work (maybe it must be > 8 ?)
*/
static void calculate_centroid_from_surface( SDL_Surface * surf, IMAGE * img )
{
    int res = SDL_LockSurface(surf);
    if( res == 0)
    {
        if( surf->format->BitsPerPixel != 8 ) // should not be 8 BPP here
        {
            calculate_rgb_centroid(surf, &img->cmp.centroid);
            img->cmp.centroid_lab = rgb_to_lab(&img->cmp.centroid);

            //printf("%s\t%f\t%f\t%f", img->abspath, img->cmp.centroid.r, img->cmp.centroid.g, img->cmp.centroid.b);
            //printf("\t%f\t%f\t%f\n", img->cmp.centroid_lab.l, img->cmp.centroid_lab.a, img->cmp.centroid_lab.b);
        }
        else
        {
            fprintf(stderr, "%s(): surf->format->BitsPerPixel == 8\n", __FUNCTION__);
        }
    }
    SDL_UnlockSurface(surf);
    return;
}



/** \brief Create a new IMAGE structure and read the image file with FreeImage-Lib, setting the thumbnail.
- If image file can be read, create a thumbnail from it,
    and give back a newly allocated IMAGE, with thumbnail.
- If the file could not be opened as an image file, just return NULL.
*/
IMAGE * new_image_with_thumbnail( char * file_abspath, int thumb_size )
{
    assert(file_abspath);

    IMAGE * img = new_image( file_abspath ); // create image structure
    assert(img);

    SDL_Surface * thsurf = NULL;
    IMG_META thumb;

    thumb = read_imagefile_resize(file_abspath, thumb_size);
    if( thumb.fib == NULL )
    {
        free(img);
        return NULL;
    }

    thsurf = sdl_surface_from_fib(thumb.fib);
    calculate_centroid_from_surface(thsurf, img); // Lab-centroid is needed for the color sort

    img->width  = thumb.img_w;
    img->height = thumb.img_h;
    img->thumb.surface = thsurf;
    img->thumb.fib     = thumb.fib; // must be saved, because surface reuses it (referenced, not deeply-copied)

    return img;
}



/** \brief Free all data that has been allocated inside the IMAGE structure and then the IMAGE structure also.
*/
void destroy_image( IMAGE * img )
{
    assert(img);

    if( img->abspath )
    {
        free(img->abspath);
        img->abspath = NULL;
    }

    if( img->cmp.namestr )
    {
        free(img->cmp.namestr);
        img->cmp.namestr = NULL;
    }

    if( img->name )
    {
        free(img->name);
        img->name = NULL;
    }

    if( img->texture )
    {
        SDL_DestroyTexture(img->texture);
        img->texture = NULL;
    }

    if( img->thumb.texture )
    {
        SDL_DestroyTexture(img->thumb.texture);
        img->thumb.texture = NULL;
    }

    if( img->thumb.surface )
    {
        SDL_FreeSurface(img->thumb.surface);
        img->thumb.surface = NULL;
    }

    if( img->thumb.fib )
    {
        FreeImage_Unload(img->thumb.fib);
        img->thumb.fib = NULL;
    }

    destroy_mutex(&img->mut);

    free(img);

    return;
}



// =============================================================================
// =============================================================================

/** \brief Print dimensions of the image.
*/
void show_image_dimensions( IMAGE * image )
{
    printf("width = %d, height = %d\n", image->width, image->height);

    return;
}



/** \brief Calculate the aspect ratio of the image.
*/
double aspect_ratio_of_image( const IMAGE * image )
{
    return (double) image->width / image->height;
}


/** \brief Print information about the IMAGE structure. Output channel as parameter.
*/
static void print_image_info_ch( IMAGE * img, FILE * ch )
{
    fprintf(ch, "IMAGE (%p):\n", img);
    if( img->abspath ) { fprintf(ch, "    abspath:  %s\n", img->abspath); }
    if( img->name )    { fprintf(ch, "    name:     %s\n", img->name); }
    fprintf(ch, "    width:    %4d\n", img->width);
    fprintf(ch, "    height:   %4d\n", img->height);
    fprintf(ch, "    texture:  %p\n", img->texture);

    return;
}


/** \brief Print information about the IMAGE structure. Output channel is stdout.
*/
void print_image_info( IMAGE * img )
{
    print_image_info_ch(img, stdout);
}


void orientation_reset( IMAGE * img )
{
    img->orientation = 0;
}

void orientation_set_hflip( IMAGE * img )
{
    img->orientation = img->orientation | HFLIP_MASK;
}

void orientation_unset_hflip( IMAGE * img )
{
    img->orientation = img->orientation & ~HFLIP_MASK;
}

void orientation_set_vflip( IMAGE * img )
{
    img->orientation = img->orientation | VFLIP_MASK;
}

void orientation_unset_vflip( IMAGE * img )
{
    img->orientation = img->orientation & ~VFLIP_MASK;
}

ROTATION  orientation_get_rotate( IMAGE * img )
{
    return img->orientation & ROTATION_MASK;
}

void orientation_set_rotate( IMAGE * img, ROTATION rot_enum )
{
    int rot = (rot_enum & ROTATION_MASK);

    img->orientation = img->orientation & ~ROTATION_MASK; // delete old rotation value
    img->orientation = img->orientation | rot;            // set new rotation value
}


bool orientation_is_hflip( IMAGE * img )
{
    return img->orientation & HFLIP_MASK;
}

bool orientation_is_vflip( IMAGE * img )
{
    return img->orientation & VFLIP_MASK;
}

int orientation_rot_degrees( IMAGE * img )
{
    int rot = img->orientation & ROTATION_MASK;
    int deg = rot * 90;

    return deg;
}


/** \brief Return the SDL_RendererFlip value of an image.
*/
SDL_RendererFlip flip_of_image( IMAGE * img )
{
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    assert(flip == 0); // needed for the following logic

    flip = flip | ( orientation_is_hflip(img) ? SDL_FLIP_HORIZONTAL : 0);
    flip = flip | ( orientation_is_vflip(img) ? SDL_FLIP_VERTICAL : 0);

    return flip;
}


/** \brief Print mogrify-command for in-place modification of an image, if flip-/rotation-operations have been applied to it.

    Possible Enhancement: vertical flip + horizontal flip is aequivalent to 180 degree rotation - so operations might be optimized.
*/
void print_mogrify_command( IMAGE * img )
{
    assert(img);
    char command[PATH_MAX];

    bool print = false;

    command[0] = '\0';
    strcat(command, "mogrify");

    // add flip-switch to mogrify
    // ---------------------------
    if( orientation_is_hflip(img) )
    {
        strcat(command, " -flop");
        print = true;
    }

    if( orientation_is_vflip(img) )
    {
        strcat(command, " -flip");
        print = true;
    }

    // if vertical + horizontal flip, this is aequivalent to rotation by 180 degrees
    // so here can be optimized, so that flip+flop will be removed and replaced by rotation-offset of 180 degrees


    // add rotation switch to mogrify
    // -------------------------------
    switch( orientation_get_rotate(img) )
    {
        case ROT_0: break;
        case ROT_90: strcat(command, " -rotate 270"); print = true; break;
        case ROT_180: strcat(command, " -rotate 180"); print = true; break;
        case ROT_270: strcat(command, " -rotate 90"); print = true; break;
    }

    // add filename as argument to jepgtran
    // ------------------------------------
    strcat(command, " ");
    strcat(command, img->abspath);
    strcat(command, "\n");

    // print mogrify command if any flip or rotation is needed
    // --------------------------------------------------------
    if( print == true )
    {
        fprintf(stdout, command);
    }
}


// =============================================================================
// =============================================================================



/*
    IMAGE-COMPARISON-FUNCTIONS
*/


/** \brief Calling the image comapre functions with the unpacked IMAGE pointers.

    The unpacking is needed, because the IMAGE pointers are coming from GRID's,
    which itself contain pointers to the image structures.

    The unpacking could also be done in any of the comparison functions,
    but that would add boiler plate code inany of the comparison functions.
    (But it would save another function call.)
    So this function here is to avoid boiler plate code in the raw comparison functions.

    Other effects:
        - The raw comparison functions can assume, that the image pointers are not NULL.
        - Easier code for potential extension of the comparisons later on.


*/
static int call_image_comparator( const void * vimg1, const void * vimg2, IMAGE_COMP icomp )
{
    assert(vimg1);
    assert(vimg2);
    assert(icomp);

    const IMAGE * img1 = * (IMAGE **) vimg1;
    const IMAGE * img2 = * (IMAGE **) vimg2;

    return icomp(img1, img2);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images by width.
*/
static int compare_width_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmpres = 0;

    if( img1->width > img2->width )
    {
        cmpres = 1;
    }

    if( img1->width < img2->width )
    {
        cmpres = -1;
    }

    return cmpres;
}



/** \brief Compare two images by width, with arguments compatible to grid_sort().
*/
int compare_width( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_width_raw);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images by height.
*/
static int compare_height_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmpres = 0;

    if( img1->height > img2->height )
    {
        cmpres = 1;
    }

    if( img1->height < img2->height )
    {
        cmpres = -1;
    }

    return cmpres;
}



/** \brief Compare two images by height, with arguments compatible to grid_sort().
*/
int compare_height( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_height_raw);
}
// -----------------------------------------------------------------------------


/** \brief Compare two images by size (width x height).
*/
static int compare_size_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int size_1 = img1->height * img1->width;
    int size_2 = img2->height * img2->width;

    int cmpres = 0;

    if( size_1 > size_2 )
    {
        cmpres = 1;
    }

    if( size_1 < size_2 )
    {
        cmpres = -1;
    }

    return cmpres;
}



/** \brief Compare two images by size (width x height), with arguments compatible to grid_sort().
*/
int compare_size( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_size_raw);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images by aspect ratio.
*/
static int compare_aspect_ratio_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmpres = 0;

    double ar_1 = aspect_ratio_of_image(img1);
    double ar_2 = aspect_ratio_of_image(img2);

    if( ar_1 > ar_2 )
    {
        cmpres = 1;
    }

    if( ar_1 < ar_2 )
    {
        cmpres = -1;
    }

    return cmpres;
}



/** \brief Compare two images by aspect ratio, with arguments compatible to grid_sort().
*/
int compare_aspect_ratio( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_aspect_ratio_raw);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images by name (abspath).

    For string comparison strcmp(3) is used.
*/
static int compare_name_raw( const IMAGE * img1, const IMAGE * img2 )
{
    return strcmp(img1->abspath, img2->abspath);
}



/** \brief Compare two images by name (abspath), with arguments compatible to grid_sort().
*/
int compare_name( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_name_raw);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images by file modification time (with nanosec resolution).
*/
static int compare_modtime_raw( const IMAGE * img1, const IMAGE * img2 )
{
    return cmp_timespec(&img1->modtime_hr, &img2->modtime_hr);
}



/** \brief Compare two images by file modification time (with nanosec resolution), with arguments compatible to grid_sort().
*/
int compare_modtime( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_modtime_raw);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images by name structure.

    - The namestructure is calculated as described here:
      https://www.first.in-berlin.de/software/tools/pftdbns/
    - Then on the name structure, strcmp(3) is used.
*/
static int compare_namestructure_raw( const IMAGE * img1, const IMAGE * img2 )
{
    assert(img1->cmp.namestr);
    assert(img2->cmp.namestr);

    return strcmp(img1->cmp.namestr, img2->cmp.namestr);
}



/** \brief Compare two images by name structure, with arguments compatible to grid_sort().
*/
int compare_namestructure( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_namestructure_raw);
}


// -----------------------------------------------------------------------------


/** \brief Compare two images first with compare_namestructure(),
    and when the name structures are equal, compare the filenames (abspath) with compare_name().
*/
int compare_combine_namestruct_name( const void * i1, const void * i2 )
{
    int cmpres = compare_namestructure(i1, i2);

    if( cmpres == 0 )
    {
        cmpres = compare_name(i1, i2);
    }

    return cmpres;
}


// -----------------------------------------------------------------------------


/** \brief Compare images first by size (with compare_size(),
    and if sizes are equal, then compare with compare_aspect_ratio().
*/
int compare_combine_size_aspect( const void * i1, const void * i2 )
{
    int cmpres = compare_size(i1, i2);

    if( cmpres == 0 )
    {
        cmpres = compare_aspect_ratio(i1, i2);
    }

    return cmpres;
}


// -----------------------------------------------------------------------------


/** \brief Compare images first by aspect ratio (with compare_aspect_ratio(),
    and when it is equal, then use compare_size().
*/
int compare_combine_aspect_size( const void * i1, const void * i2 )
{
    int cmpres = compare_aspect_ratio(i1, i2);

    if( cmpres == 0 )
    {
        cmpres = compare_size(i1, i2);
    }

    return cmpres;
}


// -----------------------------------------------------------------------------


/** \brief Compare images by luminance (L of Lab).
*/
static int compare_centroid_luminance_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmp = 0;

    if( img1->cmp.centroid_lab.l > img2->cmp.centroid_lab.l )
    {
        cmp = 1;
    }
    else if( img1->cmp.centroid_lab.l < img2->cmp.centroid_lab.l )
    {
        cmp = -1;
    }

    return cmp;
}



/** \brief Compare images by luminance (L of Lab), with arguments compatible to grid_sort().
*/
int compare_centroid_luminance( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_centroid_luminance_raw);
}


// -----------------------------------------------------------------------------


static int compare_centroid_lab_a_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmp = 0;

    if( img1->cmp.centroid_lab.a > img2->cmp.centroid_lab.a )
    {
        cmp = 1;
    }
    else if( img1->cmp.centroid_lab.a < img2->cmp.centroid_lab.a )
    {
        cmp = -1;
    }

    return cmp;
}



int compare_centroid_lab_a( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_centroid_lab_a_raw);
}


// -----------------------------------------------------------------------------


/* centroid.a * centroid.b - workaround as long as 2D-compare/(X,Y)-Grid is not available */
static int compare_centroid_lab_ab_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmp = 0;

    if( img1->cmp.centroid_lab.a * 256 + img1->cmp.centroid_lab.b > img2->cmp.centroid_lab.a * 256 + img2->cmp.centroid_lab.b )
    {
        cmp = 1;
    }
    else if( img1->cmp.centroid_lab.a * 256 + img1->cmp.centroid_lab.b < img2->cmp.centroid_lab.a * 256 + img2->cmp.centroid_lab.b )
    {
        cmp = -1;
    }

    return cmp;
}



int compare_centroid_lab_ab( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_centroid_lab_ab_raw);
}


// -----------------------------------------------------------------------------


/* centroid.a * centroid.b - workaround as long as 2D-compare/(X,Y)-Grid is not available */
static int compare_centroid_lab_ab_atan2_raw( const IMAGE * img1, const IMAGE * img2 )
{
    int cmp = 0;

    double phi1 = atan2(img1->cmp.centroid_lab.b, img1->cmp.centroid_lab.a);
    double phi2 = atan2(img2->cmp.centroid_lab.b, img2->cmp.centroid_lab.a);

    if( phi1 > phi2 )
    {
        cmp = 1;
    }
    else if( phi1 < phi2 )
    {
        cmp = -1;
    }

    return cmp;
}



int compare_centroid_lab_ab_atan2( const void * i1, const void * i2 )
{
    return call_image_comparator(i1, i2, compare_centroid_lab_ab_atan2_raw);
}


// -----------------------------------------------------------------------------


void rename_image( IMAGE * img, char * newname )
{
    // renaming the image (aka abspath of it)
    // --------------------------------------
    free(img->abspath);
    img->abspath = strdup(newname);

    // update namestructure
    // --------------------
    free(img->cmp.namestr);
    img->cmp.namestr = new_namestructure_from_string(img->abspath);
    return;
}
