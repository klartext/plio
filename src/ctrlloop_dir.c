/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "ctrlloop_dir.h" // only for show_dircollection_info
#include "collection.h" // only for show_dircollection_info
#include "commandenums.h"
#include "keys2cmd.h"


static void evaluate_command( STATE_ACTION * states_vector, int statesvec_len, USTATE * ustp, void * vdata)
{
    bool done = false;

    STM_IN  instate;
    STM_OUT outstate;
    bzero(&outstate, sizeof(outstate));

    instate.fsel = ustp->fsel;
    instate.cmd  = ustp->cmd;

    done = on_statematch_from_statevec_apply_func(&instate, states_vector, statesvec_len, &outstate, vdata);
    if( done ) // update state in stp to theoutput state of the statemachine
    {
        ustp->fsel = outstate.fsel;
        ustp->cmd  = CMD_NOTHING;
    }
    else
    {
        fprintf(stderr, "Warning: detected key is unbound to functionality in this view.\n");
    }

    return;
}



static int get_rsel(int fsel )
{
    int rsel = 0;

    switch( fsel )
    {
        case DIR_VIEW:   rsel = RENDER_DIRSVIEW;  break;
        case INDEX_VIEW: rsel = RENDER_INDEXVIEW; break;
        case IMAGE_VIEW: rsel = RENDER_IMAGEVIEW; break;
        default:         rsel = RENDER_NO_UPDATE; break;
    }

    return rsel;
}


// ----------- Alles neu macht der Januar -----------
int command_eval_wrapper( THREAD_DATA  *args )
{
    APP            * app    = args->app;
    DIR_COLLECTION * dcollp = args->dcollp; // kann doch in command_loop_dirview() erzeugt werden...?!

    // the followng three lines only for Event-Loop
    USTATE * stp = &args->st;
    READ_KEY * input = &args->input;

    EXTRACTED ext = prepare_extraction(dcollp); // locking done later "by hand"
    args->extracted = (void*) &ext;

    // Convert key to command, check if command is CMD_QUIT
    // or otherwise evaluate other commands.
    // ----------------------------------------------------
    dcollp->winsize_changed = input->winsize_changed;

    if( input->key != NONE )
    {
        args->needs_rendering = true;
        keycodes_to_commands(input->key, stp); // key to command

        if( stp->cmd != CMD_QUIT )
        {
            evaluate_command(app->stm.stmvec, app->stm.num_stm_entries, stp, args);

            stp->cmd_done = 0;
            dcollp->rsel = get_rsel(stp->fsel);
        }
    }
    else
    {
        dcollp->rsel = get_rsel(stp->fsel);
    }

    return 0;
}
