/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __BOUNDED_COUNTER__
#define __BOUNDED_COUNTER__

#include <stdlib.h>
#include <stdbool.h>

/** \typedef BCOUNT

    Bounded Counter

    A BCOUNT-counter can either be just cretaed as struct _bounded_counter,
    or dynamically created/destroyed with create_counter() and destroy_counter().
*/
typedef struct _bounded_counter BCOUNT;

/** \struct _bounded_counter

    Implements a bounded counter, which is a counter within a range
    [min, max].
*/
struct _bounded_counter
{
    int count; /**< the value of the counter, initially set to min value */
    int min;   /**< minimal vlaue of the counter */
    int max;   /**< maximum value of the counter */

    int (*incr)(BCOUNT *); /**< function pointer to counter_increment() */
    int (*decr)(BCOUNT *); /**< function pointer to counter_decrement() */
    int (*get)(BCOUNT *);  /**< function pointer to counter_get()       */
    int (*set)(BCOUNT *, int newval); /**< function pointer to counter_set() */

    bool (*ismin)(BCOUNT *); /**< function pointer to counter_is_max() */
    bool (*ismax)(BCOUNT *); /**< function pointer to counter_is_min() */

};


BCOUNT * create_counter( int minval, int maxval ); // dynamically cllocated counter
void destroy_counter( BCOUNT * counter ); // destroy dynamically allocated counter

BCOUNT make_counter( int minval, int maxval ); // just returns BCOUNT directly

int counter_increment(BCOUNT * counter);
int counter_decrement(BCOUNT * counter);
int counter_get(BCOUNT * counter);
int counter_set(BCOUNT * counter, int new_value );

void show_counter_raw( BCOUNT * counter, char * delim, char * end );
void show_counter( BCOUNT * counter );
#endif
