/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include "marks.h"
//#include "collection.h"



bool is_marked( MARKS * mrk )
{
    bool is_marked = mrk->general || mrk->delete;

    return is_marked;
}


void clear_all_marks( MARKS * mrk )
{
    mrk->general = false;
    mrk->delete  = false;
}


// --------------------------------------------------------------------
bool is_marked_general( MARKS * mrk )
{
    return mrk->general;
}

void set_mark_general( MARKS * mrk )
{
    mrk->general = true;
    return;
}

void clear_mark_general( MARKS * mrk )
{
    mrk->general = false;
    return;
}
// --------------------------------------------------------------------
bool is_marked_delete( MARKS * mrk )
{
    return mrk->delete;
}

void set_mark_delete( MARKS * mrk )
{
    mrk->delete = true;
    return;
}

void clear_mark_delete( MARKS * mrk )
{
    mrk->delete = false;
    return;
}
// --------------------------------------------------------------------
// --------------------------------------------------------------------
// --------------------------------------------------------------------

