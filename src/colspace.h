/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __COLSPACE__
#define __COLSPACE__

#include <SDL2/SDL.h>


/*
typedef struct _double_rgba_ DBL_RGBA;
struct _double_rgba_
{
    double r;
    double g;
    double b;
    double a;
};
*/

typedef struct _rgbf_colorspace_ RGBF;
struct _rgbf_colorspace_
{
    double r;
    double g;
    double b;
    double a;
};

typedef struct _xyz_colorspace_ XYZ;
struct _xyz_colorspace_
{
    double x;
    double y;
    double z;
};

typedef struct _labf_colorspace_ LABF;
struct _labf_colorspace_
{
    double l;
    double a;
    double b;
};

typedef struct _labi_colorspace_ LABI;
struct _labi_colorspace_
{
    int l;
    int a;
    int b;
};

LABF rgb_to_lab( const RGBF * rgbf);
LABI labi_of_labf( const LABF * labf); // this function just casts the double's to int's

void show_labf( LABF * col );
void show_labi( LABI * col );

#endif
