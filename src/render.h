/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __RENDER__
#define __RENDER__

typedef enum render_update_selection { RENDER_NO_UPDATE, RENDER_DIRSVIEW, RENDER_INDEXVIEW, RENDER_IMAGEVIEW } RENDER_UPDATE;

int render_any( void * vdata );

#endif
