/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __INDEX_LAYOUT__
#define __INDEX_LAYOUT__

#include <SDL2/SDL.h>

typedef struct _index_layout INDEX_LAYOUT;
typedef struct _index_dimensions INDEX_DIM;

struct _index_layout
{
    int outer_frame;
    int thumb_sidelength; // sidelen: side length /occupied space by thumb (quadratic)
    int minsep; // the frame around a thumb to have a distance between the thumbs
    int selection_frame_thickness; // thickness of frame around the selected image
};

struct _index_dimensions
{
    int width;  // num of x-positions
    int height; // num of y-positions

    int x_padding; // aditional horizontal space between the thumbs
    int y_padding; // aditional vertical   space between the thumbs

    // stuff, calculated from width and height
    int thumbs_per_screen;
    int thumbs_per_full_row;
    int rows_per_screen;

    int num_of_full_rows;
    int num_of_thumbs_on_partially_filled_row;

    int num_scroll_max; // number of lines (fully or partially filled) to scroll
    int num_of_thumbs_on_last_screen;
};

INDEX_LAYOUT set_index_layout(
                       int outer_frame,
                       int thumb_sidelength,
                       int minsep,
                       int selection_frame_thickness);

INDEX_LAYOUT scale_index_layout( INDEX_LAYOUT * in, double scale );

INDEX_DIM initialize_indexview_layout(int win_w, int win_h, INDEX_LAYOUT * bare_layout, int num_images );
void show_index_layout( INDEX_LAYOUT * lay );
void show_index_dimensions( INDEX_DIM * dim );

#endif
