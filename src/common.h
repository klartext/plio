/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __COMMON__
#define __COMMON__

#include <limits.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "statemachine.h"

#define COLNUM 4


struct _font_
{
    char *name;
    TTF_Font * font; // initialzed in init() with size_normal

    int size;
};


typedef struct _app APP;
struct _app
{
    SDL_Window   * window;
    SDL_Renderer * renderer;

    int initial_win_w; // initial window width
    int initial_win_h; // initial window height

    Uint32 start_ticks;

    int num_displays; // number of displays (aka monitors)
    int current_display; // display/monitor, on which the window is shown
    int thumb_load_size; // size of the thumbs (load, not display)
    double prescale;  // PRESCALE, potentially overridden by PLIO_PRESCALE
    double max_scale; // max scale overall (PRESCALE and max dpi-scale)

    Uint8 bgcol[COLNUM];   // background color setting
    Uint8 drawcol[COLNUM]; // drawing color setting
    Uint8 emphcol[COLNUM]; // drawing color *emphs* setting

    // STATE_MACHINE
    STATE_MACHINE stm;

    struct _font_ * fontvec;

    int delay;  // delay for event loops (milli seconds)
    uint32_t looptime_upto;  // time in milli seconds, that the main loop can/should need
};

#endif
