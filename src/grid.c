/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <assert.h>
#include <string.h>

#include "geometry.h" // wegen index_to_indexpair()
#include "grid.h"
#include "macros.h"




void lock_grid( GRID * grid )
{
    assert(grid);
    lock_mutex(&grid->mut);
    return;
}



void unlock_grid( GRID * grid )
{
    assert(grid);
    unlock_mutex(&grid->mut);
    return;
}



/* GET META PROPERTIES */
/* =================== */
// get current index value
// -----------------------
int grid_index_raw( GRID * grid )
{
    assert(grid);
    return grid->index;
}

int grid_index( GRID * grid )
{
    assert(grid);
    int res = 0;
    lock_grid(grid);
    res = grid_index_raw(grid);
    unlock_grid(grid);
    return res;
}

// get current row index value
// ---------------------------
int grid_row_raw( GRID * grid )
{
    assert(grid);
    return grid->index / grid->xlen;
}

int grid_row( GRID * grid )
{
    assert(grid);
    int res = 0;
    lock_grid(grid);
    res = grid_row_raw(grid);
    unlock_grid(grid);
    return res;
}

// get current col index value
// ---------------------------
int grid_col_raw( GRID * grid )
{
    assert(grid);
    return grid->index % grid->xlen;
}

int grid_col( GRID * grid )
{
    assert(grid);
    int res = 0;
    lock_grid(grid);
    res = grid_col_raw(grid);
    unlock_grid(grid);
    return res;
}


// get the xlen-value
// ------------------
int grid_get_xlen_raw( GRID * grid )
{
    assert(grid);
    return grid->xlen;
}

int grid_get_xlen( GRID * grid )
{
    assert(grid);
    int res = 0;
    lock_grid(grid);
    res = grid_get_xlen_raw(grid);
    unlock_grid(grid);
    return res;
}


// set xlen
// - it's allowed that xlen > veclen, even though this may seems to be strange
// ------------------------------------------------------------------------------------
void grid_set_xlen_raw( GRID * grid, int new_xlen )
{
    assert(grid);
    grid->xlen = new_xlen;
    return;
}

void grid_set_xlen( GRID * grid, int new_xlen )
{
    assert(grid);

    lock_grid(grid);
    grid_set_xlen_raw(grid, new_xlen);
    unlock_grid(grid);
    return;
}

// get the length of the grid (maxidx + 1)
// ---------------------------------------
int grid_length_raw( GRID * grid )
{
    assert(grid);
    return grid->used_length;
}

int grid_length( GRID * grid )
{
    int res = 0;
    lock_grid(grid);
    res = grid_length_raw(grid);
    unlock_grid(grid);
    return res;
}

// get the value of the index
// --------------------------
int grid_get_index_raw( GRID * grid )
{
    assert(grid);
    return grid_index_raw(grid);
}

int grid_get_index( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_get_index_raw(grid);
    unlock_grid(grid);
    return res;
}


/** \brief Get the index of the last available element.
*/
int grid_get_last_index_raw( GRID * grid )
{
    assert(grid);
    return grid->used_length - 1;
}

int grid_get_last_index( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_get_last_index_raw(grid);
    unlock_grid(grid);
    return res;
}


// get the value of the cursor
// -------------------------------
int grid_get_cursor_raw( GRID * grid )
{
    assert(grid);
    return grid->cursor;
}

int grid_get_cursor( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_get_cursor_raw(grid);
    unlock_grid(grid);
    return res;
}

// Check, if index is in first row
// -------------------------------
bool in_first_row_raw( GRID * grid )
{
    assert(grid);

    int xidx = 0;
    int yidx = 0;

    index_to_indexpair(grid->index, grid->xlen, &xidx, &yidx);

    return (yidx == 0) ? true : false;
}

bool in_first_row( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = in_first_row_raw(grid);
    unlock_grid(grid);
    return res;
}


// Check, if index is in first column
// ---------------------------------
bool in_first_col_raw( GRID * grid )
{
    assert(grid);

    int xidx = 0;
    int yidx = 0;

    index_to_indexpair(grid->index, grid->xlen, &xidx, &yidx);

    return (xidx == 0) ? true : false;
}

bool in_first_col( GRID * grid )
{
    assert(grid);

    bool res = 0;
    lock_grid(grid);
    res = in_first_col_raw(grid);
    unlock_grid(grid);
    return res;
}

// Check, if index is in last row
// -------------------------------
bool in_last_row_raw( GRID * grid )
{
    assert(grid);
    return ( grid->index / grid->xlen == ((grid->used_length - 1) / grid->xlen) ) ? true : false;
}

bool in_last_row( GRID * grid )
{
    bool res = 0;
    lock_grid(grid);
    res = in_last_row_raw(grid);
    unlock_grid(grid);
    return res;
}

// Check, if index is in last row
// -------------------------------
bool in_last_col_raw( GRID * grid )
{
    assert(grid);

    int xidx = 0;
    int yidx = 0;

    index_to_indexpair(grid->index, grid->xlen, &xidx, &yidx);

    return (xidx == grid->xlen - 1) ? true : false;
}

bool in_last_col( GRID * grid )
{
    assert(grid);

    bool res = 0;
    lock_grid(grid);
    res = in_last_col_raw(grid);
    unlock_grid(grid);
    return res;
}


// Check, if index is in last row
// -------------------------------
bool is_last_element_raw( GRID * grid )
{
    assert(grid);
    return (grid->index == grid->used_length - 1) ? 1 : 0;
}

bool is_last_element( GRID * grid )
{
    assert(grid);

    bool res = 0;
    lock_grid(grid);
    res = is_last_element_raw(grid);
    unlock_grid(grid);
    return res;
}



/* SET META PROPERTIES */
/* =================== */
int grid_set_index_raw( GRID * grid, int index )
{
    assert(grid);
    assert(index >= 0);
    assert(index < grid->veclen);
    assert(index < grid->used_length); // new criterium

    grid->index = index;

    return grid->index;
}

int grid_set_index( GRID * grid, int index )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_set_index_raw(grid, index);
    unlock_grid(grid);
    return res;
}

// store current index as  cursor
// ------------------------------
int grid_store_cursor_raw( GRID * grid )
{
    assert(grid);
    grid->cursor = grid->index;
    return grid->cursor;
}

int grid_store_cursor( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_store_cursor_raw(grid);
    unlock_grid(grid);
    return res;
}






/* INCREMENT /DECREMENT Index */
/* ========================== */
int grid_increment_raw( GRID * grid )  // increment: increment_x
{
    assert(grid);

    if( grid->index + 1 < grid->used_length )
    {
        ++grid->index;
    }

    return grid->index;
}

int grid_increment( GRID * grid )  // increment: increment_x
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_increment_raw(grid);
    unlock_grid(grid);
    return res;
}


int grid_decrement_raw( GRID * grid )
{
    assert(grid);

    if( grid->index > 0 )
    {
        --grid->index;
    }

    return grid->index;
}

int grid_decrement( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_decrement_raw(grid);
    unlock_grid(grid);
    return res;
}



int grid_increment_y_raw( GRID * grid )
{
    assert(grid);

    if( grid->index + grid->xlen < grid->used_length )
    {
        grid->index += grid->xlen;
    }

    return grid->index;
}

int grid_increment_y( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_increment_y_raw(grid);
    unlock_grid(grid);
    return res;
}



int grid_decrement_y_raw( GRID * grid )
{
    assert(grid);

    if( grid->index - grid->xlen >= 0 )
    {
        grid->index -= grid->xlen;
    }

    return grid->index;
}

int grid_decrement_y( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_decrement_y_raw(grid);
    unlock_grid(grid);
    return res;
}

/* REGISTER / UNREGISTER PAYLOAD */
/* ============================= */

// register with overwrite
void grid_register_single_payload_xy_raw( GRID * grid, int xidx, int yidx, void * payload )
{
    assert(grid);
    assert(payload);

    int idx = yidx * grid->xlen + xidx;

    assert(idx < grid->veclen);
    assert(grid->index < grid->used_length); // new criterium

    grid->payload[idx] = payload;

    return;
}

void grid_register_single_payload_xy( GRID * grid, int xidx, int yidx, void * payload )
{
    assert(grid);

    lock_grid(grid);
    grid_register_single_payload_xy_raw(grid, xidx, yidx, payload);
    unlock_grid(grid);
    return;
}



void grid_unregister_single_payload_raw( GRID * grid, int xidx, int yidx )
{
    assert(grid);

    int idx = yidx * grid->xlen + xidx;
    assert(idx < grid->veclen);
    assert(grid->index < grid->used_length); // new criterium

    grid->payload[idx] = NULL;

    return;
}

void grid_unregister_single_payload( GRID * grid, int xidx, int yidx )
{
    assert(grid);

    lock_grid(grid);
    grid_unregister_single_payload_raw(grid, xidx, yidx);
    unlock_grid(grid);
    return;
}



// add payload, sert used_length to index + 1
void grid_set_payload_raw( GRID * grid, void * new_payload )
{
    assert(grid);
    assert(grid->used_length <= grid->veclen);
    assert(grid->index < grid->veclen);

    grid->payload[grid->index] = new_payload;

    return;
}

void grid_set_payload( GRID * grid, void * new_payload )
{
    assert(grid);

    lock_grid(grid);
    grid_set_payload_raw(grid, new_payload);
    unlock_grid(grid);
    return;
}

// Extra: increment used_length
int grid_increment_used_length_raw( GRID * grid )
{
    assert(grid);

    // both are length allowed to be equal, and after increment used_length is at most equal
    if( grid->used_length < grid->veclen )
    {
        ++grid->used_length;
    }

    return grid->index;
}

int grid_increment_used_length( GRID * grid )
{
    assert(grid);

    int res = 0;
    lock_grid(grid);
    res = grid_increment_used_length_raw(grid);
    unlock_grid(grid);
    return res;
}


void grid_set_used_length_raw( GRID * grid, int new_used_length )
{
    assert(grid != NULL);
    assert(new_used_length <= grid->veclen);

    grid->used_length = new_used_length;
    return;
}

void grid_set_used_length( GRID * grid, int new_used_length )
{
    assert(grid);

    lock_grid(grid);
    assert(new_used_length <= grid->veclen);
    unlock_grid(grid);
    return;
}


// Copy the pointers from the source payload to the grid.
// Starting to write the target (grid) at index 0,
void grid_register_range_payload_raw( GRID * grid, int src_start_offset, int copy_num, void ** payload_source )
{
    //printf("====> %s(): src_start_offset = %d,  copy_num = %d \n", __FUNCTION__, src_start_offset, copy_num);
    assert(src_start_offset >= 0);
    assert(copy_num > 0);
    assert(copy_num <= grid->veclen);

    int itemsleft = MIN(grid->veclen, copy_num); // ist in den asserts schion abgedecklt, aber nunja

    grid->used_length = itemsleft;

    if(grid->index > grid->used_length - 1 )
    {
        grid->index = grid->used_length - 1;
    }


    int idx    = 0;
    while(itemsleft--)
    {
        grid->payload[idx] = payload_source[idx + src_start_offset];
        idx++;
    }

    return;
}

void grid_register_range_payload( GRID * grid, int src_start_offset, int copy_num, void ** payload_source )
{
    assert(grid);

    lock_grid(grid);
    grid_register_range_payload_raw(grid, src_start_offset, copy_num, payload_source);
    unlock_grid(grid);
    return;
}


/* ACCESS Payload */
/* ============== */

/** \brief Get the active payload (the payload indexed by the current index value).
*/
void * grid_get_payload_raw( GRID * grid )
{
    assert(grid);
    return grid->payload[grid->index];
}

void * grid_get_payload( GRID * grid )
{
    assert(grid);

    void * res = NULL;
    lock_grid(grid);
    res = grid_get_payload_raw(grid);
    unlock_grid(grid);
    return res;
}


/** \brief Get the payload, indexed by idx.
*/
void * grid_get_payload_by_index_raw( GRID * grid, int idx )
{
    assert(grid);

    assert(idx >= 0 && idx < grid->veclen);
    assert(idx >= 0 && idx < grid->used_length); // new criterium

    return grid->payload[idx];
}

void * grid_get_payload_by_index( GRID * grid, int idx )
{
    assert(grid);

    void * res = NULL;
    lock_grid(grid);
    res = grid_get_payload_by_index_raw(grid, idx);
    unlock_grid(grid);
    return res;
}


/** \brief Get the payload from the grid, indexed aith x- and y-index values.
*/
void * grid_get_payload_xy_raw( GRID * grid, int xidx, int yidx )
{
    assert(grid);

    int idx = yidx * grid->xlen + xidx;
    assert(idx < grid->veclen);
    assert(idx >= 0 && idx < grid->used_length); // new criterium

    return grid->payload[idx];
}

void * grid_get_payload_xy( GRID * grid, int xidx, int yidx )
{
    assert(grid);

    void * res = NULL;
    lock_grid(grid);
    res = grid_get_payload_xy_raw(grid, xidx, yidx);
    unlock_grid(grid);
    return res;
}

/** \brief revrse the order of the payload elements of the grid.
*/
void grid_reverse_payload_order_raw( GRID * grid )
{
    assert(grid);

    int maxidx = grid->used_length / 2;
    int idx = 0;
    void * tmp = NULL;

    for( idx = 0; idx < maxidx; idx++ )
    {
        tmp = grid->payload[idx]; // grid_get_payload_by_index()
        grid->payload[idx]= grid->payload[grid->used_length - 1 - idx];
        grid->payload[grid->used_length - 1 - idx] = tmp;
    }


    return;
}

void grid_reverse_payload_order( GRID * grid )
{
    assert(grid);

    lock_grid(grid);
    grid_reverse_payload_order_raw(grid);
    unlock_grid(grid);
    return;
}


/** \brief Exhcange two payloads, selected bythe index arguments.
*/
void grid_exchange_payloads_raw( GRID * grid, int idx_1, int idx_2 )
{
    assert(grid);
    assert(idx_1 < grid->veclen); // should I check on used_length?
    assert(idx_2 < grid->veclen); // should I check on used_length?

    void * pay1 = grid->payload[idx_1];
    void * pay2 = grid->payload[idx_2];

    grid->payload[idx_1] = pay2;
    grid->payload[idx_2] = pay1;

    return;
}

void grid_exchange_payloads( GRID * grid, int idx_1, int idx_2 )
{
    assert(grid);

    lock_grid(grid);
    grid_exchange_payloads_raw(grid, idx_1, idx_2);
    unlock_grid(grid);
    return;
}


/** \brief Sort the grid with elements of size 'payload_size' with Compare function.
*/
void grid_sort_raw( GRID * grid, int payload_size, int (*Compare)(const void * pl1, const void * pl2) )
{
    assert(grid);
    assert(Compare); // allowing Comapre to be NULL might make sense for some dummy code

    qsort(&grid->payload[0], (size_t) grid->used_length, payload_size, Compare);
}

void grid_sort( GRID * grid, int payload_size, int (*Compare)(const void * pl1, const void * pl2) )
{
    assert(grid);

    lock_grid(grid);
    grid_sort_raw(grid, payload_size, Compare);
    unlock_grid(grid);
    return;
}


/** \brief Apply Func to the payload of the current index.
*/
void grid_apply_raw( GRID * grid, void (*Func)(void*) )
{
    assert(grid);
    assert(Func);

    (*Func)(grid->payload[grid->index]);

    return;
}

void grid_apply( GRID * grid, void (*Func)(void*) )
{
    assert(grid);

    lock_grid(grid);
    grid_apply_raw(grid, Func);
    unlock_grid(grid);
    return;
}


/** \brief Apply Func to all used payloads.
*/
void grid_iter_raw( GRID * grid, void (*Func)(void*) )
{
    assert(grid);
    assert(Func);

    for( int idx = 0; idx < grid->used_length; idx++ )
    {
        (*Func)(grid->payload[idx]);
    }

    return;
}

void grid_iter( GRID * grid, void (*Func)(void*) )
{
    assert(grid);

    lock_grid(grid);
    grid_iter_raw(grid, Func);
    unlock_grid(grid);
    return;
}


/** \brief Rotate the grid in the subset between start_idx and end_idx (rotate right).
*/
void grid_rotate_right_raw( GRID * grid, int start_idx, int end_idx )
{
    assert(grid);
    assert(0 <= start_idx);
    assert(start_idx <= end_idx);
    assert(end_idx < grid->used_length);

    void * last = grid->payload[end_idx];
    for( int idx = end_idx; idx > start_idx; --idx )
    {
        grid->payload[idx] = grid->payload[idx-1];
    }
    grid->payload[start_idx] = last;

    return;
}


/** \brief Rotate the grid in the subset between start_idx and end_idx (rotate left).
*/
void grid_rotate_left_raw( GRID * grid, int start_idx, int end_idx )
{
    assert(grid);
    assert(0 <= start_idx);
    assert(start_idx <= end_idx);
    assert(end_idx < grid->used_length);

    void * first = grid->payload[start_idx];
    for( int idx = start_idx; idx < end_idx; ++idx )
    {
        grid->payload[idx] = grid->payload[idx+1];
    }
    grid->payload[end_idx] = first;

    return;
}


/* CREATIE / DESTROY GRID */
/* ====================== */

GRID * create_grid( int initial_length, int x_len )
{
    // create GRID
    GRID * grid = calloc(1, sizeof(GRID));
    assert(grid);

    assert(initial_length >= 0);
    assert(x_len > 0);

    if( x_len > initial_length )
    {
        x_len = initial_length;
        fprintf(stderr, "%s(): clamping x_len to initial_length (=%d)\n", __FUNCTION__, initial_length);
    }


    // create Payload array
    grid->payload = calloc(initial_length, sizeof(void *));
    if( grid->payload == NULL )
    {
        destroy_grid(grid);
        exit(EXIT_FAILURE);
    }


    grid->veclen = initial_length;
    grid->xlen   = x_len;

    grid->curidx = grid_index;
    grid->len    = grid_length;

    grid->xincr = grid_increment;
    grid->xdecr = grid_decrement;

    grid->yincr = grid_increment_y;
    grid->ydecr = grid_decrement_y;

    grid->get   = grid_get_payload;
    grid->get_by_xy = grid_get_payload_xy;

    grid->row   = grid_row;
    grid->store = grid_store_cursor;

    initialize_mutex(&grid->mut);
char name[100];
name[0] = '\0';
sprintf(name, "GRID %p", grid);
    mutex_set_name(&grid->mut, name);

    return grid;
}


void destroy_grid( GRID * grid )
{
    assert(grid);

    if( grid->payload )
    {
        free(grid->payload);
    }

    destroy_mutex(&grid->mut);

    free(grid);

    return;
}



void grid_payload_copy_raw( GRID * src, GRID * dst, int copy_offset )
{
    assert(src);
    assert(dst);
    assert(copy_offset >= 0);
    assert(copy_offset < src->veclen);

    int loopmax = MIN(src->veclen - copy_offset, dst->veclen); // TODO: check again

    memcpy(&dst->payload[0], &src->payload[copy_offset], loopmax * sizeof(void*));

    dst->used_length = loopmax;

    return;
}

void grid_payload_copy( GRID * src, GRID * dst, int copy_offset )
{
    lock_grid(src);
    lock_grid(dst);

    grid_payload_copy_raw(src, dst, copy_offset);

    unlock_grid(dst);
    unlock_grid(src);
    return;
}



GRID * grid_copy_create_raw( GRID * grid, int ini_len, int x_len, int copy_offset )
{
    assert(grid);
    assert(ini_len > 0);
    assert(x_len > 0);
    assert(copy_offset >= 0 && copy_offset < (*grid->len)(grid));

    GRID * new  = create_grid(ini_len, x_len);

    grid_payload_copy(grid, new, copy_offset);

    return new;
}

GRID * grid_copy_create( GRID * grid, int ini_len, int x_len, int copy_offset )
{
    assert(grid);

    GRID * res = NULL;
    lock_grid(grid);
    res = grid_copy_create_raw(grid, ini_len, x_len, copy_offset);
    unlock_grid(grid);
    return res;
}


void grid_payload_ncopy_raw( GRID * src, GRID * dst, int copy_num, int copy_offset )
{
    assert(src);
    assert(dst);
    assert(copy_offset >= 0);
    assert(copy_offset < src->veclen);
    assert(copy_num >= 0);

    if( copy_num == 0 )
    {
        return;
    }

    int loopmax = MIN(src->veclen - copy_offset, dst->veclen); // TODO: check again
    loopmax = MIN(loopmax, copy_num);

    memcpy(&dst->payload[0], &src->payload[copy_offset], loopmax * sizeof(void*));

    dst->used_length = loopmax;

    return;
}

void grid_payload_ncopy( GRID * src, GRID * dst, int copy_num, int copy_offset )
{
    lock_grid(src);
    lock_grid(dst);

    grid_payload_ncopy_raw(src, dst, copy_num, copy_offset);

    unlock_grid(dst);
    unlock_grid(src);
    return;
}

void show_grid_delim_raw( GRID * grid, char * delim, char * end )
{ // layout adapted to BCOUNT-printer
    printf("grid.veclen = %3d%s"  "grid.used_length = %3d%s"  "grid.xlen     = %3d%s"  "grid.indxex = %3d%s",
            grid->veclen, delim, grid->used_length, delim, grid->xlen, delim, grid->index, end);
}

void show_grid_delim( GRID * grid, char * delim, char * end )
{
    assert(grid);

    lock_grid(grid);
    show_grid_delim_raw(grid, delim, end);
    unlock_grid(grid);
    return;
}

void show_grid( GRID * grid )
{
    assert(grid);

    show_grid_delim(grid, ", ", "\n");
    return;
}
