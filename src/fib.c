/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

//#include "tools.h"
#include "fib.h"

// SDL2 limits textures to a size of 8192x8192.
// To stay inside this limit, Surfaces should be limited to this size.
// To have that value available as constant, it's defined here.
// For reference, see here: https://forums.libsdl.org/viewtopic.php?p=49643
#define MAX_SDL_TEXTURE_SIDE_LENGTH 8192


/** \brief Check, if the filetype is known.

    This function is provided for special needs.
    The functions get_freeimage_bitmap_unaltered() and new_image_with_thumbnail() check
    the readability by themselves, so normally it is not necessary to
    call filetype_is_known() before.
*/
bool filetype_is_valid_fif( char * filename )
{
    FREE_IMAGE_FORMAT filetype = FreeImage_GetFileType(filename, 0);

    if( filetype == FIF_UNKNOWN )
    {
        fprintf(stderr, "Filetype: FIF_UNKNOWN (file: %s)\n", filename);
        return false;
    }

    return true;
}



/** \brief Open the file with FreeImage Library, give back a bitmap.
*/
static FIBITMAP * get_freeimage_bitmap_unaltered(char *filename)
{
    FREE_IMAGE_FORMAT filetype = FreeImage_GetFileType(filename, 0);

    if( filetype == FIF_UNKNOWN )
    {
        fprintf(stderr, "Filetype: FIF_UNKNOWN (file: %s)\n", filename);
        return NULL;
    }

    // might additionally be used: FreeImage_Validate(filetype, filename)

    FIBITMAP *freeimage_bitmap = FreeImage_Load(filetype, filename, 0);

    return freeimage_bitmap;
}



/** \brief If the FreeImage bitmap has less than BPP of 32 (or higher than 32 Bits), then create a new one as the old one converted to 32 bit.

    If fibm has BPP of less (or even more) than 32 Bits, then (try to) convert it to 32 bit.
    On success, the old bitmap then is unloaded/freed and a new one created.
    The pointer to the new FI-Bitmap is returned.

    If fibm has not less (or more) than 32 Bit, or conversion failed,
    then the original bitmap is returned,
    and a message to stderr is created.
*/
static FIBITMAP * convert_fib_bitmap_to_32bit( FIBITMAP * fibm )  // TODO: better name, to make clear, that replaced FIB will be freed
{
    if( fibm == NULL )
    {
        fprintf(stderr, "%s(): fibm is NULL\n", __FUNCTION__);
        return NULL;
    }

    if( FreeImage_GetBPP(fibm) != 32 )
    {
        FIBITMAP * tmp = FreeImage_ConvertTo32Bits(fibm);
        if( tmp != NULL )
        {
            FreeImage_Unload(fibm);
            fibm = tmp;
        }
        else
        {
            fprintf(stderr, "Conversion of FI-Bitmap to 32 Bit failed.\n");
        }
    }

    return fibm;
}



/** \brief Scale down FIB to MAX_SDL_TEXTURE_SIDE_LENGTH x MAX_SDL_TEXTURE_SIDE_LENGTH if the image is larger.

    Creates a new FIB if scaling is necessary.
    Otherwise the original FIBITMAP is returned.

*/
static FIBITMAP * ensure_surface_fits_maximal_sdl_texture_size( FIBITMAP * fib )
{
    assert(fib);

    FIBITMAP * fitting = NULL;

    if( FreeImage_GetWidth(fib) > MAX_SDL_TEXTURE_SIDE_LENGTH || FreeImage_GetHeight(fib) > MAX_SDL_TEXTURE_SIDE_LENGTH )
    {
        fitting = FreeImage_MakeThumbnail(fib, MAX_SDL_TEXTURE_SIDE_LENGTH, TRUE);
    }
    else
    {
        fitting = fib;
    }

    return fitting;
}



/** \brief Open the file with FreeImage Library, convert to 32 bit, Flip vertical, then give back the resulting bitmap.

    If the image is larger than 8192 x 8192 pixles, it will be scaled down to matching this size.
    The reason is, that SDL2 does not allow textures larger than this size,
    see https://forums.libsdl.org/viewtopic.php?p=49643 for example.

    To prevent the texture being NULL later on,
    the image is resized here.
    Of course that degrades the resolution of the image,
    ifit is larger than that limit.
    But at least large images can be seen now also.

    If (when?) the image vieweing is enhanced/overhauled,
    and navigation inside an image is added, maybe this issue could also be revolved then.

    It's not clear so far, if this is a problem that would affect too many users, or if this is just
    a too rare case to be solved differently. So for now (2023-04-22 - before the first release),
    this big images issue will just be resolved with downscaling.

*/
FIBITMAP * get_freeimage_bitmap(char *filename)
{
    FIBITMAP    * fib     = NULL;
    FIBITMAP    * fib32   = NULL;
    FIBITMAP    * result  = NULL;

    fib = get_freeimage_bitmap_unaltered(filename);
    if( fib != NULL )
    {
        FIBITMAP * scaled = ensure_surface_fits_maximal_sdl_texture_size(fib);
        if( scaled != fib )
        {
            FreeImage_Unload(fib);
            fib = scaled;
            fprintf(stderr, "Image %s has been scaled down\n", filename);
        }

        fib32 = convert_fib_bitmap_to_32bit(fib); // good to know: if conversion is done, old fib will be freed by this function
        FreeImage_FlipVertical(fib32); // the image is upside-down, this must be corrected here
    }

    result = (fib32 != NULL) ? fib32 : fib;

    return result;
}



/** \brief Read image file, create thumbnail, return the thumbnail and the width and height of the original image.
*/
IMG_META read_imagefile_resize( char * file_abspath, int thumb_size )
{
    FIBITMAP * fib_image = NULL;
    IMG_META meta;
    bzero(&meta, sizeof(meta));

    fib_image = get_freeimage_bitmap(file_abspath);
    if(fib_image != NULL)
    {

        meta.img_w = FreeImage_GetWidth(fib_image);
        meta.img_h = FreeImage_GetHeight(fib_image);

        if( thumb_size > 0 )
        {
            meta.fib = FreeImage_MakeThumbnail(fib_image, thumb_size, TRUE);
            FreeImage_Unload(fib_image);
        }
        else
        {
            meta.fib = fib_image;
        }

    }

    return meta;
}



void show_freeimage_bitmap_properties( FIBITMAP * fib_bitmap, const char * prepend )
{
    printf("FIBITMAP * %p\n", fib_bitmap);

    if( fib_bitmap == NULL )
    {
        return;
    }

    printf(" %s -> FreeImage_GetBits():      %p\n", prepend, FreeImage_GetBits(fib_bitmap));
    printf(" %s -> FreeImage_GetWidth():     %14d\n", prepend, FreeImage_GetWidth(fib_bitmap));
    printf(" %s -> FreeImage_GetHeight():    %14d\n", prepend, FreeImage_GetHeight(fib_bitmap));
    printf(" %s -> FreeImage_GetBPP():       %14d\n", prepend, FreeImage_GetBPP(fib_bitmap));
    printf(" %s -> FreeImage_GetPitch():     %14d\n", prepend, FreeImage_GetPitch(fib_bitmap));
    printf(" %s -> FreeImage_GetRedMask():   %14X\n", prepend, FreeImage_GetRedMask(fib_bitmap));
    printf(" %s -> FreeImage_GetGreenMask(): %14X\n", prepend, FreeImage_GetGreenMask(fib_bitmap));
    printf(" %s -> FreeImage_GetBlueMask():  %14X\n", prepend, FreeImage_GetBlueMask(fib_bitmap));
    printf(" %s -> Is there no GetAlphamask?():\n", prepend);

    return;
}
