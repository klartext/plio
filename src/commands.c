/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "clock_filetime.h"
#include "collection.h"
#include "commandenums.h"
#include "commands.h"
#include "grid.h"
#include "image.h"
#include "stringhelpers.h"
#include "threadsupport.h"
#include "selection.h"
#include "ustate.h"
#include "display.h" // only for toggle_fullscreen() (maybe to move elsewhere?)
#include "renamefiles.h"
#include "config.h"


// DIR_COLLECTION-Mutex and USTATE-Mutex are already locked by command_loop_dirview()
// The other mutexs must be locked here.

// ============================================================================

/** \brief Set the 'dump_screen' flag, used by the renderer.
*/
void dump_screen( void * vdata )
{
    THREAD_DATA * data = vdata;
    DIR_COLLECTION * dcollp   = data->dcollp;
    dcollp->dump_screen = true;

    return;
}
// -----------------------------------------------------------------------------

/** \brief Toggle the general mark of the active image.

    This function is more general than for image marks, but only called from within the INDEX_VIEW.
*/
void toggle_general_image_mark( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;

    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID  * image_grid = navp->grid;

    IMAGE * img = NULL;

    bool initially_marked = false;
    bool initially_marked_general = false;
    bool now_marked = false;


    img = grid_get_payload(image_grid);

    initially_marked         = is_marked(&img->mark);
    initially_marked_general = is_marked_general(&img->mark);

    if( initially_marked_general )
    {
        clear_mark_general(&img->mark);
    }
    else
    {
        set_mark_general(&img->mark);
    }

    // after setting/resetting, update the DirContents mark-counter
    // ------------------------------------------------------------
    now_marked = is_marked(&img->mark);

    if( switched_off(initially_marked, now_marked) )
    {
        extp->currdir->num_of_marked -= 1;
    }

    if( switched_on(initially_marked, now_marked) )
    {
        extp->currdir->num_of_marked += 1;
    }

    //printf("Number of marked images: %d (dir: %s)\n", extp->currdir->num_of_marked, extp->currdir->dirname);

    stp->cmd_done = true;

    unlock_currdir(extp);

    return;
}


/** \brief Clear the general marks of all images of the current directory/index.

    This function is more general than for image marks, but only called from within the INDEX_VIEW.
*/
void clear_general_image_marks( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;

    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID  * image_grid = navp->grid;
    IMAGE * img = NULL;

    for( int idx = 0; idx < image_grid->used_length; idx++ )
    {
        img = grid_get_payload_by_index(image_grid, idx);
        clear_mark_general(&img->mark);
    }

    extp->currdir->num_of_marked = 0;

    stp->cmd_done = true;
    unlock_currdir(extp);

    return;
}


/** \brief Calculate the string that is to be prepended to the basename in the renaming operation.
*/
static void calculate_prepend_string_for_image_renaming( IMAGE * img, bool is_sorted, int pos_idx, int used_sort_command, char * prepend_buf )
{
    prepend_buf[0] = '\0';

    if( is_sorted )
    {
        switch( used_sort_command )
        {
            case CMD_SORT_MTIME: get_timestring_from_timespec(prepend_buf, &img->modtime_hr);
                                 strcat(prepend_buf, "_");
            break;

            case CMD_SORT_WIDTH: sprintf(prepend_buf, "%05d_", img->width);
            break;

            case CMD_SORT_HEIGHT: sprintf(prepend_buf, "%05d_", img->height);
            break;

            case CMD_SORT_SIZE: sprintf(prepend_buf, "%09d_", img->width * img->height);
            break;

            default: sprintf(prepend_buf, "%05d_", pos_idx);

        }
    }
    else
    {
        sprintf(prepend_buf, "%05d_", pos_idx); // prepend index if not sorted
    }

    return;
}


/** \brief Raw function to rename all files of the current index-view.

    This function is intended to be called by rename_files_of_index() as well as
    rename_files_of_index_noprepend().
*/
static void rename_files_of_index_raw( void * vdata, bool prepend )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;

    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID  * image_grid = navp->grid;

    IMAGE * img = NULL;
    NAME_MAP mapping = make_name_map(grid_length(image_grid));

    char newname[PATH_MAX];
    char prepend_string[PATH_MAX];

    // maybe add: assert(stp->fsel == INDEX_VIEW); // should be true by function selection of statemachine anyway

    // if directory has been changed by someone else... (not plio)
    if( dircontents_did_change(extp->currdir) )
    {
        fprintf(stderr, "contents of dir has been externally changed: %s\n", extp->currdir->dirname);
        // Maybe do something here, like re-reading the directory contents (updating DIRCONTENTS in place)?
    }


    // setting up the renaming map
    // ---------------------------
    for( int idx = 0; idx < grid_length(image_grid); idx++ )
    {
        img = grid_get_payload_by_index(image_grid, idx);
        bzero(&newname, sizeof(newname));
        bzero(&prepend_string, sizeof(prepend_string));

        if( prepend )
        {
            calculate_prepend_string_for_image_renaming(img, navp->sort_status.is_sorted, idx,  navp->sort_status.used_sort_command, prepend_string);
            prepend_string_to_basename(img->abspath, prepend_string, newname);
        }
        else
        {
            calculate_prepend_string_for_image_renaming(img, false, idx,  CMD_NOTHING, prepend_string); // creates Index-String
            prepend_string_to_extension_of_basename(img->abspath, prepend_string, newname); // prepend_string + (only extension of basename)
        }

        insert_name_mapping_at_index(&mapping, img->abspath, newname, idx);
    }

    // If new names are not too long, then rename files/images
    // -------------------------------------------------------
    if( new_names_fit_limits(&mapping) )
    {
        // rename the image-abspaths
        // -------------------------
        for( int idx = 0; idx < grid_length(image_grid); idx++ )
        {
            char * newname;
            img = grid_get_payload_by_index(image_grid, idx);

            read_name_mapping_at_index(&mapping, NULL, &newname, idx );
            rename_image(img, newname);
        }

        // rename the files on the filesystem
        // ----------------------------------
        apply_renaming(&mapping); // should also do the image renaming... or should be renamed to apply_renaming_on_filesystem()

        // updating the modtime of DIRCONTENTS
        // -----------------------------------
        dircontents_update_modtime(extp->currdir); // external changes so far will now be undetectable
    }
    else
    {
        fprintf(stderr, "Renaming of the files not possible, because names would become too long!\n");
        // Maybe also write message to GUI, so that the problem is obvious also without looking at the terminal.
        // Workaround: use 'R' renaming to shorten the names.
    }


    // clean up resources
    // ------------------
    cleanup_name_map(&mapping);
    unlock_currdir(extp);

    stp->cmd_done = true;

    return;
}


/** \brief Rename all files of the current index-view.

    This function is intended to be called from within INDEX_VIEW, for CMD_RENAME_FILES.
*/
void rename_files_of_index( void * vdata )
{
    rename_files_of_index_raw(vdata, true);
    return;
}


/** \brief Rename all files of the current index-view - without prepending to existing basename. Instead replace basename with index-value.

    This function is intended to be called from within INDEX_VIEW, for CMD_RENAME_FILES_REPLACE_BASENAME.
*/
void rename_files_of_index_noprepend( void * vdata )
{
    rename_files_of_index_raw(vdata, true); // this makes all filenames longer - so that shortening can't clash (check if this is needed)
    rename_files_of_index_raw(vdata, false);
    return;
}


// =============================================================================
/** \brief Return boolean that indicates if the command 'cmd' is a sort command.
*/
static bool is_sortcommand( enum scr_cmd   cmd )
{
    bool is_sort_cmd = false;

    if( cmd == CMD_SORT_WIDTH ||
        cmd == CMD_SORT_HEIGHT ||
        cmd == CMD_SORT_ASPECT ||
        cmd == CMD_SORT_SIZE ||
        cmd == CMD_SORT_NAME ||
        cmd == CMD_SORT_NAMESTRUCTURE ||
        cmd == CMD_SORT_MTIME ||
        cmd == CMD_SORT_COLOR_LUMINANCE ||
        cmd == CMD_SORT_COLOR_LAB_AB ||
        cmd == CMD_SORT_COLOR_LAB_B ||
        cmd == CMD_SORT_COLOR
      )
    {
        is_sort_cmd = true;
    }

    return is_sort_cmd;
}
// =============================================================================

typedef void (*GRID_FUNC)(GRID * grd);


/** \brief Call the GRID_FUNC functions and set the sorting status.

    This function extracts the GRID that is used as argument for the GRID_FUNC
    functions from the vdata and calls the function GrdFun with that argument.

    Intention of this function is to remove boilerplate code for
    GRID-extraction from the GRID_FUNC functions.

    The sort status will also be set.
*/
void call_function_with_grid( void * vdata, GRID_FUNC GrdFun )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID  * grid = navp->grid;

    // if sort-function is used, set NAV->sort_status accordingly
    if( is_sortcommand(stp->cmd) )
    {
        navp->sort_status.is_sorted = true;
        navp->sort_status.inverted  = false;
        navp->sort_status.used_sort_command = stp->cmd;
    }

    if( navp->sort_status.is_sorted == true && stp->cmd == CMD_REVERSE_ORDER )
    {
        navp->sort_status.inverted = ! navp->sort_status.inverted;
    }

    if( stp->cmd == CMD_EXCHANGE_GRID_ENTRIES || stp->cmd == CMD_EXCHANGE_GRID_ENTRIES_VERTICAL )
    {
        navp->sort_status.is_sorted = false;
    }

    (*GrdFun)(grid);

    unlock_currdir(extp);

    return;
}

// -----------------------------------------------------------------------------
/** \brief Move right in the active grid.
*/
void cmd_move_right( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_move_right(navp);

    stp->cmd_done = true;

    unlock_currdir(extp);
    return;
}

// -----------------------------------------------------------------------------

/** \brief Move left in the active grid.
*/
void cmd_move_left( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_move_left(navp);

    stp->cmd_done = true;

    unlock_currdir(extp);
    return;
}

// -----------------------------------------------------------------------------

/** \brief Move up in the active grid.
*/
void cmd_move_up( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_move_up(navp);

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------

/** \brief Move down in the active grid.
*/
void cmd_move_down( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_move_down(navp); // Example: nav_last_pos(navp);

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------

/** \brief Move to the next Dir (from within any view).
*/
void cmd_incr_dir_index( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;

    int old_view = stp->fsel; // save current view

    stp->fsel = DIR_VIEW;  // Switch to the DirsView
    cmd_move_right(vdata); // Increment the DirsView
    stp->fsel = old_view;  // Switch back to former view
}

// -----------------------------------------------------------------------------

/** \brief Move to the next Dir (from within any view).
*/
void cmd_decr_dir_index( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;

    int old_view = stp->fsel; // save current view

    stp->fsel = DIR_VIEW; // Switch to the DirsView
    cmd_move_left(vdata); // Decrement the DirsView
    stp->fsel = old_view; // Switch back to former view
}

// -----------------------------------------------------------------------------

/** \brief Horizontal exchange of two grid entries.
*/
void cmd_exchange_grid_entries( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_exchange_grid_entries_horizontal(navp);
    navp->sort_status.is_sorted = false;

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------

/** \brief Vertical exchange of two grid entries.
*/
void cmd_exchange_grid_entries_vertical( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_exchange_grid_entries_vertical(navp); // Example: nav_last_pos(navp);
    navp->sort_status.is_sorted = false;

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------

/** \brief Replace grid element at pos. 0 with the active grid element,
    and shift down elements from pos 0 to (pos_active - 1) to active position.

    It's a rotation (righ-shift) of the grid from pos. 0 to active element.

    If elem pos. 3 is active,
    the grid [0, 1, 2, 3, 4, 5 ] will be changed to [ 3, 0, 1, 2, 4, 5 ].
*/
void cmd_replace_zero_with_curr_shift_right( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    grid_rotate_right_raw(navp->grid, 0, grid_get_index_raw(navp->grid));


    navp->sort_status.is_sorted = false;

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------
void cmd_replace_last_with_curr_shift_left( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    grid_rotate_left_raw(navp->grid, grid_get_index_raw(navp->grid), grid_get_last_index(navp->grid));

    navp->sort_status.is_sorted = false;

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------


/** \brief Reverse order of the active grid.
*/
void cmd_reverse_order( void * vdata ) // CMD_REVERSE_ORDER
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;

    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);
    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_reverse_order(navp);

    unlock_currdir(extp);

}
// -----------------------------------------------------------------------------


/** \brief Grid entry at pos. 0 is set to be the active image. (Go to pos. 0)
*/
void cmd_first_pos( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_first_pos(navp);

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------
/** \brief Grid entry at last position is set to be the active image. (Go to last pos.)
*/
void cmd_last_pos( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;

    nav_last_pos(navp);

    stp->cmd_done = true;

    unlock_currdir(extp);

    return;
}


// -----------------------------------------------------------------------------
/** \brief Grid entry that is marked and has higher index than curr pos is set to be the active image. (Go to next marked image.) Wrap-around search at end.

    If there is no marked image, this command does nothing.
    If there are marked images, but only with lower index values,
    then the current index/position, serach starts again from 0.
    This means next image index in this case is the first found from the beginning.
    (Wrap-around search.)
*/
void cmd_next_marked_pos( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID  * image_grid = navp->grid;

    int num_marked  = extp->currdir->num_of_marked;
    bool marks_used = num_marked > 0;

    int curridx = grid_get_index(image_grid);
    int gridlen = grid_length(image_grid);
    int next_marked = -1;

    if( marks_used )
    {
        // look-up from curridx until end
        // ------------------------------
        for( int idx = curridx; idx < gridlen && next_marked == -1; idx++ )
        {
            IMAGE * img = grid_get_payload_by_index(image_grid, idx);
            if( is_marked_general(&img->mark) && idx > curridx ) // accept only index of a marked file, higher than curridx
            {
                next_marked = idx;
            }
        }

        // look-up from 0 until (including) curridx
        // ----------------------------------------
        for( int idx = 0; idx <= curridx && next_marked == -1; idx++ )
        {
            IMAGE * img = grid_get_payload_by_index(image_grid, idx);
            if( is_marked_general(&img->mark) ) // any index of marked image accepted here
            {
                next_marked = idx;
            }
        }

        if( next_marked >= 0 )
        {
            nav_goto_pos(navp, next_marked);
        }
    }

    stp->cmd_done = true;

    unlock_currdir(extp);

    return;
}


// -----------------------------------------------------------------------------
/** \brief Grid entry that is marked and has lower index than curr pos is set to be the active image. (Go to previous marked image.) Wrap-around search at end.

    If there is no marked image, this command does nothing.
    If there are marked images, but only with higher index values,
    then the current index/position, search starts again from last image.
    This means 'previous' image index in this case is the first found from the end.
    (Wrap-around search.)
*/
void cmd_prev_marked_pos( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID  * image_grid = navp->grid;

    int num_marked  = extp->currdir->num_of_marked;
    bool marks_used = num_marked > 0;

    int curridx = grid_get_index(image_grid);
    int last_idx = grid_length(image_grid) -1;
    int next_marked = -1;

    if( marks_used )
    {
        // look-up from curridx until end
        // ------------------------------
        for( int idx = curridx; idx >= 0 && next_marked == -1; idx-- )
        {
            IMAGE * img = grid_get_payload_by_index(image_grid, idx);
            if( is_marked_general(&img->mark) && idx < curridx ) // accept only index of a marked file, higher than curridx
            {
                next_marked = idx;
            }
        }

        // look-up from 0 until (including) curridx
        // ----------------------------------------
        for( int idx = last_idx; idx >= curridx && next_marked == -1; idx-- )
        {
            IMAGE * img = grid_get_payload_by_index(image_grid, idx);
            if( is_marked_general(&img->mark) ) // any index of marked image accepted here
            {
                next_marked = idx;
            }
        }

        if( next_marked >= 0 )
        {
            nav_goto_pos(navp, next_marked);
        }
    }

    stp->cmd_done = true;

    unlock_currdir(extp);

    return;
}

// -----------------------------------------------------------------------------
/** \brief Print all directory names of the DirsView.
*/
void cmd_print_dirs_names( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID   * dcgrid = navp->grid;

    printf("\n# Directories:\n");
    for( int dirsel = 0; dirsel < grid_length(dcgrid); dirsel++ )
    {
        DIRCONTENTS * dc = grid_get_payload_by_index(dcgrid, dirsel);

        IMAGE * img = *dc->rpl;

        PATHFIDDLE pf = calculate_paths(img->abspath);
        printf("\"%s\"\n", pf.dirname);
    }

    unlock_currdir(extp);

    fflush(stdout);

    return;
}


/** \brief Print all filenames.
*/
void cmd_print_images_names( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID   * image_grid = navp->grid;

    printf("\n# (%d) Filenames:\n", grid_length(image_grid));
    for( int idx = 0; idx < grid_length(image_grid); idx++ )
    {
        IMAGE * img = grid_get_payload_by_index(image_grid, idx);
        printf("\"%s\"\n", img->abspath);
    }

    unlock_currdir(extp);

    fflush(stdout);

    return;
}


/** \brief If images are marked, print only name of these files. If no image is marked, print all filenames.
*/
void cmd_print_images_names_if_marked( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID   * image_grid = navp->grid;

    int num_marked  = extp->currdir->num_of_marked;
    bool marks_used = num_marked > 0;

    printf("\n# (%d) marked out ot of (%d) Filenames:\n", num_marked, grid_length(image_grid));
    for( int idx = 0; idx < grid_length(image_grid); idx++ )
    {
        IMAGE * img = grid_get_payload_by_index(image_grid, idx);

        if(  (! marks_used) || (marks_used && is_marked_general(&img->mark)) )
        {
            printf("\"%s\"\n", img->abspath);
        }
    }

    unlock_currdir(extp);

    fflush(stdout);

    return;
}


/** \brief Print the name of the active image.
*/
void cmd_print_image_name( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID      * image_grid = navp->grid;

    IMAGE * img = grid_get_payload(image_grid);
    printf("\n# Filename:\n");
    printf("\"%s\"\n", img->abspath);
    stp->cmd_done = true;

    unlock_currdir(extp);

    fflush(stdout);
}


/** \brief Toggle the horizontal flip.
*/
void cmd_toggle_hflip( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID      * image_grid = navp->grid;

    IMAGE * img = grid_get_payload(image_grid);

    if( orientation_is_hflip(img) == false )
    {
        orientation_set_hflip(img);
    }
    else
    {
        orientation_unset_hflip(img);
    }

    stp->cmd_done = true;

    unlock_currdir(extp);
}



/** \brief Toggle the vertical flip.
*/
void cmd_toggle_vflip( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID      * image_grid = navp->grid;

    IMAGE * img = grid_get_payload(image_grid);

    if( orientation_is_vflip(img) == false )
    {
        orientation_set_vflip(img);
    }
    else
    {
        orientation_unset_vflip(img);
    }

    stp->cmd_done = true;

    unlock_currdir(extp);
}


/** \brief Set the orientation of the image: rotate counter clockwise by 90 degrees.
*/
void cmd_rotate_ccw( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID      * image_grid = navp->grid;

    IMAGE * img = grid_get_payload(image_grid);

    ROTATION rot = orientation_get_rotate(img);

    switch(rot)
    {
        case ROT_0:
                orientation_set_rotate(img, ROT_90);
            break;

        case ROT_90:
                orientation_set_rotate(img, ROT_180);
            break;

        case ROT_180:
                orientation_set_rotate(img, ROT_270);
            break;

        case ROT_270:
                orientation_set_rotate(img, ROT_0);
            break;
    }

    stp->cmd_done = true;

    unlock_currdir(extp);
}



/** \brief Set the orientation of the image: rotate clockwise by 90 degrees.
*/
void cmd_rotate_cw( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID      * image_grid = navp->grid;

    IMAGE * img = grid_get_payload(image_grid);

    ROTATION rot = orientation_get_rotate(img);

    switch(rot)
    {
        case ROT_0:
                orientation_set_rotate(img, ROT_270);
            break;

        case ROT_90:
                orientation_set_rotate(img, ROT_0);
            break;

        case ROT_180:
                orientation_set_rotate(img, ROT_90);
            break;

        case ROT_270:
                orientation_set_rotate(img, ROT_180);
            break;
    }

    stp->cmd_done = true;

    unlock_currdir(extp);
}



/** \brief Call external program with absolute pathname of the active image.

This function uses system(3), which calls a shell.
As this may cause security problems, this command can be disabled by setting
ENABLE_EXTERNAL_PROGRAM in config.h to false.

The name of the program to be called with the filename is taken from the
environment variable
'PLIO_EXTERNAL_PROGRAM'.

*/
void cmd_open_image_external( void * vdata )
{
    THREAD_DATA * data = vdata;
    USTATE              * stp    = &data->st;
    EXTRACTED           * extp = data->extracted;
    char * cmd_name = NULL;
    char cmd[PATH_MAX];
    cmd[0] = '\0';

    const char * envvar = "PLIO_EXTERNAL_PROGRAM"; // maybe PLIO_SETTINGS und dann prog=gimp, thumbsize=80 usw.

    if( ENABLE_EXTERNAL_PROGRAM != true )
    {
        fprintf(stderr, "calling external program is not enabled in configuration file\n");
        return;
    }

    extract_currdir(extp);

    NAV   * navp = (stp->fsel == DIR_VIEW) ? extp->collnav : extp->dirnav;
    GRID      * image_grid = navp->grid;

    IMAGE * img = grid_get_payload(image_grid);
    printf("\n# Filename (would open the image in external program):\n");
    printf("%s\n", img->abspath);

    cmd_name = getenv(envvar);

    if( cmd_name != NULL )
    {
        sprintf(cmd, "%s %s\n", cmd_name, img->abspath);
        system(cmd);
    }
    else
    {
        fprintf(stderr, "Environment variable %s is not set - no external program can be used\n", envvar);
    }

    stp->cmd_done = true;

    unlock_currdir(extp);
}

// -----------------------------------------------------------------------------

/** \brief Toggle full screen view.
*/
void cmd_toggle_fullscreen( void * vdata )
{
    THREAD_DATA * data = vdata;
    APP * app = data->app;

    toggle_fullscreen(app);

    return;
}

// -----------------------------------------------------------------------------

static void cmd_img_sort_width_raw( GRID * grid ) // CMD_SORT_WIDTH
{
    grid_sort(grid, sizeof(IMAGE*), compare_width);
}

void cmd_img_sort_width( void * vdata )
{
    call_function_with_grid(vdata, cmd_img_sort_width_raw);
}

// -----------------------------------------------------------------------------

static void cmd_img_sort_height_raw( GRID * grid ) // CMD_SORT_HEIGHT
{
    grid_sort(grid, sizeof(IMAGE*), compare_height);
}

void cmd_img_sort_height( void * vdata )
{
    call_function_with_grid(vdata, cmd_img_sort_height_raw);
}

// -----------------------------------------------------------------------------

static void imagegrid_sort_by_size_then_aspect_raw( GRID * grid ) // CMD_SORT_SIZE
{
    grid_sort(grid, sizeof(IMAGE*), compare_combine_size_aspect);
}

void cmd_img_sort_size_then_aspect( void * vdata )
{
    call_function_with_grid(vdata, imagegrid_sort_by_size_then_aspect_raw);
}

// -----------------------------------------------------------------------------

static void cmd_img_sort_aspect_raw( GRID * grid ) // CMD_SORT_ASPECT
{
    grid_sort(grid, sizeof(IMAGE*), compare_combine_aspect_size);
}

void cmd_img_sort_aspect( void * vdata )
{
    call_function_with_grid(vdata, cmd_img_sort_aspect_raw);
}

// -----------------------------------------------------------------------------

static void cmd_img_sort_name_raw( GRID * grid ) // CMD_SORT_NAME
{
    grid_sort(grid, sizeof(IMAGE*), compare_name);
}

void cmd_img_sort_name( void * vdata )
{
    call_function_with_grid(vdata, cmd_img_sort_name_raw);
}

// -----------------------------------------------------------------------------

static void cmd_img_sort_namestructure_then_name_raw( GRID * grid ) // CMD_SORT_NAMESTRUCTURE
{
    grid_sort(grid, sizeof(IMAGE*), compare_combine_namestruct_name);
}

void cmd_img_sort_namestructure( void * vdata )
{
    call_function_with_grid(vdata, cmd_img_sort_namestructure_then_name_raw);
}

// -----------------------------------------------------------------------------

static void imagegrid_sort_by_modtime_raw( GRID * grid ) // CMD_SORT_MTIME
{
    grid_sort(grid, sizeof(IMAGE*), compare_modtime);
}

void cmd_img_sort_by_modtime( void * vdata )
{
    call_function_with_grid(vdata, imagegrid_sort_by_modtime_raw);
}

// -----------------------------------------------------------------------------

static void imagegrid_sort_by_luminance_raw( GRID * grid ) // CMD_SORT_COLOR_LUMINANCE
{
    grid_sort(grid, sizeof(IMAGE*), compare_centroid_luminance);
}

void cmd_img_sort_by_luminance( void * vdata )
{
    call_function_with_grid(vdata, imagegrid_sort_by_luminance_raw);
}

// -----------------------------------------------------------------------------

static void imagegrid_sort_by_lab_ab_raw( GRID * grid ) // CMD_SORT_COLOR_LAB_AB
{
    grid_sort(grid, sizeof(IMAGE*), compare_centroid_lab_ab);
}

void cmd_img_sort_by_lab_ab( void * vdata )
{
    call_function_with_grid(vdata, imagegrid_sort_by_lab_ab_raw);
}

// -----------------------------------------------------------------------------

static void imagegrid_sort_by_lab_ab_atan2_raw( GRID * grid ) // CMD_SORT_COLOR_LAB_AB_ATAN2
{
    grid_sort(grid, sizeof(IMAGE*), compare_centroid_lab_ab_atan2);
}

void cmd_img_sort_by_lab_ab_atan2( void * vdata )
{
    call_function_with_grid(vdata, imagegrid_sort_by_lab_ab_atan2_raw);
}

// -----------------------------------------------------------------------------

static void dirgrid_sort_by_name( GRID * grid )
{
    grid_sort(grid, sizeof(DIRCONTENTS*), comp_dircontents_by_dirname);
}

void cmd_dir_sort_by_name( void * vdata )
{
    call_function_with_grid(vdata, dirgrid_sort_by_name);
}

// -----------------------------------------------------------------------------

static void dirgrid_sort_by_num( GRID * grid )
{
    grid_sort(grid, sizeof(DIRCONTENTS*), dircontents_compare_num_entries);
}

void cmd_dir_sort_by_num( void * vdata )
{
    call_function_with_grid(vdata, dirgrid_sort_by_num);
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// =============================================================================



// =====================================================================================================================
/*
case CMD_PREPARE_XY_GRID: (Fuer INDEX_VIEW)
{
    printf("CMD_PREPARE_XY_GRID - Thumbview\n");
    stp->cmd_done = true;
    stp->view_changed = false; // only true, if nothing in the display changed (text-message?)
}
*/

/* seems to work well.
case CMD_SORT_NAME: (for DIR_VIEW)
{
    printf("CMD_SORT_NAME - DirsView\n");
    qsort(&dcgrid->payload[0], (size_t) grid_length(dcgrid), sizeof(DIRCONTENTS_OLD*), dircontents_compare_dirname);
    stp->cmd_done = true;
}
break;
*/


/* Crasht, irgendwo in list.c
case CMD_SORT_SIZE: (for DIR_VIEW)
{
    printf("CMD_SORT_SIZE - DirsView\n");
    qsort(&dcgrid->payload[0], (size_t) grid_length(dcgrid), sizeof(DIRCONTENTS_OLD*), dircontents_compare_num_entries);
    stp->cmd_done = true;
}
break;
*/

/* More commands, that should be added to the statemachine-config...

    if( STATEMATCH(DIR_VIEW, CMD_INCR_DIR_INDEX) )
    {
        //(*dcollp->scroll.incr)(&dcollp->scroll);
        (*dcollp->nav.grid->xdecr)(dcollp->nav.grid);
        return;
    }

    // --- Commands that use 'tricks' like calling do_..._states() functions ---

    if( STATEMATCH(INDEX_VIEW, CMD_DECR_DIR_INDEX) || STATEMATCH(IMAGE_VIEW, CMD_DECR_DIR_INDEX) )
    {
        printf("----------- DECR_INDEX ------- WTF\n");
        IMAGE * currimg = NULL;
        USTATE ust = *stp;
        USTATE * ustp = &ust;

        ust.fsel = INDEX_VIEW;
        ust.key  = LEFT;
        ust.cmd_avail  = false;
        ust.cmd_done   = false;
        ust.cmd_failed = false;

        return;
    }

    if( STATEMATCH(INDEX_VIEW, CMD_INCR_DIR_INDEX) || STATEMATCH(IMAGE_VIEW, CMD_INCR_DIR_INDEX) )
    {
        printf("----------- INCR_INDEX ------- WTF\n");
        IMAGE * currimg = NULL;
        USTATE ust = *stp;
        USTATE * ustp = &ust;

        ust.fsel = INDEX_VIEW;
        ust.key  = RIGHT;
        ust.cmd_avail  = false;
        ust.cmd_done   = false;
        ust.cmd_failed = false;

        return;
    }
*/
