/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __DISPLAY_DIR__
#define __DISPLAY_DIR__

#include "common.h"
#include "indexlayout.h"
#include "collection.h"

void setup_initial_dirview( APP * app, DIR_COLLECTION * dcollp, INDEX_LAYOUT * layout );
void draw_dirsview_thumbs_frames( APP * app, INDEX_LAYOUT * layout, NAV * navp );

SELECTED_ITEMS make_dirsview_title(void);
SELECTED_ITEMS make_dirsview_footer(void);

#endif
