/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __DISPLAY__
#define __DISPLAY__

#include "common.h"
#include "image.h"
#include "grid.h"
#include "range.h"
#include "indexlayout.h"
#include "collection.h"

// Frames
// ------
void draw_frame( SDL_Renderer * renderer, Uint8 * col, int xpos, int ypos, int width, int height, int thickness );
void draw_thick_frame_for_active_image( APP * app, INDEX_LAYOUT * layout, int display_idx, GRID * image_grid, int win_w, int win_h );
void draw_thumb_frames( SDL_Renderer * renderer, uint8_t * drawcol, INDEX_LAYOUT * layout, int anzahl, int win_w, int win_h );
void display_marks( SDL_Renderer * renderer, INDEX_LAYOUT * layout, GRID * image_grid, RANGE * range, int win_w, int win_h );

// Screen related
// --------------
void clear_screen( APP * app, bool present );

// Text
// ----
void display_text( SDL_Renderer * renderer , TTF_Font * font, char * text, SDL_Color color, int xpos, int ypos);
void show_index_number( APP * app, int win_w, int win_h, INDEX_LAYOUT * layout, GRID * image_grid, RANGE * imgidx_range );
void render_header_and_footer( APP * app, INDEX_LAYOUT * layout, NAV * navp, SELECTED_ITEMS * title, SELECTED_ITEMS * footer );

// Image / Thumbs
// --------------
void render_copy_thumbnail( SDL_Renderer * renderer, IMAGE * img, SDL_Rect * dest );
void display_image_trimmed( SDL_Renderer * renderer, IMAGE * img, int win_w, int win_h );

// combined stuff
// --------------
void display_index_thumbs_of_grid_via_range( SDL_Renderer * renderer, INDEX_LAYOUT * layout, GRID * image_grid, RANGE * range, int win_w, int win_h );

// Windowsize
// ----------
void toggle_fullscreen( APP * app );

// Scaling-related functions
double get_dpi_scaling( int display_idx );
double get_max_dpi_scaling( APP * app );

#endif
