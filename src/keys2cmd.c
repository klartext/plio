/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdbool.h>
#include <stdio.h>

#include "commandenums.h"
#include "keysenums.h"
#include "keys2cmd.h"


/** \brief Map keycodes to commands for Dir-View.
*/
static void keycodes_to_commands_dir( int key, USTATE * stp )
{
    int cmd = CMD_NOTHING;
    stp->cmd_avail = false;

    //printf("min = %d, max = %d\n", view->min, view->max);
    //print_ragprop(&prop); // print properties

    switch(key)
    {
        case RIGHT: { cmd = CMD_RIGHT;  } break;
        case LEFT:  { cmd = CMD_LEFT;   } break;
        case DOWN:  { cmd = CMD_DOWN;   } break;
        case UP:    { cmd = CMD_UP;     } break;

        case ENTER: { cmd = CMD_SWITCH_INDEXVIEW; } break;
        case ALPH_e:{ cmd = CMD_SWITCH_INDEXVIEW; } break;

        case 'd':   { cmd = CMD_SCREENDUMP; } break;

        case CTRL_n:   { cmd = CMD_SORT_NAME; } break;
        //case CTRL_m:   { cmd = CMD_SORT_NAMESTRUCTURE; } break;
        case CTRL_r:   { cmd = CMD_REVERSE_ORDER; } break;
        case CTRL_s:   { cmd = CMD_SORT_SIZE; } break;

        case ALPH_p:   { cmd = CMD_PRINT_NAMES_OF_MARKED; } break;
        case ALPH_P:   { cmd = CMD_PRINT_NAMES; } break;

        case FIRST_POS:  { cmd = CMD_FIRST_POS; } break;
        case LAST_POS:   { cmd = CMD_LAST_POS; } break;
        case PAGE_UP:    { cmd = CMD_PAGE_UP;  } break;
        case PAGE_DOWN:  { cmd = CMD_PAGE_DOWN; } break;

        case CTRL_PAGEUP:    { cmd = CMD_DECR_DIR_INDEX; } break;
        case CTRL_PAGEDOWN:  { cmd = CMD_INCR_DIR_INDEX; } break;

        case CTRL_q:  { cmd = CMD_QUIT;   } break;

        case SYM_CARET:  { cmd = CMD_TOGGLE_FULLSCREEN;   } break;
        case ALPH_F:  { cmd = CMD_TOGGLE_FULLSCREEN;   } break; // synonyme for caret
    }

    stp->cmd = cmd;

    if( cmd != CMD_NOTHING )
    {
        stp->cmd_avail = true;
    }

    return;
}



/** \brief Map keycodes to commands for Thumb-/Index-View.
*/
static void keycodes_to_commands_idx( int key, USTATE * stp )
{
    int cmd = CMD_NOTHING;
    stp->cmd_avail = false;

    //printf("min = %d, max = %d\n", view->min, view->max);
    //print_ragprop(&prop); // print properties

    switch(key)
    {
        case RIGHT: { cmd = CMD_RIGHT;  } break;
        case LEFT:  { cmd = CMD_LEFT;   } break;
        case DOWN:  { cmd = CMD_DOWN;   } break;
        case UP:    { cmd = CMD_UP;     } break;

        case ENTER: { cmd = CMD_SWITCH_IMAGEVIEW; } break;
        case ALPH_e:{ cmd = CMD_SWITCH_IMAGEVIEW; } break;

        case 'd':   { cmd = CMD_SCREENDUMP; } break;

        case CTRL_w:   { cmd = CMD_SORT_WIDTH; } break;
        case CTRL_h:   { cmd = CMD_SORT_HEIGHT; } break;
        case CTRL_s:   { cmd = CMD_SORT_SIZE; } break;
        case CTRL_a:   { cmd = CMD_SORT_ASPECT; } break;
        case CTRL_n:   { cmd = CMD_SORT_NAME; } break;
        case CTRL_m:   { cmd = CMD_SORT_NAMESTRUCTURE; } break;
        case CTRL_t:   { cmd = CMD_SORT_MTIME; } break;

        case CTRL_u:   { cmd = CMD_SORT_COLOR_LUMINANCE; } break;
        case CTRL_v:   { cmd = CMD_SORT_COLOR_LAB_AB_ATAN2; } break;
        //case 'c':   { cmd = CMD_SORT_COLOR; } break;

        case CTRL_r:   { cmd = CMD_REVERSE_ORDER; } break;

        // EXHANGE Elements
        case CTRL_x:  { cmd = CMD_EXCHANGE_GRID_ENTRIES;   } break;
        case CTRL_y:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_VERTICAL;   } break;

        // EXHANGE Elements - synonyme without Ctrl for faster/easier moving of images
        case ALPH_x:  { cmd = CMD_EXCHANGE_GRID_ENTRIES;   } break;
        case ALPH_y:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_VERTICAL;   } break;

        case ALPH_o:   { cmd = CMD_OPEN_IMAGEFILE_EXTERN; } break;
        case ALPH_p:   { cmd = CMD_PRINT_NAMES_OF_MARKED; } break;
        case ALPH_P:   { cmd = CMD_PRINT_NAMES; } break;
        case ALPH_r:   { cmd = CMD_RENAME_FILES; } break;
        case ALPH_R:   { cmd = CMD_RENAME_FILES_REPLACE_BASENAME; } break;

        case ALPH_m:   { cmd = CMD_TOGGLE_GENERAL_IMAGE_MARK; } break;
        case ALPH_U:   { cmd = CMD_UNSET_GENERAL_IMAGE_MARK; } break;

        case FIRST_POS:  { cmd = CMD_FIRST_POS; } break;
        case LAST_POS:   { cmd = CMD_LAST_POS; } break;
        case PAGE_UP:    { cmd = CMD_PAGE_UP;  } break;
        case PAGE_DOWN:  { cmd = CMD_PAGE_DOWN; } break;
        case ALPH_n:     { cmd = CMD_NEXT_MARKED_POS; } break;
        case ALPH_N:     { cmd = CMD_PREV_MARKED_POS; } break;

        case CTRL_PAGEUP:  { cmd = CMD_DECR_DIR_INDEX; } break;
        case CTRL_PAGEDOWN:  { cmd = CMD_INCR_DIR_INDEX; } break;
        case CTRL_0:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE; } break;
        case ALPH_0:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE; } break; // maybe '0' would also be good for just move to 0

        case CTRL_9:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST; } break;
        case ALPH_9:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST; } break; // maybe '9' would also be good for just move to 9

        case ALPH_q:  { cmd = CMD_SWITCH_DIRVIEW;   } break;

        case SYM_CARET:  { cmd = CMD_TOGGLE_FULLSCREEN;   } break;
        case ALPH_F:  { cmd = CMD_TOGGLE_FULLSCREEN;   } break; // synonyme for caret
    }

    stp->cmd = cmd;

    if( cmd != CMD_NOTHING )
    {
        stp->cmd_avail = true;
    }

    return;
}



/** \brief Map keycodes to commands for Image-View.
*/
static void keycodes_to_commands_img( int key, USTATE * stp )
{
    int cmd = CMD_NOTHING;
    stp->cmd_avail = false;

    //printf("min = %d, max = %d\n", view->min, view->max);
    //print_ragprop(&prop); // print properties

    switch(key)
    {
        case RIGHT: { cmd = CMD_RIGHT;  } break;
        case LEFT:  { cmd = CMD_LEFT;   } break;
        case DOWN:  { cmd = CMD_DOWN;   } break;
        case UP:    { cmd = CMD_UP;     } break;

        case ENTER: { cmd = CMD_SWITCH_INDEXVIEW; } break;
        case ALPH_e:{ cmd = CMD_SWITCH_INDEXVIEW; } break;

        case 'd':   { cmd = CMD_SCREENDUMP; } break;

        case CTRL_w:   { cmd = CMD_SORT_WIDTH; } break;
        case CTRL_h:   { cmd = CMD_SORT_HEIGHT; } break;
        case CTRL_s:   { cmd = CMD_SORT_SIZE; } break;
        case CTRL_a:   { cmd = CMD_SORT_ASPECT; } break;
        case CTRL_n:   { cmd = CMD_SORT_NAME; } break;
        case CTRL_m:   { cmd = CMD_SORT_NAMESTRUCTURE; } break;
        case CTRL_t:   { cmd = CMD_SORT_MTIME; } break;
        //case 'c':   { cmd = CMD_SORT_COLOR; } break;

        case CTRL_r:   { cmd = CMD_REVERSE_ORDER; } break;

        case ALPH_o:   { cmd = CMD_OPEN_IMAGEFILE_EXTERN; } break;
        case ALPH_p:   { cmd = CMD_PRINT_NAMES_OF_MARKED; } break;
        case ALPH_P:   { cmd = CMD_PRINT_NAMES; } break;

        /*
        case ALPH_s:   { cmd = CMD_PRINT_NAMES; } break;
        case ALPH_r:   { cmd = CMD_PRINT_NAMES; } break;
        */
        case ALPH_m:   { cmd = CMD_TOGGLE_GENERAL_IMAGE_MARK; } break;

        case FIRST_POS:  { cmd = CMD_FIRST_POS; } break;
        case LAST_POS:   { cmd = CMD_LAST_POS; } break;
        case PAGE_UP:    { cmd = CMD_PAGE_UP;  } break;
        case PAGE_DOWN:  { cmd = CMD_PAGE_DOWN; } break;
        case ALPH_n:     { cmd = CMD_NEXT_MARKED_POS; } break;
        case ALPH_N:     { cmd = CMD_PREV_MARKED_POS; } break;

        case CTRL_PAGEUP:  { cmd = CMD_DECR_DIR_INDEX; } break;
        case CTRL_PAGEDOWN:  { cmd = CMD_INCR_DIR_INDEX; } break;

        case ALPH_q:  { cmd = CMD_SWITCH_DIRVIEW;   } break;
        case CTRL_0:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE; } break;
        case ALPH_0:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE; } break; // maybe '0' would also be good for just move to 0

        case CTRL_9:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST; } break;
        case ALPH_9:  { cmd = CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST; } break; // maybe '9' would also be good for just move to 9

        case SYM_VBAR:        { cmd = CMD_FLIP_HOR; } break;
        case SYM_UNDERSCORE:  { cmd = CMD_FLIP_VERT; } break;
        case SYM_LT:  { cmd = CMD_ROTATE_CCW; } break;
        case SYM_GT:  { cmd = CMD_ROTATE_CW; } break;

        case SYM_CARET:  { cmd = CMD_TOGGLE_FULLSCREEN;   } break;
        case ALPH_F:  { cmd = CMD_TOGGLE_FULLSCREEN;   } break; // synonyme for caret
    }

    stp->cmd = cmd;

    if( cmd != CMD_NOTHING )
    {
        stp->cmd_avail = true;
    }

    return;
}


void keycodes_to_commands( int keyval, USTATE * stp )
{
    switch( stp->fsel )
    {
        case DIR_VIEW:   { keycodes_to_commands_dir(keyval, stp); } break;
        case INDEX_VIEW: { keycodes_to_commands_idx(keyval, stp); } break;
        case IMAGE_VIEW: { keycodes_to_commands_img(keyval, stp); } break;

        /*
        case XY_GRID:
        {
        }
        break;

        case XY_IMAGEVIEW:
        {
        }
        break;
        */

        case  UNSELECTED:
        default:
        {
            fprintf(stderr, "unknown fsel value detected\n");
            abort();
        }
    }

    return;
}
