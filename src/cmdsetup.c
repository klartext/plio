/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <string.h>

#include "cmdsetup.h"
#include "commandenums.h"
#include "commands.h"
#include "statemachine.h"
//#include "ctrlloop_dir.h"

void setup_cmd_statemachine(APP * app)
{
    bzero(&app->stm.stmvec, sizeof(app->stm.stmvec));

    int idx = 0;

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_SCREENDUMP }, .fun = dump_screen, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SCREENDUMP }, .fun = dump_screen, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_SCREENDUMP }, .fun = dump_screen, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_SWITCH_INDEXVIEW }, .fun = NULL, .out =  { INDEX_VIEW } }; // view changed

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SWITCH_DIRVIEW }, .fun = NULL, .out =  { DIR_VIEW } }; // view changed
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_SWITCH_DIRVIEW }, .fun = NULL, .out =  { DIR_VIEW } }; // view changed

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SWITCH_IMAGEVIEW }, .fun = NULL, .out =  { IMAGE_VIEW } }; // view changed

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW, CMD_SWITCH_INDEXVIEW }, .fun = NULL, .out =  { INDEX_VIEW } }; // view changed

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_INCR_DIR_INDEX }, .fun = cmd_incr_dir_index, .out =  { KEEP_INSTATE } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_INCR_DIR_INDEX }, .fun = cmd_incr_dir_index, .out =  { KEEP_INSTATE } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_INCR_DIR_INDEX }, .fun = cmd_incr_dir_index, .out =  { KEEP_INSTATE } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_DECR_DIR_INDEX }, .fun = cmd_decr_dir_index, .out =  { KEEP_INSTATE } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_DECR_DIR_INDEX }, .fun = cmd_decr_dir_index, .out =  { KEEP_INSTATE } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_DECR_DIR_INDEX }, .fun = cmd_decr_dir_index, .out =  { KEEP_INSTATE } };


    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_RENAME_FILES }, .fun = rename_files_of_index, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_RENAME_FILES_REPLACE_BASENAME }, .fun = rename_files_of_index_noprepend, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_TOGGLE_GENERAL_IMAGE_MARK }, .fun = toggle_general_image_mark, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_TOGGLE_GENERAL_IMAGE_MARK }, .fun = toggle_general_image_mark, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_UNSET_GENERAL_IMAGE_MARK }, .fun = clear_general_image_marks, .out =  { INDEX_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_RIGHT }, .fun = cmd_move_right, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_RIGHT }, .fun = cmd_move_right, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_RIGHT }, .fun = cmd_move_right, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_LEFT }, .fun = cmd_move_left, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_LEFT }, .fun = cmd_move_left, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_LEFT }, .fun = cmd_move_left, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_UP }, .fun = cmd_move_up, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_UP }, .fun = cmd_move_up, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_UP }, .fun = cmd_move_up, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_DOWN }, .fun = cmd_move_down, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_DOWN }, .fun = cmd_move_down, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_DOWN }, .fun = cmd_move_down, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_REVERSE_ORDER }, .fun = cmd_reverse_order, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_REVERSE_ORDER }, .fun = cmd_reverse_order, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_REVERSE_ORDER }, .fun = cmd_reverse_order, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_FIRST_POS }, .fun = cmd_first_pos, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_FIRST_POS }, .fun = cmd_first_pos, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_FIRST_POS }, .fun = cmd_first_pos, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW,   CMD_LAST_POS }, .fun = cmd_last_pos, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_LAST_POS }, .fun = cmd_last_pos, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_LAST_POS }, .fun = cmd_last_pos, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_NEXT_MARKED_POS }, .fun = cmd_next_marked_pos, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_NEXT_MARKED_POS }, .fun = cmd_next_marked_pos, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_PREV_MARKED_POS }, .fun = cmd_prev_marked_pos, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_PREV_MARKED_POS }, .fun = cmd_prev_marked_pos, .out =  { IMAGE_VIEW } };


    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW, CMD_PRINT_NAMES }, .fun = cmd_print_dirs_names, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_PRINT_NAMES }, .fun = cmd_print_images_names, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_PRINT_NAMES }, .fun = cmd_print_image_name, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW, CMD_PRINT_NAMES_OF_MARKED }, .fun = cmd_print_dirs_names, .out =  { DIR_VIEW } }; // no marks-handling for dir impl. so far
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_PRINT_NAMES_OF_MARKED }, .fun = cmd_print_images_names_if_marked, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_PRINT_NAMES_OF_MARKED }, .fun = cmd_print_image_name, .out =  { IMAGE_VIEW } }; // no marks-handling for single image

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_OPEN_IMAGEFILE_EXTERN }, .fun = cmd_open_image_external, .out =  { IMAGE_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_OPEN_IMAGEFILE_EXTERN }, .fun = cmd_open_image_external, .out =  { INDEX_VIEW } };


    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_HEIGHT }, .fun = cmd_img_sort_height, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_WIDTH }, .fun = cmd_img_sort_width, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_SIZE }, .fun = cmd_img_sort_size_then_aspect, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_ASPECT }, .fun = cmd_img_sort_aspect, .out =  { INDEX_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_NAME }, .fun = cmd_img_sort_name, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW, CMD_SORT_NAME }, .fun = cmd_dir_sort_by_name, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW, CMD_SORT_SIZE }, .fun = cmd_dir_sort_by_num, .out =  { DIR_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_NAMESTRUCTURE }, .fun = cmd_img_sort_namestructure, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_MTIME }, .fun = cmd_img_sort_by_modtime, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_COLOR_LUMINANCE }, .fun = cmd_img_sort_by_luminance, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_COLOR_LAB_AB }, .fun = cmd_img_sort_by_lab_ab, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_SORT_COLOR_LAB_AB_ATAN2 }, .fun = cmd_img_sort_by_lab_ab_atan2, .out =  { INDEX_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_EXCHANGE_GRID_ENTRIES }, .fun = cmd_exchange_grid_entries, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_EXCHANGE_GRID_ENTRIES_VERTICAL }, .fun = cmd_exchange_grid_entries_vertical, .out =  { INDEX_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE }, .fun = cmd_replace_zero_with_curr_shift_right, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_EXCHANGE_GRID_ENTRIES_0_VS_ACTIVE }, .fun = cmd_replace_zero_with_curr_shift_right, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST }, .fun = cmd_replace_last_with_curr_shift_left, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_EXCHANGE_GRID_ENTRIES_ACTIVE_VS_LAST }, .fun = cmd_replace_last_with_curr_shift_left, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_FLIP_HOR }, .fun = cmd_toggle_hflip, .out =  { IMAGE_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_FLIP_VERT }, .fun = cmd_toggle_vflip, .out =  { IMAGE_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_ROTATE_CCW }, .fun = cmd_rotate_ccw, .out =  { IMAGE_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_ROTATE_CW }, .fun = cmd_rotate_cw, .out =  { IMAGE_VIEW } };

    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { DIR_VIEW, CMD_TOGGLE_FULLSCREEN }, .fun = cmd_toggle_fullscreen, .out =  { DIR_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { INDEX_VIEW, CMD_TOGGLE_FULLSCREEN }, .fun = cmd_toggle_fullscreen, .out =  { INDEX_VIEW } };
    app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { IMAGE_VIEW, CMD_TOGGLE_FULLSCREEN }, .fun = cmd_toggle_fullscreen, .out =  { IMAGE_VIEW } };

    //app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { X_IMAGE_VIEW, Y_CMD_SWITCH_INDEXVIEW }, .fun = NULL, .out =  { Z_INDEX_VIEW } }; // view changed
    //app->stm.stmvec[idx++] = (STATE_ACTION) { .in = { X_IMAGE_VIEW, Y_CMD_SWITCH_INDEXVIEW }, .fun = NULL, .out =  { Z_INDEX_VIEW } }; // view changed


    app->stm.num_stm_entries = idx;

    return;
}
