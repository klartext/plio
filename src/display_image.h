/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __DISPLAY_IMAGE__
#define __DISPLAY_IMAGE__

#include <stdbool.h>

#include "grid.h"
#include "common.h"

void display_imageview( APP * app, int win_w, int win_h, GRID * image_grid );

#endif
