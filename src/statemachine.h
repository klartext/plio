/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __STATEMACHINE__
#define __STATEMACHINE__

#include <stdbool.h>

#define STATEMACHINE_COMMANDVEC_LEN 200

// -----------------------------------------------------------------------------
typedef struct _statemachine_input_ STM_IN;
struct _statemachine_input_
{
    int fsel;
    int cmd;
};
// -----------------------------------------------------------------------------
typedef struct _statemachine_output_ STM_OUT;
struct _statemachine_output_
{
    int fsel;
};

void print_statemachine( STM_IN * stm );
bool stm_equal( STM_IN * first, STM_IN * second );
bool stm_matches( STM_IN * stm, int funsel, int sub_state );
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

typedef void (*ACTION)(void *);

typedef struct _in_action_out_ STATE_ACTION;
struct _in_action_out_
{
    STM_IN  in;
    ACTION  fun;
    STM_OUT out;
};
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

typedef struct _state_machine_ STATE_MACHINE;

struct _state_machine_
{
    STATE_ACTION stmvec[STATEMACHINE_COMMANDVEC_LEN];
    int num_stm_entries;
};

void print_stm_input( STM_IN * stm );

bool on_statematch_from_statevec_apply_func( STM_IN * istate, STATE_ACTION * states_vector, int statesvec_len, STM_OUT * ostate, void * vdata );

#endif
