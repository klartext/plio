/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __MUTEXSUPPORT__
#define __MUTEXSUPPORT__

#include <pthread.h>

typedef struct mutex_with_attribute MUTEX_WITH_ATTR;
struct mutex_with_attribute
{
    pthread_mutex_t     mutex;
    pthread_mutexattr_t attr;
    char * name;
};


void initialize_mutex( MUTEX_WITH_ATTR * mut );
void destroy_mutex( MUTEX_WITH_ATTR * mut );
void mutex_set_name( MUTEX_WITH_ATTR * mut, char * name );
void print_mutex_info( MUTEX_WITH_ATTR * mut, char * action );

void lock_mutex( MUTEX_WITH_ATTR * mut );
void unlock_mutex( MUTEX_WITH_ATTR * mut );

#endif
