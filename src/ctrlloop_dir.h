/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __CONTROL_LOOP_DIR__
#define __CONTROL_LOOP_DIR__

#include "image.h"
#include "ustate.h"
#include "threadsupport.h"

typedef enum internal_state_enums { INST_INPUT, INST_READ_IMAGES, INST_RENDER, INST_END } INST_ENUMS;

typedef struct _internal_state_ ISTATE;
struct _internal_state_
{
    INST_ENUMS  istate;
    USTATE      ustate;
};

typedef struct _lasting_items_ LASTING_ITEMS;
struct _lasting_items_
{
    IMAGE * image;  // last image
    int     index;  // last index;
};

int command_eval_wrapper( THREAD_DATA  *args );

#endif
