/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include "selection.h"

/** \brief Depending on the integer value 'num', return either 'singular' or 'plural'.
*/
char * singularplural( int num, char * singular, char * plural )
{
    return (num == 1) ? singular : plural;
}



bool switched_on( bool before, bool after )
{
    return (!before) && after;
}

bool switched_off( bool before, bool after )
{
    return before && (!after);
}

bool did_not_switch( bool before, bool after )
{
    return before == after;
}

bool did_switch( bool before, bool after )
{
    return before != after;
}
