/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __EVENTS__
#define __EVENTS__

#include <stdbool.h>

#include <SDL2/SDL.h>

#include "keysenums.h"

typedef struct read_key READ_KEY;
struct read_key
{
    int  key;
    bool winsize_changed; // set/reset by call to get_keys() to indicate changeof window size
};

typedef struct keymapping KEYMAPPING;
struct keymapping
{
    int in;         // input code
    SDL_Keymod mod; // modifier
    int out; // what's given as result
};

typedef struct _keyresult_ KEYRESULT;
struct _keyresult_
{
    bool shift;   /**< modifier shift */
    bool control; /**< modifier control */
    bool numpad;  /**< modifier numpad */
    int key;      /**< the pressed 'normal' key */
};

void delay_and_flush_events( int ms );
bool get_kmapping(struct keymapping * kmap, int len, int input, SDL_Keymod km, int * output);
enum keys get_keys( bool * winsize_changed, int delay_ms );

#endif

