/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __FIBSURFTEX__
#define __FIBSURFTEX__

#include <FreeImage.h>
#include <SDL2/SDL.h>


typedef struct surface_and_fib SURF_FIB;
struct surface_and_fib // this is needed, when using SDL_CreateRGBSurfaceFrom()
{
    SDL_Surface * surface;
    FIBITMAP    * fib;  // can't be freed before img-destroy, because SDL_CreateRGBSurfaceFrom() is used.
};

SDL_Surface * sdl_surface_from_fib( FIBITMAP * fibm );
SDL_Texture * load_image_as_texture( SDL_Renderer * renderer, char * file_abspath );

#endif

