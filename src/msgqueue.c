/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stddef.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "clock_filetime.h"
#include "msgqueue.h"


void send_message(char * msg)
{

    const int bufsize = 8192;
    char buf[bufsize];
    buf[0] = '\0';

    get_clock(buf);
    strcat(buf, ": ");
    strcat(buf, msg);

    /* Create and open a message queue for writing */
    mqd_t mqd = mq_open (MQ_PATH, O_CREAT | O_EXCL | O_WRONLY, 0600, NULL);

    /* Ensure the creation was successful */
    if (mqd == -1)
    {
        if( errno == EEXIST )
        {
            //fprintf(stderr, "Will try to open without O_CREAT | O_EXCL\n");
            mqd = mq_open (MQ_PATH, O_WRONLY, 0600, NULL);
        }
        else
        {
            perror ("mq_open");
            exit (1);
        }
    }

    mq_send (mqd, buf, strlen(buf) + 1, 10);
    mq_close (mqd);

}
