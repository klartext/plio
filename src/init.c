/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

/** \file init.c
     Initialization and shutdown of the application.

     SDL2 and TTF library as well as initial settings for the layout
     will be handled in this file.
*/
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "init.h"
#include "cmdsetup.h"
#include "config.h"
#include "display.h"
#include "macros.h"
#include "version.h"
#include "environment.h"

/*
    PLIO_VERSION_STRING is defined in version.h,
    which has to contain exactly one line of the following form:

#define PLIO_VERSION_STRING "dummy"

    The file will be removed from version control,
    to create less clutter/confusion.

    (Tricks with .gitignore might work, but that file is created in
    the version.h-target in Makefile - and documented here,
    so this should be ok.)
*/
#define WINDOW_TITLE "plio ("PLIO_VERSION_STRING")"

/**
    \brief Initailze SDL2.

    For the initialization SDL_Init() is called.
    If an error occurs, an error message is printed to stderr and the
    program exited afterwards.
*/
void initialize_sdl()
{
    int res = 0;
    res = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    if( res == -1 )
    {
        fprintf(stderr, "SDL_Init() failed!\n");
        exit(EXIT_FAILURE);
    }
    //else
    //{
    //    fprintf(stderr, "SDL_Init() successful!\n");
    //}
    return;
}


/**
    \brief Shut down SDL2.

    After SDL_Quit() is called, a message to stderr is printed.
*/
void shutdown_sdl()
{
    SDL_Quit();
    //fprintf(stderr, "SDL quitted.\n");
    return;
}



/**
    \brief Create a centered SDL2-Window.

*/
SDL_Window * start_centered_window( char *title, int width, int height )
{
    SDL_Window * window = NULL;

    window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);

    if( window == NULL )
    {
        fprintf(stderr, "start_centered_window(): %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    return window;
}

/**
    \brief Destroy the SDL2 window of the application.

*/
void destroy_window(APP * app )
{
    if( app->window )
        SDL_DestroyWindow(app->window);

    return;
}



/** \brief Set up colors.
*/
void init_colors( APP * app )
{
    /** - set background color */
    app->bgcol[0] =  60;
    app->bgcol[1] =  60;
    app->bgcol[2] =  60;
    app->bgcol[3] = 255;

    /** - set drawing color */
    app->drawcol[0] = 240;
    app->drawcol[1] = 240;
    app->drawcol[2] = 240;
    app->drawcol[3] = 255;

    /** - set emphasize color */
    app->emphcol[0] =  19;
    app->emphcol[1] = 212;
    app->emphcol[2] =  20;
    app->emphcol[3] = 255;

    return;
}



/**
    \brief Initialze the application.

*/
void init_app( APP * app )
{
    char * fontname = use_env_string("PLIO_FONTNAME", FONT_NAME);
    app->prescale = use_env_double_nonzero("PLIO_PRESCALE", PRESCALE);
    app->prescale = (app->prescale < 0.01 || app->prescale > 10.0) ? PRESCALE : app->prescale; // just in case
    /* First basic settings */
    /* -------------------- */
    /** - set initial window width and height */
    app->initial_win_w = INITIAL_WINDOW_WIDTH;
    app->initial_win_h = INITIAL_WINDOW_HEIGHT;

    init_colors(app);

    /** - set delay for loops and expected loop time */
    app->delay = 20; // default delay time (for event loops etc.) in milliseconds
    app->looptime_upto = 40; // let main loop need up to this many milliseconds

    /* Now SDL */
    /* ------- */
    initialize_sdl(); /** - call initialize_sdl() */

    /** - create window and renderer */


    app->window   = start_centered_window(WINDOW_TITLE, app->initial_win_w, app->initial_win_h);
    app->renderer = SDL_CreateRenderer(app->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    on_NULL_errexit_with_sdlerrmsg(app->renderer);

    /** - set start_ticks for timing purposes */
    app->start_ticks = SDL_GetTicks();

    // get number of displas (monitors)
    app->num_displays = SDL_GetNumVideoDisplays();
    if( app->num_displays < 0 )
    {
        fprintf(stderr, "Number of displays not available %s\n", SDL_GetError());
        shutdown_app(app);
        exit(EXIT_FAILURE);
    }

    // Allocate Fonts-vector
    app->fontvec = malloc(app->num_displays * sizeof(struct _font_));
    if( app->fontvec == NULL )
    {
        fprintf(stderr, "Could not allocate vector for fonts (not enough memory)\n");
        shutdown_app(app);
        exit(EXIT_FAILURE);
    }

    // TrueType Font Rendering Initialization
    // --------------------------------------
    /** - initialize TTF library */
    if( TTF_Init() < 0 )
    {
        fprintf(stderr, "Could not initialize TTF-lib, exiting.\n");
        shutdown_app(app);
        exit(EXIT_FAILURE);
    }

    // Setting up Fonts
    // ----------------
    for(int display = 0; display < app->num_displays; display++)
    {
        double dpi_scaling = get_dpi_scaling(display);
        dpi_scaling = (dpi_scaling < 1.0) ? 1.0 : dpi_scaling; // don't scale down

        int fontsize = FONTSIZE * app->prescale * dpi_scaling;
        app->fontvec[display].font = TTF_OpenFont(fontname, fontsize);
        if( app->fontvec[display].font == NULL )
        {
            fprintf(stderr, "could not create TTF_Font (fontname: %s, %d point)\n", fontname, fontsize);
        }
    }

    // calculating max scale and size of thumbs, aequivalent to the base-size for max. dpi's
    // -------------------------------------------------------------------------------------
    app->max_scale = app->prescale * get_max_dpi_scaling(app);
    app->thumb_load_size = THUMB_SIDELENGTH * app->max_scale;
fprintf(stderr, "thumb_load_size = %d\n", app->thumb_load_size); // TODO: remove, when pot. scaling problems can be excluded


    // State Machine Setup
    // -------------------
    setup_cmd_statemachine(app);
}



/** \brief Shutdown the application.

*/
void shutdown_app( APP * app )
{
    for( int display = 0; display < app->num_displays; display++)
    {
        TTF_CloseFont(app->fontvec[display].font);
    }
    free(app->fontvec);
    app->fontvec = NULL;

    TTF_Quit(); /** Shutdown the TTF library */

    SDL_DestroyRenderer(app->renderer);
    destroy_window(app); /** call destroy_window() */

    shutdown_sdl(); /** call shutdown_sdl() */

    return;
}
