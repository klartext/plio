/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <stdlib.h>

#include "indexlayout.h"
#include "geometry.h"



INDEX_LAYOUT set_index_layout(
                       int outer_frame,
                       int thumb_sidelength,
                       int minsep,
                       int selection_frame_thickness)
{
    INDEX_LAYOUT bare_layout;

    bare_layout.outer_frame = outer_frame;
    bare_layout.thumb_sidelength = thumb_sidelength;
    bare_layout.minsep = minsep;
    bare_layout.selection_frame_thickness = selection_frame_thickness;

    return  bare_layout;
}



INDEX_LAYOUT scale_index_layout( INDEX_LAYOUT * in, double scale )
{
    INDEX_LAYOUT out;

    out.outer_frame = scale * in->outer_frame;
    out.thumb_sidelength = scale * in->thumb_sidelength;
    out.minsep = scale * in->minsep;
    out.selection_frame_thickness = scale * in->selection_frame_thickness;

    return out;
}



/** \brief Calculate the logical layout from the bare layout, the window size and number of images.
*/
INDEX_DIM initialize_indexview_layout(int win_w, int win_h, INDEX_LAYOUT * bare_layout, int num_images )
{
    INDEX_DIM dim;

    int grid_width  = 0;
    int grid_height = 0;
    int x_padding   = 0;
    int y_padding   = 0;
    int anzahl_thumbs_lastscreen = 0;

    /* calculations */
    /* ------------ */
    calc_itemnum_and_padding(win_w, bare_layout->outer_frame, bare_layout->thumb_sidelength, bare_layout->minsep, &grid_width,  &x_padding);
    calc_itemnum_and_padding(win_h, bare_layout->outer_frame, bare_layout->thumb_sidelength, bare_layout->minsep, &grid_height, &y_padding);

    int anzahl_thumbs_pro_screen       = grid_width * grid_height;
    int anzahl_thumbs_pro_voller_zeile = grid_width;
    int anzahl_zeilen_pro_screen       = anzahl_thumbs_pro_screen / anzahl_thumbs_pro_voller_zeile;

    int anzahl_voller_zeilen                 = num_images / grid_width;
    int anzahl_thumbs_in_teilbesetzter_zeile = num_images % grid_width;

    // anzahl_nicht_auf_screen_passender_zeilen: temporal calculation, if neg. number, it really is 0
    int anzahl_nicht_auf_screen_passender_zeilen = anzahl_voller_zeilen + (anzahl_thumbs_in_teilbesetzter_zeile > 0 ? 1: 0) - anzahl_zeilen_pro_screen;
    int anzahl_scroll_zeilen                     = anzahl_nicht_auf_screen_passender_zeilen > 0 ? anzahl_nicht_auf_screen_passender_zeilen : 0;


    // calculation of anzahl_thumbs_lastscreen
    // ---------------------------------------
    if( num_images < grid_width * grid_height )
    {
        anzahl_thumbs_lastscreen = num_images;
    }
    else if( num_images == grid_width * grid_height )
    {
        anzahl_thumbs_lastscreen = num_images;
    }
    else
    {
        if( num_images % (grid_width * grid_height) == grid_width )
        {
            anzahl_thumbs_lastscreen = anzahl_thumbs_pro_screen;
        }
        else
        {
            anzahl_thumbs_lastscreen = (grid_height - 1) * grid_width + anzahl_thumbs_in_teilbesetzter_zeile;
        }
    }


    // saving the results in the layout-struct
    // ---------------------------------------
    dim.width  = grid_width;
    dim.height = grid_height;
    dim.x_padding = x_padding;
    dim.y_padding = y_padding;

    dim.thumbs_per_screen = anzahl_thumbs_pro_screen;
    dim.thumbs_per_full_row = anzahl_thumbs_pro_voller_zeile;
    dim.rows_per_screen = anzahl_zeilen_pro_screen;
    dim.num_of_full_rows = anzahl_voller_zeilen;
    dim.num_of_thumbs_on_partially_filled_row = anzahl_thumbs_in_teilbesetzter_zeile;
    dim.num_scroll_max = anzahl_scroll_zeilen;
    dim.num_of_thumbs_on_last_screen = anzahl_thumbs_lastscreen;

    /*
    putchar('\n');
    printf("Werte aufgrund der Geometrie des Fensters:\n");
    PRINTINT(anzahl_thumbs_pro_screen);
    PRINTINT(anzahl_thumbs_pro_voller_zeile);
    PRINTINT(anzahl_zeilen_pro_screen);

    putchar('\n');

    printf("Werte unter Berücksichtigung der anzahl der Bild-Dateien\n");
    PRINTINT(num_images);
    PRINTINT(anzahl_voller_zeilen);
    PRINTINT(anzahl_thumbs_in_teilbesetzter_zeile);
    putchar('\n');
    printf("Werte, die speziell für's Scrolling wichtig sind\n");
    PRINTINT(anzahl_scroll_zeilen);
    PRINTINT(anzahl_thumbs_lastscreen);
    putchar('\n');
    */

    return dim;
}



void show_index_layout( INDEX_LAYOUT * lay )
{
    printf("outer_frame:               %4d\n", lay->outer_frame);
    printf("thumb_width:               %4d\n", lay->thumb_sidelength);
    printf("thumb_height:              %4d\n", lay->thumb_sidelength);
    printf("minsep:                    %4d\n", lay->minsep);
    printf("selection_frame_thickness: %4d\n", lay->selection_frame_thickness);

    return;
}



void show_index_dimensions( INDEX_DIM * dim )
{

    printf("width:               %3d\n", dim->width);
    printf("height:              %3d\n", dim->height);
    printf("x_padding:           %3d\n", dim->x_padding);
    printf("y_padding:           %3d\n", dim->y_padding);
    printf("thumbs_per_screen:   %3d\n", dim->thumbs_per_screen);
    printf("thumbs_per_full_row: %3d\n", dim->thumbs_per_full_row);
    printf("rows_per_screen:     %3d\n", dim->rows_per_screen);
    printf("num_of_full_rows:    %3d\n", dim->num_of_full_rows);
    printf("num_scroll_max:      %3d\n", dim->num_scroll_max);
    printf("num_of_thumbs_on_partially_filled_row:  %3d\n", dim->num_of_thumbs_on_partially_filled_row);
    printf("num_of_thumbs_on_last_screen:           %3d\n", dim->num_of_thumbs_on_last_screen);

    return;
}
