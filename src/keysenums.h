/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __KEYS_ENUMS__
#define __KEYS_ENUMS__

enum keys       { NONE,
                  ALPH_e,
                  ALPH_m,
                  ALPH_n,
                  ALPH_o,
                  ALPH_p,
                  ALPH_q,
                  ALPH_r,
                  ALPH_s,
                  ALPH_x,
                  ALPH_y,

                  ALPH_F,
                  ALPH_N,
                  ALPH_P,
                  ALPH_U,
                  ALPH_R,

                  ALPH_0,
                  ALPH_9,
                  SYM_VBAR,
                  SYM_UNDERSCORE,
                  SYM_CARET,
                  SYM_LT,
                  SYM_GT,

                  QUIT_SDL_QUIT, LEFT, RIGHT, UP, DOWN, ENTER, REPEATED,
                  CTRL_w, CTRL_h, CTRL_s, CTRL_a, CTRL_n, CTRL_p,
                  CTRL_q,
                  CTRL_m,
                  CTRL_r,
                  CTRL_t,

                  CTRL_u,
                  CTRL_v,

                  CTRL_x,
                  CTRL_y,

                  CTRL_0,
                  CTRL_9,

                  FIRST_POS, LAST_POS,
                  PAGE_UP, PAGE_DOWN,
                  CTRL_PAGEUP, CTRL_PAGEDOWN,
                  KEYS_ENUM_END
                }; // KEYS_ENUM_END can be used for getting number/count of the enum's

#endif
