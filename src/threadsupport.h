/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __THREADSUPPORT__
#define __THREADSUPPORT__

#include "collection.h"
#include "common.h"
#include "events.h"
#include "image.h"
#include "indexlayout.h"
#include "ustate.h"

typedef struct extracted EXTRACTED;
struct extracted
{
    DIR_COLLECTION * dcollp;
    DIRCONTENTS    * currdir;
    IMAGE          * currimg;

    NAV * collnav; // Dirs-View
    NAV * dirnav; // index-nav (Index-View / Image-View)

    //SDL_mutex * coll_mutex;
    MUTEX_WITH_ATTR dir_mutex;
};


EXTRACTED prepare_extraction( DIR_COLLECTION * dcollp );

void extract_currdir(EXTRACTED * ext);
void unlock_currdir( EXTRACTED * ext );


typedef struct _thread_data_ THREAD_DATA;
struct _thread_data_
{
    APP * app;
    DIR_COLLECTION * dcollp;
    INDEX_LAYOUT * layout;
    READ_KEY       input;
    USTATE  st;
    EXTRACTED * extracted; // only pointer, because this is thread-local
    bool needs_rendering;
};

#endif
