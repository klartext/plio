/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __STRINGHELPERS__
#define __STRINGHELPERS__

#include <stdbool.h>
#include <limits.h>

// ============================================================================
typedef struct pathfiddling PATHFIDDLE;

struct pathfiddling
{
    char orig[PATH_MAX + 1]; // orig path, maybe just rel. path and with symlinks

    char tmp[PATH_MAX + 1]; // some opoerations might change the buffer (prserve orig)

    char real[PATH_MAX + 1];     // after realpath has been used
    char dirname[PATH_MAX + 1];  // after dirname has been applied
    char basename[PATH_MAX + 1]; // after basename has been used
};

PATHFIDDLE calculate_paths( const char * origpath );

int last_dot_position( char * string );

bool extension_is_equal( char* string, char* extension );

// ============================================================================
/*
    STRING_VEC
        is intended to provide a consistent interface for handling a bunch of
        strings.

        vec = [char*][char*][char*][...][char*]

*/
typedef struct _string_vec_ STRING_VEC;
struct _string_vec_
{
    char ** vec;

    int num;  // allocated length is for num items
    int used; // truly used number of items used (used <=num)
};

void calloc_strvec( STRING_VEC * stv, int num );
void print_stringvec( STRING_VEC * stv, char * sep );
void free_stringvec( STRING_VEC * stv );

int compstr( const void * v1p, const void * v2p );
void stringvec_sort( STRING_VEC * strvec );
STRING_VEC stringvec_sort_uniq( STRING_VEC * incoming );
STRING_VEC stringvec_of_string( char * string );
STRING_VEC stringvec_concat( STRING_VEC * stv1, STRING_VEC * stv2 );

bool stringvec_append( STRING_VEC * append_here, STRING_VEC * append_this );
void stringvec_apply( STRING_VEC * stv, void (*stringfunc)(char * str) );
STRING_VEC stringvec_filter( STRING_VEC * stv, bool (*stringcheck)(char * str) );

// ============================================================================

typedef struct _x_out_of_y_ SELECTED_ITEMS;
struct _x_out_of_y_
{ // <curr> <actual_items> <wrap_start> <max> <src_items> <wrap_end>
  // example:   1 Image  (out of 100 Files)
  //           curr: 1, actual_sing: "Image ", wrap_start: "(out of ", max: 100, src_plur = "Files", wrap_end = ")"
  //
  // example:  12 Images (out of 100 Files)
  //           curr: 12, actual_plur: "Images", wrap_start: "(out of ", max: 100, src_plur = "Files", wrap_end = ")"

    int curr;
    int max;
    char * curr_fmt;
    char * max_fmt;

    char * actual_sing;
    char * actual_plur;

    char * src_sing;
    char * src_plur;

    char * wrap_start;
    char * wrap_end;
};

void string_of_selected_items( char * textbuf, SELECTED_ITEMS * sel );

void prepend_string_to_basename( const char * abspath, const char * prepend_string, char * buf );
void prepend_string_to_extension_of_basename( const char * abspath, const char * replacement_string, char * buf );

// ============================================================================

#endif
