/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

#include "renamefiles.h"
#include "stringhelpers.h"


/*
    Renaming files - by prepending strings to eaach basename.

    The idea behind the renaming scheme is,
    that renaming files might overwrite already existing files.

    When renaming files by prepending strings t the basenames, overwriging of files can occur,
    when a file has a name, which is the same as a renamed other file would have.

    Example:
        The '->' indicates the mapping from old name to new name:

        No problem here:
            A.jpg -> 00000_A.jpg
            B.jpg -> 00001_B.jpg

            Even when prepending the same string (00000 to both filenmes would not b eproblematic).

        Potential problem here:

            A.jpg       -> 00000_A.jpg
            00000_A.jpg -> 00000_00000_A.jpg

            When the renaming occurs in the order as shown above,
            this would overwrite file 00000_A.jpg with file A.jpg,
            which then gets the name 00000_A.jpg.
            Afterwards this file is renamed to 00000_00000_A.jpg.

        All in all, this would remove file 00000_A.jpg and rename file A.jpg to 00000_00000_A.jpg.

        If the renaming would be done in opposit e order, no problem would occur:

            00000_A.jpg -> 00000_00000_A.jpg
            A.jpg       -> 00000_A.jpg


        So, when renaming files by prepending strings to the basenames,
        it's necessary to start with the files with longest names,
        then in ascending order of filename-length proceed.

        Even prepending the same string to all files would then be safe.

        Renaming files by doing the renames in this order, is what the code in
        this file is intended to do.

*/


static NAME_MAPPING * create_mapvec( int num_files )
{
    NAME_MAPPING * mapvec;
    assert(num_files > 0 );

    mapvec = calloc(num_files, sizeof(NAME_MAPPING));
    assert(mapvec != NULL);

    return mapvec;
}



NAME_MAP make_name_map( int num_files )
{
    NAME_MAP map;

    map.mapvec = create_mapvec(num_files);
    map.len = num_files;

    return map;
}



/** \brief Deallocate/free the mapping vector of the NAME_MAP. Name-strings will also be deallocated here.
*/
void cleanup_name_map( NAME_MAP * mapaddr )
{
    assert(mapaddr != NULL);

    int idx = 0;

    if( mapaddr->mapvec && mapaddr->len > 0 )
    {
        // deallocate the mapping-strings
        // ------------------------------
        for( idx = 0; idx < mapaddr->len; idx++ )
        {
            free(mapaddr->mapvec[idx].origname);
            free(mapaddr->mapvec[idx].newname);
        }

        // deallocate the mapping-vector
        // -----------------------------
        free(mapaddr->mapvec);
        mapaddr->mapvec = NULL;
    }

    mapaddr->len = 0;

    return;
}



// -----------------------------------------------------------------------------

/** \brief Insert the name-strings into the map. Allocation of memory for the strings is done via strdup(3).
*/
void insert_name_mapping_at_index( NAME_MAP * map, char * orig, char * new, int idx )
{
    assert(map != NULL);
    assert(idx >= 0 );
    assert(idx < map->len);

    map->mapvec[idx].origname = strdup(orig);
    map->mapvec[idx].newname  = strdup(new);

    return;
}



/** \brief Read out origname and mapping name from map at index idx.

For orig_ptr as well as new_ptr it is possible to be NULL.
In this case, no data is read from the map.

For a non-NULL pointer, the pointers are set to the strings of the map.
*/
void read_name_mapping_at_index( NAME_MAP * map, char ** orig_ptr, char ** new_ptr, int idx )
{
    assert(map != NULL);
    assert(idx >= 0 );
    assert(idx < map->len);

    if( orig_ptr != NULL )
    {
        *orig_ptr = map->mapvec[idx].origname;
    }

    if( new_ptr != NULL )
    {
        *new_ptr = map->mapvec[idx].newname;
    }

    return;
}


// -----------------------------------------------------------------------------


static int compare_orignames_length( const void * vnm_1, const void * vnm_2 )
{
    int cmpres = 0;
    const NAME_MAPPING * nm_1 = vnm_1;
    const NAME_MAPPING * nm_2 = vnm_2;

    int len_1 = strlen(nm_1->origname);
    int len_2 = strlen(nm_2->origname);

    if( len_1 > len_2 )
    {
        cmpres = -1;
    }
    else if( len_1 < len_2 )
    {
        cmpres = 1;
    }
    else
    {
        cmpres = 0;
    }

    return cmpres;
}



void show_name_mapping( NAME_MAP * map )
{
    assert(map != NULL);

    char * oldname;
    char * newname;


    for( int idx = 0; idx < map->len; idx++ )
    {
        oldname = map->mapvec[idx].origname;
        newname = map->mapvec[idx].newname;

        if( oldname != NULL )
        {
            printf("\"%s\" mapped to ", oldname);
        }
        else
        {
            fprintf(stderr, "no origname has been set (%1d)\n", idx);
            continue;
        }

        if( newname != NULL )
        {
            printf("\"%s\"", newname);
        }
        else
        {
            printf(" ./. (no new name available)");
        }

        putchar('\n');
    }

    return;
}



void sort_map_by_namelength( NAME_MAP * map )
{
    qsort(map->mapvec, (size_t) map->len, sizeof(NAME_MAPPING), compare_orignames_length);
    return;
}


/*\brief Returns true, iff all new names fit into PATH_MAX and NAME_MAX limits.
*/
bool new_names_fit_limits( NAME_MAP * map )
{
    assert(map != NULL);
    bool fit = true;
    PATHFIDDLE path;

    for( int idx = 0; idx < map->len; idx++ )
    {
        fit &= strlen(map->mapvec[idx].newname) < PATH_MAX;

        path = calculate_paths(map->mapvec[idx].newname);
        fit &= strlen(path.basename) < NAME_MAX;
    }

    return fit;
}



void apply_renaming( NAME_MAP * map )
{
    assert(map != NULL);
    int res = 0;

    sort_map_by_namelength(map); // reorder map to ensure safe renaming

    for( int idx = 0; idx < map->len; idx++ )
    {
        char * old = map->mapvec[idx].origname;
        char * new = map->mapvec[idx].newname;

        res = rename(old, new);
        if( res != 0 )
        {
            fprintf(stderr, "could not rename %s to %s\n", old, new);
        }
    }

    return;
}
