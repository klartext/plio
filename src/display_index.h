/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __DISPLAY_INDEX__
#define __DISPLAY_INDEX__

#include "common.h"
#include "dircontents.h"
#include "stringhelpers.h"

#include "indexlayout.h"

void setup_initial_indexview( APP * app, DIRCONTENTS * dcont, INDEX_LAYOUT * layout );
void render_thumbindex_from_range( APP * app, INDEX_LAYOUT * layout, NAV * navp, SELECTED_ITEMS * title, SELECTED_ITEMS * footer );

SELECTED_ITEMS make_indexview_title(void);
SELECTED_ITEMS make_indexview_footer(void);

#endif
