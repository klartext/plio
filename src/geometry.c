/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <string.h>
#include <assert.h>

#include "geometry.h"



/** \brief For scaling down an image to a thumb with correct aspect ratio, create SDL_rect with width/height set.

    - The returned SDL_Rect has 'w' and 'h' set, so that the whole image fits into the thumb-size.
    - The position ('x' and 'y') are both 0.

    Use this function, if you want to scale down an image to the size of the thumb,
    while preserving the aspect ratio.

    Positioning then can be done by setting (x,y) afterwards.
*/
#ifdef TEST_STATIC_FUNCTIONS
SDL_Rect aspectratio_correction_from_intpair( int image_width, int image_height, int thumb_width, int thumb_height )
#else
static SDL_Rect aspectratio_correction_from_intpair( int image_width, int image_height, int thumb_width, int thumb_height )
#endif
{
    SDL_Rect corrected;

    bzero(&corrected, sizeof(corrected));

    double width_div  = (double) image_width / thumb_width;
    double height_div = (double) image_height / thumb_height;

    double scale = width_div > height_div ? width_div : height_div;

    double res_h = image_height / scale;
    double res_w = image_width / scale;

    corrected.w = res_w;
    corrected.h = res_h;

    return corrected;
}



/** \brief Return SDL_Rect with same position as 'orig', but downscaled to (image_width x image_width) and  aspect-ratio unchanged.

    The result has same position as the incoming 'orig' rectangle.
    The width and height from the 'orig' rectangle scaled down to fir into image_width x image_width
    is then the dimension that the return rectangle has.
*/
/* Can this fucntion be removed?
static SDL_Rect aspectratio_correction( int image_width, int image_height, SDL_Rect * orig )
{
    SDL_Rect corrected;

    bzero(&corrected, sizeof(corrected));

    double width_div  = (double) image_width / orig->w;
    double height_div = (double) image_height / orig->h;

    double scale = width_div > height_div ? width_div : height_div;

    double res_h = image_height / scale;
    double res_w = image_width / scale;

    corrected.x = orig->x;
    corrected.y = orig->y;
    corrected.w = res_w;
    corrected.h = res_h;

    return corrected;
}
*/



/*
Beispiel:
   0 1 2
   0 1 2

  Idx -> xpos
    0 -> 0
    1 -> 1
    2 -> 2

    3 -> 0
    4 -> 1
    5 -> 2

    [...]


  Idx -> ypos
    0 -> 0
    1 -> 0
    2 -> 0

    3 -> 1
    4 -> 1
    5 -> 1

    6 -> 2
    [...]
*/
/*
*/
/** \brief Return int-division and rest via pointers 'x_pos' and 'y_pos'.

    For a given index and width, calculate x-pos and ypos and return the
    result via the pointers x_pos and y_pos.
    If one of the result components is not needed, NULL can be passed,
    to indicate that the result can be ignored.

    - can add a table here later

    For 'x_pos' and 'y_pos' individually it is checked,
    if they are NULL. If so, then no value is returned.
    So if only one of the results is needed, just set the other value to NULL;
*/
void index_to_indexpair( int index, int width, int * x_pos, int * y_pos )
{
    assert(index >=0);
    assert(width >= 1);

    if( x_pos )
    {
        *x_pos = index % width;
    }

    if( y_pos )
    {
        *y_pos = index / width;
    }

    return;
}



/* ================================================ */
/* More general functions for position calculation. */
/* ================================================ */

/** \brief Calculate number of boxes and padding space.

    Given available_space and box_width,
    calculate num (of items/boxes) that fit into available_space
    and the left over padding space between these num items.
    Results num and padding will be wriiten into the variables,
    whose adresses are given.
    minsep: minimum distance between boxes.
*/
void calc_itemnum_and_padding( int window_size, int outer_frame, int box_width, int minsep, int * num, int * padding )
{
    *num     = (window_size - 2 * outer_frame) / (box_width + 2 * minsep);
    *padding = (window_size - 2 * outer_frame) % (box_width + 2 * minsep);

    return;
}



/** \brief For given number of items, box_width, available padding space and box-index, calculate the boxpos.
*/
int calc_box_position( int num, int box_width, int minsep, int padding_space, int idx )
{
    double padding_width = (double) padding_space / (num - 1); // check again (on needed casts)

    double raw_boxpos = minsep + idx * (box_width + 2 * minsep + padding_width);

    return (int) floor(raw_boxpos);
}



/** \brief From index-window dimensions, ypos and old scroll calculate the new scroll value.
*/
int calc_new_scroll( INDEX_DIM * dim, int ypos, int old_scroll )
{
    RANGE scroll_range = range_from_intpair(0, dim->num_scroll_max);
    RANGE view = range_from_intpair(0, dim->rows_per_screen - 1);
    RANGE shifted_view = shift_range(&view, 1 - dim->rows_per_screen);
    RANGE applied_view = shift_range(&shifted_view, ypos);
    RANGE bounded_view = clip_range(&applied_view, &scroll_range);

    return bounded_int(old_scroll, &bounded_view);
}



/** \brief Recalculate dimensions for the index-view. Mutex of INDEX_LAYOUT locked for safe access to the data.

    Before calling this function, make sure that SDL_GetWindowSize() has been
    called to set 'win_w' and 'win_h' of bare_layout.

    @param [in]  win_w
    @param [in]  win_h
    @param [in,out]  navp
    @param [in]  bare_layout

OLD: navp contains images_grid, scrollp, imgview_p, thumbview_dim

    @param [in,out]  images_grid
    @param [out] scrollp
    @param [out]  imgview_p
    @param [in]  bare_layout
    @param [out] thumbview_dim
*/
/*
        WICHTIG: -> Mindestens ein Bild muss bereits registriert worden sein (used_length >= 1)
*/
void calc_grid_navigation( int win_w, int win_h, NAV * navp, INDEX_LAYOUT * bare_layout )
{
    assert(navp);
    assert(bare_layout);

    GRID      * images_grid   = navp->grid;
    RANGE     * imgview_p     = &navp->view;
    BCOUNT    * scrollp       = &navp->scroll;
    INDEX_DIM * thumbview_dim = &navp->view_dim;

    int old_scroll = (scrollp->get != NULL) ? (*scrollp->get)(scrollp) : 0;

    lock_grid(images_grid); // GRID-Mutex LOCKED

    int ypos      = -1; // ypos will be written to by index_to_indexpair
    int newscroll =  0; // newscroll will be calculated from ypos and the dumensions of the thumbview (indexview)


    // calculate logical dimensions if the Thumbview/IndexView
    // -------------------------------------------------------
    *thumbview_dim = initialize_indexview_layout(win_w, win_h, bare_layout, images_grid->used_length);

    grid_set_xlen_raw(images_grid, thumbview_dim->width);      // set new row length of grid
    *scrollp = make_counter(0, thumbview_dim->num_scroll_max); // max. index of scroll counter set for updated thumbview_dim

    // calculate new scroll value, so that active image can be shown
    // -------------------------------------------------------------
    index_to_indexpair(images_grid->index, images_grid->xlen, NULL, &ypos);
    newscroll = calc_new_scroll(thumbview_dim, ypos, old_scroll);

    // set new scroll position
    // -----------------------
    (*scrollp->set)(scrollp, newscroll);

    // Set view (index-range), such that the active image is shown
    // --------------------------------------------------------------
    *imgview_p = calc_imgview_range(thumbview_dim, images_grid->used_length, (*scrollp->get)(scrollp));

    unlock_grid(images_grid); // GRID-Mutex UNLOCKED

    return;
}



/** \brief Calculate range of image indexes (a view) to be displayed for a given layout and scroll.

    - The effective scroll value will be limited to it's maximum value of the layout.
    - the resulting index values are limited from 0 to num_images - 1
*/
RANGE calc_imgview_range( INDEX_DIM * layout, int num_images, int scroll )
{
    int first  = 0; // index to images-array/grid
    int last   = 0; // index to images-array/grid

    RANGE res;
    bzero(&res, sizeof(res));

    RANGE imgidx_range     = range_from_intpair(0, num_images -  1);
    RANGE scroll_range     = range_from_intpair(0, layout->num_scroll_max);

    scroll = bounded_int(scroll, &scroll_range);

    first = scroll * layout->width;
    last  = first + layout->width * layout->height - 1;

    first = bounded_int(first, &imgidx_range);
    last  = bounded_int(last, &imgidx_range);

    res.min = first;
    res.max = last;

    return res;
}



/** \brief Calculate position and dimension of the thumbnail with index 'idx'.

    - If img is given, position and dimensions for the thumb rectangle will be calculated.
    - It img is NULL, only the position is calculated and dimensions are not set (is then (0,0).

    The position of the thumb at position 'idx' is calculated always.

    The dimension of the thumb is dependent on the dimension of the image - if 'img' is not NULL.

*/
SDL_Rect get_index_position( INDEX_LAYOUT * thumbview, IMAGE * img, int idx, int win_w, int win_h )
{
    int xidx = 0;
    int yidx = 0;

    int grid_width  = 0;
    int grid_height = 0;
    int x_padding     = 0;
    int y_padding     = 0;

    SDL_Rect result;
    bzero(&result, sizeof(result));

    calc_itemnum_and_padding(win_w, thumbview->outer_frame, thumbview->thumb_sidelength, thumbview->minsep, &grid_width,  &x_padding);
    calc_itemnum_and_padding(win_h, thumbview->outer_frame, thumbview->thumb_sidelength, thumbview->minsep, &grid_height, &y_padding);

    index_to_indexpair(idx, grid_width, &xidx, &yidx);

    if( img != NULL )
    {
        result = aspectratio_correction_from_intpair(img->width, img->height, thumbview->thumb_sidelength, thumbview->thumb_sidelength); // adapting width and height
    }

    /* calculating / setting the position */
    /* ---------------------------------- */
    result.x = calc_box_position(grid_width, thumbview->thumb_sidelength, thumbview->minsep, x_padding, xidx);
    result.y = calc_box_position(grid_height, thumbview->thumb_sidelength, thumbview->minsep, y_padding, yidx);

    result.x += thumbview->outer_frame;
    result.y += thumbview->outer_frame;

    return result;
}



SDL_FRect aspect_corr_with_rot_corr( int image_width, int image_height, int target_width, int target_height, ROTATION rot )
{
    SDL_Rect  dst;
    SDL_FRect fdst;

    switch(rot)
    {
        case ROT_0: // nothing to do here //
                dst = aspectratio_correction_from_intpair(image_width, image_height, target_width, target_height); // adapting width and height
            break;

        case ROT_90:
                dst = aspectratio_correction_from_intpair(image_width, image_height, target_height, target_width); // adapting width and height
            break;

        case ROT_180:
                dst = aspectratio_correction_from_intpair(image_width, image_height, target_width, target_height); // adapting width and height
            break;

        case ROT_270:
                dst = aspectratio_correction_from_intpair(image_width, image_height, target_height, target_width); // adapting width and height
            break;

        default: // undefined enum value
            abort();
    }

    fdst.x = dst.x;
    fdst.y = dst.y;
    fdst.w = dst.w;
    fdst.h = dst.h;

    return fdst;
}



SDL_FRect rot_offset_correction( SDL_FRect* frec, ROTATION rot )
{
    SDL_FRect out;

        out.x = frec->x;
        out.y = frec->y;
        out.w = frec->w;
        out.h = frec->h;

    switch(rot)
    {
        case ROT_0: // nothing to do here //
                ;
            break;

        case ROT_90:
                out.y += frec->w;
            break;

        case ROT_180:
                out.x += frec->w;
                out.y += frec->h;
            break;

        case ROT_270:
                out.x += frec->h;
            break;

        default: // undefined enum value
            abort();
    }

    return out;
}
