/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __MARKS__
#define __MARKS__


#include <stdbool.h>


typedef struct marks MARKS;
struct marks
{
    bool general;
    bool delete;
};


bool is_marked( MARKS * mrk );
void clear_all_marks( MARKS * mrk );

bool is_marked_general( MARKS * mrk );
void set_mark_general( MARKS * mrk );
void clear_mark_general( MARKS * mrk );

bool is_marked_delete( MARKS * mrk );
void set_mark_delete( MARKS * mrk );
void clear_mark_delete( MARKS * mrk );

#endif
