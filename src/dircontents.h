/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __DIRCONTENTS__
#define __DIRCONTENTS__

#include <limits.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "grid.h"
#include "image.h"
#include "range.h"
#include "counter.h"
#include "indexlayout.h"
#include "common.h"
#include "nav.h"


union _name_or_imagedata_raw_
{
    char * filename; // should be abspath here already
    IMAGE * img;
};
// --------------------------------------------------------------------

typedef struct _name_or_imagedata_ NOID;
struct _name_or_imagedata_
{
    union _name_or_imagedata_raw_ noir;
    bool  is_imagedata;
};

// --------------------------------------------------------------------

typedef struct _dircontents DIRCONTENTS;
struct _dircontents
{
    char dirname[PATH_MAX];

    bool valid; // true, iff valid filenames found - used to deselect a Dir in DirColl-setup

    NOID * noi_vec;
    int    noi_num;

    NAV nav;

    IMAGE ** rpl; // representational payload (needed, as long as image_datnav has not been set up

    int num_of_marked;  // number of marked images of this Dir


    // Stuff needed in setup of DIRCONTENTS struct
    int ridx;
    int widx;
    bool completed;

    bool on_free_print_mogrify; // if to print modification commands for mogrify

    struct timespec modtime; // time of last modification of the dir

    MUTEX_WITH_ATTR mut;
};


DIRCONTENTS make_dircontents( char * dirname );
void set_mogrify_info( DIRCONTENTS * dircontp );
bool read_next_image_into_dircontents( DIRCONTENTS * dcont, int thumb_surface_size, bool * updated );
int read_images_of_dir_thread( void * data );
void free_dircontents( DIRCONTENTS * dcont );

void show_dircontents_info( DIRCONTENTS * dircontp );

int dircontents_num_of_entries( const DIRCONTENTS * dc );
int dircontents_compare_dirname( const DIRCONTENTS * dc1, const DIRCONTENTS * dc2 );
int dircontents_compare_num_entries( const void * dc1_v, const void * dc2_v );  // Interface for GRID-sort
int comp_dircontents_by_dirname( const void * dc_v_1, const void * dc_v_2 );  // Interface for GRID-sort
bool dircontents_did_change( const DIRCONTENTS * dc );
void dircontents_update_modtime( DIRCONTENTS * dc );

#endif
