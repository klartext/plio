/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include "statemachine.h"
#include "ustate.h"


void print_stm_input( STM_IN * stm )
{
    printf("| fsel: %2d | cmd: %2d |\n", stm->fsel, stm->cmd);
}



bool stm_equal( STM_IN * first, STM_IN * second )
{
    bool result = true;

    result = result && (first->fsel == second->fsel);
    result = result && (first->cmd == second->cmd);

    return result;
}



bool stm_matches( STM_IN * stm, int funsel, int sub_state )
{
    bool result = true;

    result = result && (stm->fsel == funsel );
    result = result && (stm->cmd == sub_state);

    return result;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

/** \brief Check istate to match stact-instate, and if it matches, apply function and afterwards set the output state.
*/
#ifdef TEST_STATIC_FUNCTIONS
bool on_match_apply_action_set( STM_IN * istate, STATE_ACTION * stact, STM_OUT * ostate, void * vdata ) // copy this as prototype to testing.c
#else
static bool on_match_apply_action_set( STM_IN * istate, STATE_ACTION * stact, STM_OUT * ostate, void * vdata )
#endif
{
    bool instate_matches_action = stm_equal(istate, &stact->in);
    bool did_match = false; // retval

    if( instate_matches_action ) // might be more tests here
    {
        did_match = true;

        if( stact->fun != NULL )
        {
            stact->fun(vdata);
        }

        ostate->fsel =  stact->out.fsel; // set new state

        if( ostate->fsel == KEEP_INSTATE ) // old state is kept as new state
        {
            ostate->fsel = istate->fsel;
        }
    }

    return did_match;
}

bool on_statematch_from_statevec_apply_func( STM_IN * istate, STATE_ACTION * states_vector, int statesvec_len, STM_OUT * ostate, void * vdata )
{
    bool matched = false;
    int idx = 0;

    for( idx = 0; idx < statesvec_len; idx++ )
    {
        if( on_match_apply_action_set(istate, &states_vector[idx], ostate, vdata) )
        {
            matched = true;
            break;
        }
    }

    return matched;
}
