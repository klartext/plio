/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>

#include <SDL2/SDL.h>

#include "image.h"
#include "macros.h"
#include "tools.h"


int random_int( int maxval )
{
    int result = 0;
    double pre = 0.0;

    pre = (double) (maxval + 1) / RAND_MAX;
    result = rand() * pre;

    return (int) result;
}


void wait_with_message(unsigned int wait_time)
{
    printf("-> Waiting with SDL_Delay(%u)\n", wait_time);
    SDL_Delay(wait_time);
    printf("... waiting time is over, going on now.\n\n");
    return;
}


// =============================================================================================================


// function maybe not needed anmyore
bool replace_lastimage_if_necessary( SDL_Renderer * renderer, IMAGE * lastimg, IMAGE * currimg )
{
    bool necessary = false;

    if( currimg != lastimg ) // img is other than last time?
    {
        necessary = true;
        if( lastimg && lastimg->texture ) // free last images texture and set to 'unused' (NULL)
        {
            SDL_DestroyTexture(lastimg->texture);
            lastimg->texture = NULL;
        }

        currimg->texture = load_image_as_texture(renderer, currimg->abspath); // allocate current texture
    }

    return necessary;
}



void windowdump_png(SDL_Renderer * renderer, char * outname)
{
    const uint32_t format = SDL_PIXELFORMAT_RGBA8888;
    SDL_Surface * output_surface = NULL;
    int win_w = 0;
    int win_h = 0;
    int res = 0;

    res = SDL_GetRendererOutputSize(renderer, &win_w, &win_h);
    if( res != 0 )
    {
        fprintf(stderr, "%s(): window dump failed: %s\n", __FUNCTION__, SDL_GetError());
        return;
    }

    output_surface = SDL_CreateRGBSurfaceWithFormat(0, win_w, win_h, 32, format);
    if( output_surface != NULL )
    {
        res = SDL_RenderReadPixels(renderer, NULL, format, output_surface->pixels, output_surface->pitch);
        sdl_errmsg_on_error(res);

        if( res ==  0 )
        {
            res = IMG_SavePNG(output_surface, outname);
            if( res )
            {
                fprintf(stderr, "Error: IMG_SavePNG() failed!\n");
            }
        }

        SDL_FreeSurface(output_surface);
    }
    else
    {
        fprintf(stderr, "%s(): window dump failed: %s\n", __FUNCTION__, SDL_GetError());
        return;
    }

    return;
}


void print_bits(int value)
{
    int idx = 0;
    int num_bits = sizeof(value) * CHAR_BIT;
    char c;

    for( idx = num_bits - 1; idx >= 0; idx-- )
    {
        c = (value & (1 << idx)) ? '1' : '0';
        putchar(c);
    }
    return;
}

void print_bits_nl(int value)
{
    print_bits(value);
    putchar('\n');
}


void print_bits_message(char * prepend, int value, char * append)
{
    printf("%s", prepend);
    print_bits(value);
    printf("%s", append);
}



/*
bool rendering_needed( DIR_COLLECTION * dcollp )
{
    bool decision_time = (cmp_timespec(&dcollp->t_last_rendering, &dcollp->t_render_request) == - 1) ? true : false;
    bool decision_winsize = dcollp->winsize_changed ? true : false;
    bool decision = decision_time || decision_winsize;
    return decision;
}
*/

/*
SDL_Rect color_2_position( LABF * col )
{
    SDL_Rect pos;

    show_labf(col); // TODO: remove later
    LABI icentr = labi_of_labf(col);
    show_labi(&icentr); // TODO: remove later
    LABI centr_pos = icentr;
    centr_pos.l += 128;
    centr_pos.a += 128;
    centr_pos.b += 128;
    show_labi(&centr_pos); // TODO: remove later
    putchar('\n');

    return pos;
}
*/


void show_surface_infos( SDL_Surface * surf )
{
    printf("BitsPerPixel  (Thumb-Surface): %2d\n", surf->format->BitsPerPixel);
    printf("BytesPerPixel (Thumb-Surface): %2d\n", surf->format->BytesPerPixel);

    return;
}



void print_rect( SDL_Rect * rect )
{
    printf("x: %4d\n", rect->x);
    printf("y: %4d\n", rect->y);
    printf("w: %4d\n", rect->w);
    printf("h: %4d\n", rect->h);
}


void switch_ints( int * first, int * second )
{
    int tmp = *first;
    *second = *first;
    *first = tmp;

    return;
}


static void print_displayinfo_of_display( int display_idx, SDL_DisplayMode mode )
{
    SDL_Rect rect;
    int res = 0;

    printf("Display #%d: current display mode is %dx%dpx @ %dHz.", display_idx, mode.w, mode.h, mode.refresh_rate);
    res = SDL_GetDisplayBounds(display_idx, &rect);
    if( res >= 0 )
    {
        printf("    -> (x,y) = (%1d, %1d), (w,h) = (%1d, %1d)\n", rect.x, rect.y, rect.w, rect.h);
    }
    else
    {
        fprintf(stderr, "%s\n", SDL_GetError());
    }

    return;
}


static void print_dpiinfo_of_display( int display_idx )
{
    float ddpi;
    float hdpi;
    float vdpi;

    int res = 0;

    res = SDL_GetDisplayDPI(display_idx, &ddpi, &hdpi, &vdpi);
    if( res == 0 )
    {
        printf("    -> ddpi = %f, hdpi = %f, vdpi = %f\n", ddpi, hdpi, vdpi);
    }
    else
    {
        fprintf(stderr, "%s\n", SDL_GetError());
    }
}


void show_display_info( APP * app )
{
    SDL_DisplayMode mode;
    int res = 0;
    const char * marker = NULL;
    int num_displays = 0;
    int display_index_of_window = 0;


    num_displays = SDL_GetNumVideoDisplays();
    if( num_displays < 0 )
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return;
    }

    display_index_of_window = SDL_GetWindowDisplayIndex(app->window);
    if( display_index_of_window < 0 )
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return;
    }

    for(int display_idx = 0; display_idx < num_displays; ++display_idx)
    {

        res = SDL_GetCurrentDisplayMode(display_idx, &mode);

        if(res != 0)
        {
          fprintf(stderr, "Could not get display mode for video display #%d: %s", display_idx, SDL_GetError());
        }
        else
        {
            marker = (display_idx == display_index_of_window) ? " * " : "   ";
            printf("%s", marker);

            print_displayinfo_of_display(display_idx, mode);
            print_dpiinfo_of_display(display_idx);
            putchar('\n');
        }

        // DPI


    }
    return;
}
/*
res = SDL_SetWindowFullscreen(app->window, SDL_WINDOW_FULLSCREEN); // set fullscreen
if( res ) fprintf(stderr, "ERROR regarding Fullscreen\n");

SDL_Delay(2000); // wait a  while
res = SDL_SetWindowDisplayMode(app->window, &mode); // back to former mode
if( res ) fprintf(stderr, "ERROR regarding old mode\n");

// https://wiki.libsdl.org/SDL2/SDL_GetWindowDisplayMode
*/
