/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __USTATE__
#define __USTATE__

#include <stdbool.h>

#include <SDL2/SDL.h>

typedef enum _switch_loops { DIR_VIEW, INDEX_VIEW, IMAGE_VIEW, XY_GRID, XY_IMAGEVIEW, KEEP_INSTATE, UNSELECTED } FUN_SEL;

typedef struct _user_command_state_ USTATE;
struct _user_command_state_
{
    enum _switch_loops fsel; // functionality selection (DIR_VIEW, INDEX_VIEW, etc.) - same as 'funselect' int used so far
    int cmd;  // CMD_ commands (might be changed to'local' commands; so far they are 'global' (for all Control-Loops))

    bool cmd_avail; // true iff key has been translated to cmd
    bool cmd_done;  // true, if command has been processed
    bool cmd_failed;    // true if comamnd processing failed

    bool view_changed; // if switching fsel (view) or moving anything in the navigation
};

#endif
