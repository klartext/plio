/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <assert.h>

#include <SDL2/SDL.h>

#include "display.h"


/** \brief Render the Imageview.
*/
void display_imageview( APP * app, int win_w, int win_h, GRID * image_grid )
{
    IMAGE * img = NULL;

    img = grid_get_payload_by_index(image_grid, image_grid->index);

    clear_screen(app, false);
    display_image_trimmed(app->renderer, img, win_w, win_h);

    SDL_RenderPresent(app->renderer);

    return;
}
