/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include "environment.h"
#include <stdlib.h>
#include <assert.h>


/** \brief Wrapper for getenv(3) / secure_getenv(3).

Currently it's just a wrapper for getenv(3).
This is fundtion is used for maybe replacing the getenv(3) with
secure_getenv(3) later - possibly after cheking availability with Test-Macros.
See man(3) for getenv().
*/
static char * get_environment( const char * name)
{
    return getenv(name); // maybe replace it (conditionally) with secure_getenv() (GNU)
}


/** \brief Lookup environment variable, return int value if available (and not zero) - otherwise return default_int.

The default value is not allowed to be 0.
The function `atoi(3)`, which is called, returns 0 on error.
So an error case can't be distinguished from a valid zero value.
Hence you need to insist on non-zero values in the environment values.
*/
int use_env_int_nonzero( char * env_varname, int default_int )
{
    int val = default_int;
    char * envval = NULL;

    assert(default_int != 0); // zero not allowed, so default value of zero means bug


    envval = get_environment(env_varname);

    if( envval != NULL )
    {
        int envint = atoi(envval);

        val = (envint != 0) ? envint : default_int;
    }

    return val;
}



/** \brief Lookup environment variable, return double value if available (and not zero) - otherwise return default_dbl.

The default value is not allowed to be 0.
The function `atof(3)`, which is called, returns 0 on error.
So an error case can't be distinguished from a valid zero value.
Hence you need to insist on non-zero values in the environment values.
*/
double use_env_double_nonzero( char * env_varname, double default_dbl )
{
    double val = default_dbl;
    char * envval = NULL;

    assert(default_dbl != 0.0); // zero not allowed, so default value of zero means bug


    envval = get_environment(env_varname);

    if( envval != NULL )
    {
        double envdbl = atof(envval);

        val = (envdbl != 0) ? envdbl : default_dbl;
    }

    return val;
}



/** \brief Lookup environment variable, return it, if available, otherwise return the default string.

No allocations are made here, just selection of pointers.
*/
char * use_env_string( char * env_varname, char * default_string )
{
    char * envval = get_environment(env_varname);

    if( envval != NULL )
    {
        return envval;
    }
    else
    {
        return default_string;
    }
}
