/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <SDL2/SDL.h>

#include "events.h"
#include "keysenums.h"


/*
bool is_window_resize_event(SDL_Event * event)
{
    bool res = false;

    if( ! (event->type == SDL_WINDOWEVENT) )
    {
        fprintf(stderr, "warning: not a windo-event (%s())\n", __FUNCTION__);
        return res;
    }

    if( event->window.event == SDL_WINDOWEVENT_RESIZED      ||
        event->window.event == SDL_WINDOWEVENT_SIZE_CHANGED ||
        event->window.event == SDL_WINDOWEVENT_MINIMIZED    ||
        event->window.event == SDL_WINDOWEVENT_MAXIMIZED
      )
    {
    printf(" -------------->  WINSIZE !!\n");
        res = true;
    }

//res = true;
    return res;
}
*/



/*
    get mapping of keys

    Example usage:

    struct keymapping kmap[] = { { 'q',0,  QUIT },
                                 { 's', KMOD_LSHIFT, 'S' },
                                 { 13, 0, ENTER },
                               };

    if( get_kmapping(kmap, sizeof(kmap) / sizeof(struct keymapping), kc, current_modkey, &outval) )
    {
        use_the_outval(outval);
    }
*/
bool get_kmapping(struct keymapping * kmap, int len, int input, SDL_Keymod km, int * output)
{
    bool found_mapping = false;
    int idx = 0;

    int checked_modifiers = KMOD_CTRL | KMOD_LSHIFT | KMOD_RSHIFT; // only those modifiers, that are used
    int nomods            = (km & checked_modifiers) == KMOD_NONE;

    for(idx = 0; idx < len; idx++)
    {
        if( input == kmap[idx].in )
        {
            if( ((kmap[idx].mod == KMOD_NONE) && nomods) || (km & kmap[idx].mod) )
            {
                *output = kmap[idx].out;
                found_mapping = true;

                break;
            }
        }
    }

    return found_mapping;
}



/*
    -------------------------------
*/
enum keys get_keys( bool * winsize_changed, int delay_ms )
{
    SDL_Event   event_str;
    SDL_Event * event = &event_str;
    SDL_Keymod km;

    extern struct keymapping kmap[]; // from keymappings.c
    extern int kmsize;               // from keymappings.c

    int outval = 0;


    *winsize_changed = false; // will be overwritten only, if such event did occur

    int kc = 0; // Key-Code
    //printf("kc = %d\n", kc);

    while(SDL_PollEvent(event))
    {
        if( event->key.repeat )
        {
            //printf("   ----> event->key.repeat\n");
            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
            SDL_Delay(delay_ms);
        }

        switch( event->type )
        {
            case SDL_QUIT:
                return QUIT_SDL_QUIT; // maybe thats coming from closing the window?
                break;

            case SDL_WINDOWEVENT:
            {
                if( ! (event->type == SDL_WINDOWEVENT) )
                {
                    break;
                }

                *winsize_changed = *winsize_changed || true;
            }
            break;


            case SDL_KEYDOWN:
                kc = SDL_GetKeyFromScancode(event->key.keysym.scancode);
                //putchar('\n');
                //printf("scan code %3d  --> ", event->key.keysym.scancode);
                //printf("key code %10d  --- key code with 0x40000000 cleared: %3d\n", kc, kc & (~0x40000000));
                //if( kc & 0x40000000 ) printf(" -> Sonderzeichen\n");
                //putchar('\n');

                km = SDL_GetModState();
                //printf("key modifier %d\n", km);

                if( get_kmapping(kmap, kmsize, kc, km, &outval) )
                {
                    //printf("_KEY_ %d maps to %d (%c)\n", kc, outval, outval);
                    return outval;
                }
                else
                {
                    //printf("_KEY_ No mapping found\n");
                    //printf("kc = %d\n", kc);
                }

            default:
                break;
        }


    }

    return NONE;
}
