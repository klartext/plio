/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "display.h"
#include "display_dir.h"
#include "dircontents.h"
#include "display_index.h"
#include "geometry.h"



void setup_initial_dirview( APP * app, DIR_COLLECTION * dcollp, INDEX_LAYOUT * layout )
{
    int idx = 0;
    int win_w = 0;
    int win_h = 0;
    int res = 0;

    // Setting up the GRID with the DIRCONTENTS'
    // -----------------------------------------
    for( idx = 0; idx < dcollp->dc_num; idx++ )
    {
        DIRCONTENTS * dcont = &dcollp->dircontvec[idx];

        while( dcont->widx == 0 ) // in case image the imagefile is crap, repeat reading
        {
            if(dcont->completed) // completed, but widx is 0, then no valid image could be read
            {
                fprintf(stderr, "fucked up\n"); // TODO: name-entry is non-null, so reading the image would be non-NULL
                abort(); // TODO: maybe setting image-pointer for image 0 to NULL could help (or disable the Dir)
            }
            read_next_image_into_dircontents(dcont, app->thumb_load_size, NULL); // first image (which is referenced by RPL!, hence needed for DirsView!)
        }

        dcont->rpl = (IMAGE**) &dcont->nav.grid->payload[0]; // Now 1st image is in GRID - rpl now points to image 0.

        lock_grid(dcollp->nav.grid); // LOCK Grid Mutex
        dcollp->nav.grid->payload[idx] = dcont;
        unlock_grid(dcollp->nav.grid); // UNLOCK Grid Mutex

        setup_initial_indexview(app, dcont, layout); // Initializing the Dir-Contents
    }
    grid_set_used_length(dcollp->nav.grid, dcollp->dc_num);

    SDL_GetWindowSize(app->window, &win_w, &win_h);

    calc_grid_navigation(win_w, win_h, &dcollp->nav, layout);

    assert(res == 0);

    return;
}



/** \brief draw thumbnails, farmes and active frame; return (via pointers) number of items in the selected/active store, as well as completed-flag).

    For DirsView, return number of files/images in selected Dir.
*/
void draw_dirsview_thumbs_frames( APP * app, INDEX_LAYOUT * layout, NAV * navp )
{
    GRID  * dcgrid = navp->grid;
    RANGE * range  = &navp->view;
    int num_thumbs = range->max - range->min + 1;

    DIRCONTENTS * dc = NULL;
    IMAGE * img = NULL;
    SDL_Rect rect;
    int idx = 0;

    int win_w = 0;
    int win_h = 0;

    SDL_GetWindowSize(app->window, &win_w, &win_h);

    draw_thumb_frames(app->renderer, app->drawcol, layout, num_thumbs, win_w, win_h);
    for( idx = 0; idx < num_thumbs; idx++ )
    {
        dc  = grid_get_payload_by_index_raw(dcgrid, range->min + idx); // CONTENTS
        if( dc == NULL )
        {
            return;
        }

        img  = *dc->rpl;

        rect = get_index_position(layout, img, idx, win_w, win_h);
        render_copy_thumbnail(app->renderer, img, &rect);

        if( range->min + idx == dcgrid->index ) // thick frame for active image
        {
            draw_frame(app->renderer, app->emphcol, rect.x, rect.y, layout->thumb_sidelength, layout->thumb_sidelength, layout->selection_frame_thickness);
        }

    }

    return;
}



SELECTED_ITEMS make_dirsview_title(void)
{
    SELECTED_ITEMS title;
    bzero(&title, sizeof(SELECTED_ITEMS));

    title.curr_fmt = "%5d ";
    title.max_fmt  = NULL;
    title.actual_sing  = "Directory";
    title.actual_plur  = "Directories";
    title.src_sing    = "";
    title.src_plur    = "";
    title.wrap_start  = "";
    title.wrap_end    = "";
    title.curr   = 0;
    title.max    = 0;

    return title;
}



SELECTED_ITEMS make_dirsview_footer(void)
{
    SELECTED_ITEMS footer;
    bzero(&footer, sizeof(SELECTED_ITEMS));

    footer.curr_fmt = "%5d ";
    footer.max_fmt  = NULL;
    footer.actual_sing  = "File:   ";
    footer.actual_plur  = "Files:  ";
    footer.src_sing    = "";
    footer.src_plur    = "";
    footer.wrap_start  = "";
    footer.wrap_end    = "DIRNAME";
    footer.curr   = 0;
    footer.max    = 0;

    return footer;
}
