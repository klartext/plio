/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include "keymappings.h"

/*
    See here for the SDL keycodes:
        https://wiki.libsdl.org/SDL2/SDL_Keycode

    See here for the SDL key modifiers:
        https://wiki.libsdl.org/SDL2/SDL_Keymod

    Or see for keyboard control in SDL in general here:
        https://wiki.libsdl.org/SDL2/CategoryKeyboard

*/

struct keymapping kmap[] = { { SDLK_q, KMOD_NONE, ALPH_q },
                             { SDLK_l, KMOD_NONE, RIGHT }, { SDLK_RIGHT, KMOD_NONE, RIGHT },
                             { SDLK_h, KMOD_NONE, LEFT },  { SDLK_LEFT, KMOD_NONE,  LEFT },
                             { SDLK_k, KMOD_NONE, UP },    { SDLK_UP, KMOD_NONE,    UP },
                             { SDLK_j, KMOD_NONE, DOWN },  { SDLK_DOWN, KMOD_NONE,  DOWN },
                             { SDLK_d, KMOD_NONE, 'd' },

                             { SDLK_HOME, KMOD_NONE, FIRST_POS },
                             { SDLK_END, KMOD_NONE, LAST_POS },

                             { SDLK_PAGEUP,   KMOD_NONE, PAGE_UP },
                             { SDLK_PAGEDOWN, KMOD_NONE, PAGE_DOWN },

                             { SDLK_PAGEUP,   KMOD_CTRL, CTRL_PAGEUP },
                             { SDLK_PAGEDOWN, KMOD_CTRL, CTRL_PAGEDOWN },

                             { SDLK_MINUS, KMOD_SHIFT, SYM_UNDERSCORE }, // '_'
                             //{ SDLK_UNDERSCORE, KMOD_NONE, SYM_UNDERSCORE }, // '_'
                             { SDLK_LESS, KMOD_RALT, SYM_VBAR }, // '|'

                             { SDLK_LESS, KMOD_NONE, SYM_LT },
                             { SDLK_LESS, KMOD_SHIFT, SYM_GT }, // '>'

                             //{ '|', KMOD_NONE, SYM_VBAR },
                             //{ '|', KMOD_RALT, SYM_VBAR },
                             //{ SDLK_KP_VERTICALBAR, KMOD_NONE, SYM_VBAR },
                             //{ SDLK_KP_VERTICALBAR, KMOD_RALT, SYM_VBAR },
                             //{ SDLK_UNDERSCORE, KMOD_NONE, SYM_VBAR },
                             //{ SDLK_UNDERSCORE, KMOD_SHIFT, SYM_UNDERSCORE }, // '_'


                             { SDLK_e, KMOD_NONE, ALPH_e },
                             { SDLK_f, KMOD_SHIFT, ALPH_F },
                             { SDLK_m, KMOD_NONE, ALPH_m },
                             { SDLK_n, KMOD_NONE, ALPH_n },
                             { SDLK_n, KMOD_SHIFT, ALPH_N },
                             { SDLK_p, KMOD_NONE, ALPH_p },
                             { SDLK_p, KMOD_SHIFT, ALPH_P },
                             { SDLK_r, KMOD_NONE, ALPH_r },
                             { SDLK_r, KMOD_SHIFT, ALPH_R },
                             { SDLK_s, KMOD_NONE, ALPH_s },
                             { SDLK_u, KMOD_SHIFT, ALPH_U },
                             { SDLK_x, KMOD_NONE, ALPH_x },
                             { SDLK_y, KMOD_NONE, ALPH_y },
                             { SDLK_o, KMOD_NONE, ALPH_o },
                             { SDLK_0, KMOD_NONE, ALPH_0 },
                             { SDLK_9, KMOD_NONE, ALPH_9 },

                             { SDLK_CARET, KMOD_NONE, SYM_CARET },

                             { SDLK_w, KMOD_CTRL, CTRL_w },
                             { SDLK_h, KMOD_CTRL, CTRL_h },
                             { SDLK_s, KMOD_CTRL, CTRL_s },
                             { SDLK_a, KMOD_CTRL, CTRL_a },
                             { SDLK_n, KMOD_CTRL, CTRL_n },
                             { SDLK_m, KMOD_CTRL, CTRL_m },
                             { SDLK_t, KMOD_CTRL, CTRL_t },

                             { SDLK_u, KMOD_CTRL, CTRL_u }, // Lab: L(uminance)
                             { SDLK_v, KMOD_CTRL, CTRL_v }, // Lab: a  -> should be used to (a,b) for (X,y)-grid

                             { SDLK_r, KMOD_CTRL, CTRL_r },

                             { SDLK_p, KMOD_CTRL, CTRL_p },

                             { SDLK_q, KMOD_CTRL, CTRL_q },

                             { SDLK_x, KMOD_CTRL, CTRL_x },
                             { SDLK_y, KMOD_CTRL, CTRL_y },

                             { SDLK_0, KMOD_CTRL, CTRL_0 },
                             { SDLK_9, KMOD_CTRL, CTRL_9 },

                             //{ SDLK_c, KMOD_CTRL, CTRL_c },

                             { SDLK_RETURN, KMOD_NONE, ENTER },
                           };


int kmsize = sizeof(kmap) / sizeof(struct keymapping);
