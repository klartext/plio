/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __COLLECTION__
#define __COLLECTION__

#include <limits.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "stringhelpers.h"
#include "dircontents.h"
#include "render.h"


typedef struct _dir_collection DIR_COLLECTION;
struct _dir_collection
{
    DIRCONTENTS * dircontvec;     // vector of DIRCONTENTS
    int    dc_num; // number of (used) entries in dirvec

    struct grid_navigation nav;

    bool completed; // currently a dummy: for compatibility with DIRCONTENTS (rendering-abstraction to 2D-array)

    RENDER_UPDATE rsel; // rsel: render_selection

    bool winsize_changed;
    bool dump_screen;
    struct timespec t_render_request;
    struct timespec t_last_rendering;
};


DIR_COLLECTION create_dir_collection( STRING_VEC * startdirs_uniq, bool recursive, bool mogrify_info );
void free_dir_collection( DIR_COLLECTION * dcollp );

void show_dircollection_info( DIR_COLLECTION * dcollp, bool with_dircontents );


int extract_dircoll_rsel( DIR_COLLECTION * dircoll ); // fun to extract render-selector from DIR_COLLECTION

#endif
