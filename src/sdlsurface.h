/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __SDLSURFACE__
#define __SDLSURFACE__

#include <SDL2/SDL.h>

#include "colspace.h"


void print_pixel_colors( SDL_Surface * surface, int pixel_idx );
void calculate_rgb_centroid( SDL_Surface * surface, RGBF * cent );

uint8_t pixel32_colval( uint32_t pixel, uint32_t mask, uint8_t shift, uint8_t loss );

#endif
