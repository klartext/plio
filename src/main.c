/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>

#include <SDL2/SDL.h>

#include "config.h"
#include "collection.h"
#include "commandenums.h"
#include "common.h"
#include "display.h"
#include "events.h"
#include "init.h"
#include "main.h"
#include "stringhelpers.h"
#include "threadsupport.h"
#include "ustate.h"
#include "ctrlloop_dir.h"
#include "render.h"
#include "display_dir.h"
#include "tools.h"



/*
From getop(3) manpage:
  "By  default, getopt() permutes the contents of argv as it scans, so that
   eventually all the nonoptions are at the end."
*/
int get_cli_options( CLI_OPTIONS* options, char * myopts, int argc, char* argv[] )
{
  int c = 0;

  while((c = getopt( argc, argv, myopts ) ) != -1 )
  {
    switch( c )
    {
      case 's': options->subdirs   = 1; break;
      case 'm': options->mogrify   = 1; break;
      case 'v': options->verbose   = 1; break;
      case 'h': options->help      = 1; break;
      case '?': fprintf(stderr, "cli-parsing error\n"); exit(EXIT_FAILURE);
    }
  }

  return 0;
}


void print_help_message(void)
{
    printf("plio help message: command line arguments\n");
    printf("    -h -> print this help message\n");
    printf("    -s -> scan also subdirs for images\n");
    printf("    -m -> print mogrify commands for images you have flipped/rotated\n");
    printf("    -v -> verbose: print more information\n");

    return;
}



// Works, but behaviour of the SDL-functions seems to differ from what the SDL2-Wiki says!
void print_display_infos( APP * app )
{
    SDL_Rect rect;
    int res = 0;
    int num = SDL_GetNumVideoDisplays();

    if( num < 0 )
    {
        fprintf(stderr, "%s(); %s\n", __FUNCTION__, SDL_GetError());
        return;
    }

    for( int display_index = 0; display_index < num; display_index++ )
    {
        res = SDL_GetDisplayBounds(display_index, &rect);
        if( res < 0 )
        {
            fprintf(stderr, "%s(): %s\n", __FUNCTION__, SDL_GetError());
            return;
        }

        printf("Display %d: x = %4d, y = %4d, w = %4d, h = %4d ", display_index, rect.x, rect.y, rect.w, rect.h);

        if( display_index == SDL_GetWindowDisplayIndex(app->window) ) putchar('*');

        putchar('\n');
    }

    return;
}



void read_events( void * data )
{
    THREAD_DATA  *args = data;
    APP      * app = args->app;
    USTATE   * stp = &args->st;
    READ_KEY * input = &args->input;


    input->key      = NONE;
    stp->cmd_avail  = false;
    stp->cmd_done   = false;
    stp->cmd_failed = false;

    input->key = get_keys(&input->winsize_changed, app->delay); // INPUT
    if( input->winsize_changed )
    {
        args->needs_rendering = true;
    }

    //printf("Key found: %d  winsize_changed: %d\n", input->key, input->winsize_changed);

    return;
}



void start_app( STRING_VEC * dirs_uniq, CLI_OPTIONS * cli_options_p )
{
    APP application;
    APP * app = &application;
    DIR_COLLECTION dircoll_new;

    THREAD_DATA loop_thrdat;
    bzero(&loop_thrdat, sizeof(loop_thrdat));

    INDEX_LAYOUT bare_layout;
    INDEX_LAYOUT scaled_layout;

    int idx = 0;

    struct sigaction sigact_initial;
    sigaction(SIGINT, NULL, &sigact_initial); // save original SIGINT handler

    // setting up
    // ==========
    bzero(&application, sizeof(application));
    init_app(app);

    sigaction(SIGINT, &sigact_initial, NULL); // reinstantiate initial SIGINT handler

    //putchar('\n');
    //show_display_info(app);
    //putchar('\n');


    // set-up layout
    // -------------
    bare_layout = set_index_layout(OUTER_FRAME, THUMB_SIDELENGTH, MIN_SEP, SELECTION_FRAME_THICKNESS);
    scaled_layout = scale_index_layout(&bare_layout, app->prescale);

    bare_layout = scaled_layout; // use the scaled layout as bare layout


    clear_screen(app, true);

    dircoll_new = create_dir_collection(dirs_uniq, cli_options_p->subdirs, cli_options_p->mogrify);
    if( cli_options_p->verbose )
    {
        for( idx = 0; idx < dircoll_new.dc_num; idx++)
        {
            printf("Dir to use: %s\n", dircoll_new.dircontvec[idx].dirname);
        }
    }
    free_stringvec(dirs_uniq);

    loop_thrdat.app = app;
    loop_thrdat.dcollp = &dircoll_new;
    loop_thrdat.layout = &bare_layout;

    loop_thrdat.st.fsel = DIR_VIEW;
    loop_thrdat.st.cmd  = CMD_NOTHING;
    loop_thrdat.needs_rendering = true;

    if( dircoll_new.dc_num > 0 )
    {
        setup_initial_dirview(app, loop_thrdat.dcollp, &bare_layout);
        while(true)
        {
            //printf("##################### Kopf der loop in start_app() #####################\n");
            read_events( (void*) &loop_thrdat);

            if( loop_thrdat.st.cmd == CMD_QUIT )
            {
                //printf("CMD_QUIT, shutting down program\n");
                break;
            }

            {
                DIRCONTENTS * currdir = grid_get_payload(loop_thrdat.dcollp->nav.grid);
                //printf("Current dir: %s\n", currdir->dirname);
                //printf("Current fsel: %d\n", loop_thrdat.st.fsel);

                if( loop_thrdat.st.fsel == INDEX_VIEW || loop_thrdat.st.fsel == IMAGE_VIEW )
                {
                    read_next_image_into_dircontents(currdir, loop_thrdat.app->thumb_load_size, &loop_thrdat.needs_rendering);
                }
            }

            command_eval_wrapper(&loop_thrdat);

            app->current_display = SDL_GetWindowDisplayIndex(app->window);
            if( app->current_display < 0 )
            {
                break; // can't work, when no display is available
            }
            render_any((void *) &loop_thrdat);
        }
    }
    else
    {
        fprintf(stderr, "No images found in any directory\n");
    }

    // Clean up
    // --------
    free_dir_collection(&dircoll_new);
    shutdown_app(app);
    return;
}



int main( int argc, char* argv[] )
{
    CLI_OPTIONS cli_option = {0}; // default values
    char* myopts = "smvh";        // accepted options

    PATHFIDDLE startpath;

    STRING_VEC dirs;
    STRING_VEC dirs_uniq;

    int idx = 0;

    THREAD_DATA loop_thrdat;
    bzero(&loop_thrdat, sizeof(loop_thrdat));

    // parse CLI options
    // -----------------
    get_cli_options( &cli_option, myopts, argc, argv );

    if( cli_option.help ) // print help message and exit
    {
        print_help_message();
        exit(0);
    }

    // if dirs were given, use them; else use "." as startdir
    // ------------------------------------------------------
    if( argc - optind > 0 ) // read the directory names from cli
    {
        calloc_strvec(&dirs, argc - optind);
        int wridx = 0;
        for( idx = optind; idx < argc; idx++ )
        {
            startpath = calculate_paths(argv[idx]);
            dirs.vec[wridx] = strdup(startpath.real);
            wridx++;
        }
        dirs.used = argc - optind;

        dirs_uniq = stringvec_sort_uniq(&dirs);
        free_stringvec(&dirs);
    }
    else // no directory names given via cli - use "." as dirname
    {
        calloc_strvec(&dirs_uniq, 1);
        startpath = calculate_paths(".");
        dirs_uniq.vec[0] = strdup(startpath.real);
        dirs_uniq.used = 1;
    }

    // here we go
    // ----------
    start_app(&dirs_uniq, &cli_option);

    return 0;
}
