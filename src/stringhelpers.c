/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#define _GNU_SOURCE

#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libgen.h>

#include <assert.h>

#include "macros.h"
#include "stringhelpers.h"
#include "selection.h"



PATHFIDDLE calculate_paths( const char * origpath )
{
    struct pathfiddling p;

    p.orig[PATH_MAX]     = '\0';
    p.tmp[PATH_MAX]      = '\0';
    p.real[PATH_MAX]     = '\0';
    p.dirname[PATH_MAX]  = '\0';
    p.basename[PATH_MAX] = '\0';

    strncpy(p.orig, origpath, PATH_MAX);
    realpath(p.orig, p.real);

    strncpy(p.tmp, p.orig, PATH_MAX);
    strcpy(p.dirname, dirname(p.tmp));

    strncpy(p.tmp, p.orig, PATH_MAX);
    strcpy(p.basename, basename(p.tmp));

    return p;
}



/** \brief Give position of last (rightmost) dot (".") in the string, or -1 if no dot has been found.
*/
int last_dot_position( char * string )
{
    int pos = -1;
    int idx = 0;

    while(idx < (int) strlen(string))
    {
        if(string[idx] == '.')
        {
            pos = idx;
        }
        idx++;
    }

    return pos;
}



bool extension_is_equal( char* filename, char* extension )
{
    bool   res   = false;
    char * found = NULL;

    int rightmost = last_dot_position(filename); // start searching at rightmost dot

    if( rightmost <  0 ) // not any dot found at all
    {
        return false;
    }


    found = strcasestr(filename + rightmost, extension);

    if( found != NULL && ! strcasecmp(extension, found) )
    {
        res = true;
    }

    return res;
}



void print_stringvec( STRING_VEC * stv, char * sep )
{
    assert(stv != NULL);
    for( int idx = 0; idx < stv->used; idx++ )
    {
        printf("%s%s", stv->vec[idx], sep);
    }

    return;
}

void free_stringvec( STRING_VEC * stv ) // is idempotent, double use is ok
{
    errexit_on_NULL(stv);

    for( int idx = 0; idx < stv->used; idx++ )
    {
        free(stv->vec[idx]);
        stv->vec[idx] = NULL;
    }
    stv->used = 0;

    free(stv->vec);
    stv->vec = NULL;

    stv->num = 0;

    return;
}



int compstr( const void * v1p, const void * v2p )
{
    char ** s1p = (void*) v1p;
    char ** s2p = (void*) v2p;

    return strcmp(*s1p, *s2p);
}



void stringvec_sort( STRING_VEC * strvec )
{
    qsort(strvec->vec, (size_t) strvec->used, sizeof(char*), compstr);
    return;
}


static int delete_equal_strings_in_vec( char ** vec, int len )
{
    assert(vec != NULL);
    assert(len > 0);

    char ** s1p = NULL;
    char ** s2p = NULL;

    int minimize = 0;

    // First: find and free duplicates
    // -------------------------------
    s2p = vec;

    while( s2p - vec < len)
    {
        s1p = s2p;
        ++s2p;

        while( s2p - vec < len )
        {
            if( strcmp(*s1p, *s2p)  == 0 ) // strings are equal
            {
                free(*s2p);
                *s2p = NULL;
                ++minimize;
            }
            else // strings are different
            {
                break;
            }

            ++s2p;
        }
    }

    return minimize;
}


static void condense_string_vec( char ** vec, int len )
{
    assert(vec != NULL);
    assert(len > 0);

    int rdidx = 0;
    int wridx = 0;

    while( wridx < len - 1 )
    {
        if( vec[wridx] != NULL ) // find lowest NULL to write into
        {
            wridx++;
            continue;
        }

        // move down available dircontents entries (copy and clear)
        // --------------------------------------------------------
        if( vec[wridx] == NULL ) // if target element is empty
        {
            rdidx = wridx + 1;
            while( rdidx < len )
            {
                if( vec[rdidx] != NULL ) // if src element is available
                {
                    memcpy(vec + wridx, vec + rdidx, sizeof(char*));
                    bzero(vec + rdidx, sizeof(char*));

                    rdidx = len; // end loop
                    break; // only one src element allowed to copy into empty target
                }
                rdidx++;
            }
        }

        wridx++;
    }


    return;
}




// like sort -u on the shell, creates new STRING_VEC
STRING_VEC stringvec_sort_uniq( STRING_VEC * incoming )
{
    assert(incoming != NULL);
    assert(incoming->used);

    int idx = 0;
    STRING_VEC outgoing;
    bzero(&outgoing, sizeof(STRING_VEC));

    // alloc vec for the copy of the incoming data
    // -------------------------------------------
    outgoing.vec = calloc(incoming->used, sizeof(char*)); // malloc might be ok too
    assert(outgoing.vec != NULL);
    outgoing.num = incoming->used;

    // copying all strings from incoming to outgoing
    // ---------------------------------------------
    for( idx = 0; idx < incoming->used; idx++ )
    {
        outgoing.vec[idx] = strdup(incoming->vec[idx]);
    }
    outgoing.used = incoming->used; // so far we have not removed potential duplicates

    // Now sort outgoing
    // -----------------
    stringvec_sort(&outgoing);

    // Uniq
    // ----
    int lowered_by = delete_equal_strings_in_vec(outgoing.vec, outgoing.used);
    condense_string_vec(outgoing.vec, outgoing.used);

    outgoing.used -= lowered_by;

    return outgoing;
}



/** \brief Allocate the char** vector and set the other values accordingly.
*/
void calloc_strvec( STRING_VEC * stv, int num )
{
    stv->vec = calloc(num, sizeof(char*));
    if( stv->vec == NULL )
    {
        exit(EXIT_FAILURE);
    }

    stv->num  = num;
    stv->used = 0;  // this will be set by the user/application

    return;
}



/** Create STRING_VEC from a single string - helper to get a STRING_VEC, when only (char*) is available.
*/
STRING_VEC stringvec_of_string( char * string )
{
    STRING_VEC strvec;

    calloc_strvec(&strvec, 1);
    strvec.vec[0] = strdup(string);
    strvec.used = 1;

    return strvec;
}



/** \brief Concatenate two String-Vec's, returning a freshly created String-Vec. Input String-Vecs stay unchanged.

    Length of the concattenated StringVec is sum of the used elements of the input.
    New strings are allocated with strdup().
*/
STRING_VEC stringvec_concat( STRING_VEC * stv1, STRING_VEC * stv2 )
{
    STRING_VEC concat;
    calloc_strvec(&concat, stv1->used + stv2->used);

    int rdidx = 0; // read index
    int wridx = 0; // write index

    // Copy strtings from stv1 to concat
    // ---------------------------------
    rdidx = 0;
    while(rdidx < stv1->used)
    {
        concat.vec[wridx] = strdup(stv1->vec[rdidx]);
        ++rdidx;
        ++wridx;
    }

    // Copy strtings from stv2 to concat
    // ---------------------------------
    rdidx = 0;
    while(rdidx < stv2->used)
    {
        concat.vec[wridx] = strdup(stv2->vec[rdidx]);
        ++rdidx;
        ++wridx;
    }

    concat.used = concat.num;

    return concat;
}



/* \brief Append 'append_this' to 'append_here'. The String-Vec 'append_here' will be changed (reallocation), while 'append_this' will be unchanged.

    On success, true is returned; otherwise false is returned and a warning is prnted to stderr.
    In case of an error, all input data can be reused as it was before, just appending did not work.
*/
bool stringvec_append( STRING_VEC * append_here, STRING_VEC * append_this )
{
    int rdidx = 0; // read index
    int wridx = 0; // write index
    void * ret = NULL; // retval of realloc(3), either NULL (fail) or new address (success).

    int int_needed_length = append_here->used + append_this->used; // new needed vec-size

    if( append_here->num > 0 ) // truly appending to existing memory
    {
        ret = realloc(append_here->vec, sizeof(char*) * int_needed_length);
        if( ret == NULL )
        {
            fprintf(stderr, "WARNING: %s(): could not append STRING_VEC (reusing old value)\n", __FUNCTION__);
            return false;
        }
    }
    else // for appending to an empty STRING_VEC, which means first memory allocation
    {
        ret = malloc(sizeof(char*) * int_needed_length);
        if( ret == NULL )
        {
            fprintf(stderr, "Could not allocate memory in %s()\n", __FUNCTION__);
            abort();
        }
    }

    append_here->vec = ret;  // reallocation has been successful
    append_here->num = int_needed_length;


    // Copy strtings from stv1 to concat
    // ---------------------------------
    rdidx = 0;
    wridx = append_here->used;
    while(rdidx < append_this->used)
    {
        append_here->vec[wridx] = strdup(append_this->vec[rdidx]);
        ++rdidx;
        ++wridx;
    }

    append_here->used = append_here->num; // vector can be used completely

    return true;
}



/** \brief Apply function 'stringfunc' to each string of the StringVec.
*/
void stringvec_apply( STRING_VEC * stv, void (*stringfunc)(char * str) )
{
    for( int idx = 0; idx < stv->used; idx++ )
    {
        (*stringfunc)(stv->vec[idx]);
    }
    return;
}



/** \brief Creates new string-vec, which only contains strings from the input StringVec, for which stringcheck function returns true.
*/
STRING_VEC stringvec_filter( STRING_VEC * stv, bool (*stringcheck)(char * str) )
{
    STRING_VEC filtered;
    calloc_strvec(&filtered, stv->used);

    for( int idx = 0; idx < filtered.num; idx++ )
    {
        if( (*stringcheck)(stv->vec[idx]) )
        {
            filtered.vec[filtered.used] = strdup(stv->vec[idx]);
            ++filtered.used;
        }
    }

    return filtered;
}



/** \brief Create string of selected items.

    When max_fmt == NULL, don't add the value of max.

*/
void string_of_selected_items( char * textbuf, SELECTED_ITEMS * sel )
{
    char * text = NULL;  // the selected text out of singular/plural

    char tmpbuf[PATH_MAX];
    tmpbuf[0]  = '\0';
    textbuf[0] = '\0';

    // -------------------------------------------------------------------
    if( sel->curr_fmt )
    {
        sprintf(tmpbuf, sel->curr_fmt, sel->curr);
    }
    text  = singularplural(sel->curr, sel->actual_sing, sel->actual_plur);
    strcat(tmpbuf, text);

    strcat(tmpbuf, sel->wrap_start);
    strcat(textbuf, tmpbuf);                            // push to textbuf
    // -------------------------------------------------------------------
    tmpbuf[0] = '\0';

    if( sel->max_fmt != NULL)
    {
        sprintf(tmpbuf,sel->max_fmt, sel->max);
    }

    text  = singularplural(sel->max, sel->src_sing, sel->src_plur);
    strcat(tmpbuf, text);

    strcat(tmpbuf, sel->wrap_end);

    strcat(textbuf, tmpbuf);                            // push to textbuf
    // -------------------------------------------------------------------

    return;
}



/** \brief rename a file (given as abspath) by prepending a string to the basename.

    The buffer must be long enough for the resulting path (PATH_MAX recommended) and be cleared.
*/
void prepend_string_to_basename( const char * abspath, const char * prepend_string, char * buf )
{
    PATHFIDDLE pf = calculate_paths(abspath);

    strcat(buf, pf.dirname);
    strcat(buf, "/");
    sprintf(buf + strlen(buf), "%s", prepend_string);
    strcat(buf, pf.basename);

    return;
}


void prepend_string_to_extension_of_basename( const char * abspath, const char * replacement_string, char * buf )
{
    PATHFIDDLE pf = calculate_paths(abspath);

    assert(abspath);
    assert(replacement_string);
    assert(buf);

    int extension_start = last_dot_position(pf.basename); // position of the extension-dot in basename

    if( extension_start == -1 ) // should never occur, as files are filtered in the input stage
    {
        fprintf(stderr, "%s(): no file extension found in %s\n", __FUNCTION__, abspath);
        strcat(buf, abspath); // give same name back
        return;
    }

    strcat(buf, pf.dirname);
    strcat(buf, "/");
    sprintf(buf + strlen(buf), "%s", replacement_string);
    strcat(buf, pf.basename + extension_start); // basename from the pos. of the dot

    return;
}
