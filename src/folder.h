/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __FOLDER__
#define __FOLDER__

#include "stringhelpers.h"

typedef struct _folder FOLDER;
typedef struct _folderlist FOLDER_LIST;

struct _folder
{
    char * abspath; // if NULL, then the folder is a virtual folder
    char * name; // symbolic name / tag
};

int count_files_extension_filtered(char * dirname, bool do_recursive ); // tested
int count_dirs (char * dirname, bool recursive );                       // tested

STRING_VEC read_regfilenames_of_single_dir_to_vec(char * dirname ); // some tests (result.used only)
STRING_VEC read_dirnames_of_single_dir_to_vec(char * dirname );     // some tests (result.used)

void print_names_of_imagefiles_and_dirs( char * dirname );

STRING_VEC collect_subdirnames( char * dirname, bool collect_orig, bool recursive ); // tests for result.used
STRING_VEC collect_dirs_subdirs_for_dirsvec( STRING_VEC * dirsvec, bool recursive );

#endif

