/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __GEOMETRY__
#define __GEOMETRY__

#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include <math.h>

#include <SDL2/SDL.h>

#include "nav.h"
#include "image.h"
#include "indexlayout.h"

void index_to_indexpair( int index, int width, int * x_pos, int * y_pos );

void calc_itemnum_and_padding( int window_size, int outer_frame, int box_width, int minsep, int * num, int * padding );
int calc_box_position( int num, int box_width, int minsep, int padding_space, int idx );

void calc_grid_navigation( int win_w, int win_h, NAV * navp, INDEX_LAYOUT * bare_layout );

RANGE calc_imgview_range( INDEX_DIM * layout, int num_images, int scroll );
SDL_Rect get_index_position( INDEX_LAYOUT * thumbview, IMAGE * img, int idx, int win_w, int win_h );

SDL_FRect aspect_corr_with_rot_corr( int image_width, int image_height, int targe_width, int target_height, ROTATION rot );
SDL_FRect rot_offset_correction( SDL_FRect* frec, ROTATION rot );

#endif
