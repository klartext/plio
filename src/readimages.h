/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __READ_IMAGES__
#define __READ_IMAGES__

#include <SDL2/SDL.h>

#include "image.h"
#include "stringhelpers.h"

IMAGE * read_first_image_of_dir( char * dirname, int thumbsize );
bool stringvec_contains_file_with_valid_fif( STRING_VEC * filenames );

#endif
