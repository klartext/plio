/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __CLOCK_FILETIME__
#define __CLOCK_FILETIME__

#include <stdio.h>
#include <time.h>

void get_timestring_from_timespec( char * buf, const struct timespec * tsp );
void print_timestring( const struct timespec * tsp );

void print_clock_res(FILE * chan);
void print_clock(FILE * chan, char * end);
void get_clock( char * buf_21B ); // buffer must have at least 21 Bytes (20 + "\0" as string delimiter)
struct timespec * copy_timespec( struct timespec * dst, struct timespec * src );

int modtime_of_file_hr( const char * abspath, struct timespec * tsp );
int cmp_timespec( const struct timespec * ts1, const struct timespec * ts2 );

#endif
