/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __GRID__
#define __GRID__

#include <stdbool.h>
#include <pthread.h>

#include <SDL2/SDL.h>

#include "mutexsupport.h"

typedef struct _grid GRID;
typedef int (*INTACCESS)(GRID * grid);
typedef struct _grid GRID;

struct _grid
{
    int index;  // current index (for inserting, readout etc)
    int veclen; // vector length (number of elements)
    int xlen;   // number of elements per row (x-length)

    int used_length; // length of used data, between 0 and veclen

    int cursor; // index-value for active image

    void ** payload;

    void * repr; // representational payload (pars pro toto)

    INTACCESS curidx;
    INTACCESS len;

    INTACCESS xincr;
    INTACCESS xdecr;

    INTACCESS yincr;
    INTACCESS ydecr;

    INTACCESS row;
    INTACCESS store; // store current index as crusor and return that value

    void * (*get)(GRID * grid);
    void * (*get_by_xy)(GRID * grid, int x, int y);

    MUTEX_WITH_ATTR mut;
};



GRID * create_grid( int initial_length, int x_len );
void destroy_grid( GRID * grid );

// The following functions come in pairs:
// 1) default-functions do lock/unlock the Mutex,
// 2) _raw-functions don't use the mutex.
//
// For just accessing the grid in a safe manner through individual
// functions, use the default functions.
// In case more than one of these functions are intended to be used in sequence,
// use the _raw-functions, wrapped inside lock_grid() and unlock_grid().
// This way the grid is ensured to be not changed in between different
// grid-access functions.
// (It's a potential race condition, when the mutex is locked/unlocked for each access.)


void grid_set_payload( GRID * grid, void * new_payload );
void grid_set_payload_raw( GRID * grid, void * new_payload );


// INCREMENT
// ---------
int grid_increment( GRID * grid );     // increment_x
int grid_increment_raw( GRID * grid ); // increment_x

int grid_increment_y( GRID * grid );
int grid_increment_y_raw( GRID * grid );

int grid_increment_used_length( GRID * grid ); // this is for managing the grid settings itself
int grid_increment_used_length_raw( GRID * grid ); // this is for managing the grid settings itself

void grid_set_used_length( GRID * grid, int new_used_length );
void grid_set_used_length_raw( GRID * grid, int new_used_length );


// DECREMENT
// ---------
int grid_decrement( GRID * grid );     // decrement_x
int grid_decrement_raw( GRID * grid ); // decrement_x

int grid_decrement_y( GRID * grid );
int grid_decrement_y_raw( GRID * grid );


// grid-to-grid copy would be nice -> Mischung aus grid_register_range_payload() und grid_payload_copy() -> copy_num felt da noch
void grid_register_range_payload( GRID * grid, int src_start_offset, int copy_num, void ** payload_source );
void grid_register_range_payload_raw( GRID * grid, int src_start_offset, int copy_num, void ** payload_source );


GRID * grid_copy_create( GRID * grid, int ini_len, int x_len, int copy_offset );
GRID * grid_copy_create_raw( GRID * grid, int ini_len, int x_len, int copy_offset );

void grid_payload_copy( GRID * src, GRID * dst, int copy_offset );
void grid_payload_copy_raw( GRID * src, GRID * dst, int copy_offset );

void grid_payload_ncopy( GRID * src, GRID * dst, int copy_num, int copy_offset );
void grid_payload_ncopy_raw( GRID * src, GRID * dst, int copy_num, int copy_offset );


void * grid_get_payload( GRID * grid );
void * grid_get_payload_raw( GRID * grid );

void * grid_get_payload_by_index( GRID * grid, int idx );
void * grid_get_payload_by_index_raw( GRID * grid, int idx );

void * grid_get_payload_xy( GRID * grid, int xidx, int yidx );
void * grid_get_payload_xy_raw( GRID * grid, int xidx, int yidx );


int grid_set_index( GRID * grid, int index );
int grid_set_index_raw( GRID * grid, int index );

int grid_get_index( GRID * grid );
int grid_get_index_raw( GRID * grid );

int grid_get_last_index( GRID * grid );
int grid_get_last_index_raw( GRID * grid );


int grid_row( GRID * grid );
int grid_row_raw( GRID * grid );

int grid_col( GRID * grid );
int grid_col_raw( GRID * grid );


int grid_get_xlen_raw( GRID * grid );
int grid_get_xlen( GRID * grid );

void grid_set_xlen( GRID * grid, int xlen );
void grid_set_xlen_raw( GRID * grid, int xlen );

int grid_length( GRID * grid );
int grid_length_raw( GRID * grid );


int grid_get_cursor( GRID * grid );
int grid_get_cursor_raw( GRID * grid );

int grid_store_cursor( GRID * grid );
int grid_store_cursor_raw( GRID * grid );


bool in_first_row( GRID * grid );
bool in_first_row_raw( GRID * grid );

bool in_first_col( GRID * grid );
bool in_first_col_raw( GRID * grid );


bool in_last_row( GRID * grid );
bool in_last_row_raw( GRID * grid );

bool in_last_col( GRID * grid );
bool in_last_col_raw( GRID * grid );

bool is_last_element( GRID * grid );
bool is_last_element_raw( GRID * grid );


void grid_register_single_payload_xy( GRID * grid, int xidx, int yidx, void * payload );
void grid_register_single_payload_xy_raw( GRID * grid, int xidx, int yidx, void * payload );

void grid_unregister_single_payload( GRID * grid, int xidx, int yidx );
void grid_unregister_single_payload_raw( GRID * grid, int xidx, int yidx );


void grid_reverse_payload_order( GRID * grid );
void grid_reverse_payload_order_raw( GRID * grid );

void grid_exchange_payloads( GRID * grid, int idx_1, int idx_2 );
void grid_exchange_payloads_raw( GRID * grid, int idx_1, int idx_2 );


void grid_sort( GRID * grid, int payload_size, int (*Compare)(const void * pl1, const void * pl2) );
void grid_sort_raw( GRID * grid, int payload_size, int (*Compare)(const void * pl1, const void * pl2) );

void grid_apply( GRID * grid, void (*Func)(void*) );
void grid_apply_raw( GRID * grid, void (*Func)(void*) );


// Printer Functions
// -----------------
void show_grid_delim( GRID * grid, char * delim, char * end );
void show_grid_delim_raw( GRID * grid, char * delim, char * end );

void show_grid( GRID * grid );
void show_grid_raw( GRID * grid );

void grid_iter( GRID * grid, void (*Func)(void*) );
void grid_iter_raw( GRID * grid, void (*Func)(void*) );

// -----------------
void grid_rotate_right_raw( GRID * grid, int start_idx, int end_idx );
void grid_rotate_left_raw( GRID * grid, int start_idx, int end_idx );

void lock_grid( GRID * grid );
void unlock_grid( GRID * grid );

#endif

