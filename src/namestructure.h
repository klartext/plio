/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __NAMESTRUCTURE__
#define __NAMESTRUCTURE__

#include <limits.h>

typedef struct _namestructure_ NAMESTRUCTURE;

/*
struct _namestructure_
{
    char * name;
    char * name_structure;
};
*/

struct _namestructure_
{
    char name [PATH_MAX];
    char name_structure [PATH_MAX];
};

char * new_namestructure_from_string( char * string );

#endif
