/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __RANGE__
#define __RANGE__

#include <limits.h>
#include <stdbool.h>

typedef struct range RANGE;
struct range
{
    int min;
    int max;
};

int bounded_int( int anyint, RANGE * range );
RANGE clip_range( RANGE * anyrange, RANGE * border );
RANGE range_from_intpair( int a, int b );
RANGE shift_range( RANGE * range, int shift );

void show_range_raw( RANGE * range, char* msg, char* sep, char* end );
void show_range( RANGE * range );

// additional helpers - calculations as if the RANGE represents a GRID

typedef struct _range_as_grid_properties RAG_PROP;
struct _range_as_grid_properties
{

    bool first; // index is first pos. in the range as grid
    bool last;  // index is last pos. in the range as grid

    bool first_row;
    bool first_col;

    bool last_row;
    bool last_col;

    bool below;   // index is below range
    bool above;   // index is above range
    bool outside; // index is outside range
    bool inside; // index is inside range

    int  local_idx; // index value relative to range.min as 0
};

RAG_PROP rag( RANGE * range, int index, int xlen );

void print_ragprop( RAG_PROP * prop ); // print name of true properties, one per line

#endif
