/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __ENVIRONMENT__
#define __ENVIRONMENT__

#include <stdbool.h>

int use_env_int_nonzero( char * env_varname, int default_int );
char * use_env_string( char * env_varname, char * default_string );
double use_env_double_nonzero( char * env_varname, double default_dbl );

#endif
