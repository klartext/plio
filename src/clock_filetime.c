/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#define _DEFAULT_SOURCE

#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include "clock_filetime.h"

#define SELECTED_CLOCK CLOCK_REALTIME


// Buffer must have at least 21 Bytes
void get_timestring_from_timespec( char * buf, const struct timespec * tsp )
{
    char tmp[100];

    buf[0] = '\0';
    tmp[0] = '\0';

    sprintf(tmp, "%010ld", tsp->tv_sec);
    strcat(buf, tmp);

    sprintf(tmp, "%.9f", tsp->tv_nsec / 1000000000.0);
    strcat(buf, tmp + 1);
}


void print_timestring( const struct timespec * tsp )
{
    char buf[30];
    buf[0] = '\0';

    get_timestring_from_timespec(buf, tsp);
    printf("%s\n", buf);

    return;
}


void get_clock( char * buf_21B )
{   // buf_21B: buffer with at least 21 Bytes
    struct timespec ts;
    int res = 0;

    buf_21B[0] = '\0';

    res = clock_gettime(SELECTED_CLOCK, &ts);
    if( res == 0 )
    {
        get_timestring_from_timespec(buf_21B, &ts);
    }
    else
    {
        fprintf(stderr, "TIMER: not accessible\n");
    }

}

struct timespec * copy_timespec( struct timespec * dst, struct timespec * src )
{
    return memcpy(dst, src, sizeof(struct timespec));
}

void print_clock_res(FILE * chan)
{
    struct timespec ts;
    int res = 0;

    res = clock_getres(SELECTED_CLOCK, &ts);
    if( res == 0 )
    {
        fprintf(chan, "TIMER-RESOLUTION: %ld (sec) %ld (nsec)\n", ts.tv_sec, ts.tv_nsec);
    }
    else
    {
        fprintf(chan, "TIMER-RESOLUTION: not accessible\n");
    }

}

void print_clock(FILE * chan, char * end)
{
    struct timespec ts;
    int res = 0;
    char buf[100];

    end = (end == NULL) ?  "\n": end;

    get_clock(buf);

    res = clock_gettime(SELECTED_CLOCK, &ts);
    if( res == 0 )
    {
        fprintf(chan, "%s%s", buf, end);
    }
    else
    {
        fprintf(chan, "TIMER-RESOLUTION: not accessible\n");
    }

}



/* \brief Write the high resolution timestamp of the file into the timespec struct via pointer tsp.

    On success, 0 is returned, on failure -1 is returned.
*/
int modtime_of_file_hr( const char * abspath, struct timespec * tsp )
{
    struct stat st;
    int res = 0;

    res = stat(abspath, &st);
    if( res != 0 )
    {
        fprintf(stderr, "stat(2) failed (setting value to 0): %s\n", strerror(errno));
        return -1;
    }

    tsp->tv_sec = st.st_mtim.tv_sec;
    tsp->tv_nsec = st.st_mtim.tv_nsec;

    return 0;
}


static int int_cmp( int val_1, int val_2 )
{
    if( val_1 < val_2 )
    {
        return -1;
    }

    if( val_1 > val_2 )
    {
        return 1;
    }

    return 0;
}


// if ts1 < ts2, return -1
// if ts1 == ts2, return 0
// if ts1 > ts2, return +1
int cmp_timespec( const struct timespec * ts1, const struct timespec * ts2 )
{
    int cmp_sec  = int_cmp(ts1->tv_sec,  ts2->tv_sec);
    int cmp_nsec = int_cmp(ts1->tv_nsec, ts2->tv_nsec);

    if( cmp_sec != 0 )
    {
        return cmp_sec;
    }
    else
    {
        return cmp_nsec;
    }
}
