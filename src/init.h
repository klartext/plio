/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#ifndef __INIT__
#define __INIT__

#include "common.h"

void initialize_sdl(void);
void shutdown_sdl(void);
SDL_Window * start_centered_window( char *title, int width, int height );
void destroy_window(APP * app );

void init_app( APP * app );
void shutdown_app( APP * app );

#endif
