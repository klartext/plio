/* ========================================================================== */
/*                 P L I O    I m a g e  V i e w e r                          */
/* ========================================================================== */
/* This file is part of the plio image viewer.                                */
/*                                                                            */
/* PLIO stands for "Pleasant Image Order" - indicating many sort options.     */
/*                                                                            */
/* Author/Copyright: Oliver Bandel                                            */
/* Copyleft:         This program is free (libre) software.                   */
/* License:          GPL-3.0-or-later                                         */
/* ========================================================================== */

#include <stdio.h>
#include <assert.h>

#include "fibsurftex.h"
#include "fib.h"


/** \brief Convert a FreeImageLib Bitmap to a SDL2 Surface - may return NULL on error.

    This functions uses SDL_CreateRGBSurfaceFrom(),

        SDL_CreateRGBSurfaceFrom():

        This function operates mostly like SDL_CreateRGBSurface(), except it
        does not allocate memory for the pixel data, instead the caller
        provides an existing buffer of data for the surface to use.

        No copy is made of the pixel data. Pixel data is not managed automatically;
        you must free the surface before you free the pixel data.

        https://wiki.libsdl.org/SDL2/SDL_CreateRGBSurfaceFrom
*/
SDL_Surface * sdl_surface_from_fib( FIBITMAP * fibm )
{
    assert(fibm != NULL);
    SDL_Surface * surface = NULL;

    surface = SDL_CreateRGBSurfaceFrom(
        FreeImage_GetBits(fibm),
        FreeImage_GetWidth(fibm),
        FreeImage_GetHeight(fibm),
        FreeImage_GetBPP(fibm),
        FreeImage_GetPitch(fibm),
        FreeImage_GetRedMask(fibm),
        FreeImage_GetGreenMask(fibm),
        FreeImage_GetBlueMask(fibm),
        0
    );

    if( surface == NULL )
    {
        fprintf(stderr, "%s(): %s", __FUNCTION__, SDL_GetError());
    }

    return surface;
}



/** \brief Load an image file with FreeImage-Lib and convert it to a SDL2 Surface. The FreeImage-Bitmap is returned as well.
*/
static SURF_FIB load_image_as_surface( char * file_abspath )
{
    SURF_FIB surf;
    surf.surface = NULL;
    surf.fib     = NULL;
    IMG_META input;

    // Create Data Structures and save them in the structure
    // -----------------------------------------------------
    input = read_imagefile_resize(file_abspath, 0);

    surf.fib     = input.fib;

    if( surf.fib != NULL )
    {
        surf.surface = sdl_surface_from_fib(surf.fib);
    }

    return surf;
}



/** \brief Load an image file with FreeImage-Lib and convert it to a SDL2 Texture.

    This function replaces IMG_LoadTexture() from SDL_image library (SDL2),
    which for certain image files failed to create a texture.
*/
SDL_Texture * load_image_as_texture( SDL_Renderer * renderer, char * file_abspath )
{
    SDL_Texture * texture = NULL;
    SURF_FIB surf;

    surf = load_image_as_surface(file_abspath);
    if( surf.surface != NULL )
    {
        texture = SDL_CreateTextureFromSurface(renderer, surf.surface);
        if( texture == NULL ) // no image will be shown, but no crash is to be expected
        {
            fprintf(stderr, "SDL_CreateTextureFromSurface() returned NULL (%s) (file: %s)\n", SDL_GetError(), file_abspath);
        }

        SDL_FreeSurface(surf.surface); // SDL1.2-docs: when SDL_CreateRGBSurfaceFrom() used, no free; SDL2-Wiki: no mention
        FreeImage_Unload(surf.fib);
        surf.fib = NULL;
    }

    return texture;
}



