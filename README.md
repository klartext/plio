PLIO Image Viewer
=================

*PLIO* stands for "Pleasant Image Order",
indicating, that *PLIO* offers a lot of sort options for the images,
so that you can view the images in the order that pleases you.

- [Tutorial for PLIO](https://codeberg.org/klartext/tutorial-for-plio)
- [Motivation to write PLIO](doc/motivation.md)
- [Key-Features](doc/keyfeatures.md)
- [First Steps](doc/firststeps.md)
- [Key-Bindings](doc/keybindings.md)
- [Sort and Rename Usage/Use Cases in Detail](doc/usecaes.md)
- [How Bulkrenaming works](doc/bulkrenaming.md)
- [Build and Install plio](doc/buildinstall.md)

## Info (2025-02-19)
After installing the SDL2-Compatibility library `sdl2-compat` not only was it necessary to add Header-Files,
which were included automatically by `SDL2` before.
Now from a very small memory footprint `plio` turned into a memory hungry monster,
eating even more memory than even the greedy `firefox`.

So if you can avoid installing the mentioned compatibility layer/library,
please do it and wait until `SDL3` will be mature enough to be used directly.

## Info (2024-06-24)
New commands: goto next marked image with key `n`, goto previous marked image with key `N`.

## Info (2023-10-25)
Two settings that so far were determined by the settings in the file `config.h` can now be set by the user:
- The font name can now be changed with the environment variable `PLIO_FONTNAME`.
- The prescaling (scaling of the thumbsize and fontsize as well as the margins) can be set via environment variable `PLIO_PRESCALE`.

## Info (2023-07-25)
- 'R' key is now available for **renaming** the basename to index value **by** **replacing** the basename (besides file extension), instead of prepending the index value.
## Info (2023-07-19)
- 'F' key is now also used to toggle fullscreen/default-size, because the &circ;-key was not working on all keyboard-mappings-/variants.
## Info (2023-06-02)
- A problem (no image previews visible) with plio 2023-05-15.0 (or newer commits) has been reported for one instance of a high-dpi environment on FreeBSD. If you encounter this problem too, please try plio 2023-05-08.0 and report an issue. Possibly the problem was in the setup of the system, not caused by plio; I'm waiting for feedback since then.
